<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLazadaTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lazada_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_number');
            $table->string('order_item_number');
            $table->string('transaction_number');
            $table->dateTime('transaction_date');
            $table->string('transaction_type');
            $table->string('fee_name');
            $table->string('seller_sku');
            $table->string('lazada_sku');
            $table->decimal('amount');
            $table->decimal('vat_amount');
            $table->decimal('wht_amount');
            $table->string('wht_included');
            $table->string('statement');
            $table->string('paid_status');
            $table->string('order_item_status');
            $table->string('shipping_provider');
            $table->string('shipping_speed');
            $table->string('shipment_type');
            $table->string('reference_number');
            $table->string('payment_reference_id');
            $table->text('comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lazada_transactions');
    }
}
