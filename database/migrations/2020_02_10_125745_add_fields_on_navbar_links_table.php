<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsOnNavbarLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('navbar_links', function (Blueprint $table) {
            $table->string('type',50);
            $table->tinyInteger('disable_link')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('navbar_links', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('disable_link');
        });
    }
}
