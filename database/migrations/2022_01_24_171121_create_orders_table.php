<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_number');
            $table->string('channel');
            $table->string('store_name');
            $table->string('status');
            $table->string('payment');
            $table->string('buyer_name');
            $table->string('recipient_name');
            $table->string('recipient_phone');
            $table->text('recipient_address');
            $table->string('courier');
            $table->string('tracking_number');
            $table->decimal('subtotal');
            $table->decimal('shipping_fee');
            $table->decimal('total');
            $table->dateTime('order_creation_date');
            $table->dateTime('order_update_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
