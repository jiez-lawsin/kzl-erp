<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEncoderColumnToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('sales_rep')->nullable()->after('order_update_date');
            $table->string('remarks')->nullable()->after('order_update_date');
            $table->integer('user_id')->nullable()->after('order_update_date');
            $table->string('order_creation_origin')->nullable()->after('order_update_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('remarks');
            $table->dropColumn('sales_rep');
            $table->dropColumn('order_creation_origin');
        });
    }
}
