<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopeeTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopee_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_number');
            $table->string('refund_id')->null();
            $table->string('username');
            $table->dateTime('order_creation_date');
            $table->dateTime('payout_completed_date');
            $table->decimal('original_price');
            $table->decimal('seller_product_promotion');
            $table->decimal('buyer_refund_amount');
            $table->decimal('product_discount_rebate');
            $table->decimal('seller_voucher_discount');
            $table->decimal('seller_absorbed_coin_cashback');
            $table->decimal('buyer_paid_shipping_fee');
            $table->decimal('shipping_fee_rebate');
            $table->decimal('third_party_logistics_shipping');
            $table->decimal('reverse_shipping_fee');
            $table->decimal('commission_fee');
            $table->decimal('service_fee');
            $table->decimal('transaction_fee');
            $table->decimal('total_released_amount');
            $table->decimal('lost_compensation');
            $table->decimal('seller_shipping_fee_promotion');
            $table->text('seller_voucher_code')->null();
            $table->string('shipping_provider');
            $table->string('courier_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopee_transactions');
    }
}
