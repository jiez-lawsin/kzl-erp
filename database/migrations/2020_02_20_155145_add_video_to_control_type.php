<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVideoToControlType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('control_type', function (Blueprint $table) {
            DB::statement("ALTER TABLE page_controls CHANGE COLUMN type type ENUM('text', 'number', 'checkbox', 'textarea', 'asset', 'select', 'color', 'date', 'time', 'date_time', 'video', 'tags') NOT NULL DEFAULT 'text'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('control_type', function (Blueprint $table) {
            DB::statement("ALTER TABLE page_controls CHANGE COLUMN type type ENUM('text', 'number', 'checkbox', 'textarea', 'asset', 'select', 'color', 'date', 'time', 'date_time', 'products', 'tags') NOT NULL DEFAULT 'text'");
        });
    }
}
