<?php

use Illuminate\Database\Seeder;

class PagesDefaultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
			"id" => 1,
			"name" => "Home",
			"slug" => "home",
			"published" => "draft",
			"page_category_id" => 0,
			"content" => null,
			"created_at" => "2018-10-12 14:01:57",
			"updated_at" => "2018-10-12 14:01:57",
			"allow_add_contents" => 0
		]);
		DB::table('pages')->insert([
			"id" => 2,
			"name" => "About",
			"slug" => "about",
			"published" => "draft",
			"page_category_id" => 0,
			"content" => null,
			"created_at" => "2018-10-12 14:43:28",
			"updated_at" => "2018-10-12 14:43:28",
			"allow_add_contents" => 0
		]);

		
		DB::table('pages')->insert([
			"id" => 3,
			"name" => "Privacy Policy",
			"slug" => "privacy-policy",
			"published" => "draft",
			"page_category_id" => 0,
			"content" => null,
			"created_at" => "2018-10-12 17:02:40",
			"updated_at" => "2018-10-12 17:02:40",
			"allow_add_contents" => 0
		]);
		DB::table('pages')->insert([
			"id" => 4,
			"name" => "Terms and Conditions",
			"slug" => "terms",
			"published" => "draft",
			"page_category_id" => 0,
			"content" => null,
			"created_at" => "2018-10-12 17:04:00",
			"updated_at" => "2018-10-12 17:04:00",
			"allow_add_contents" => 0
		]);
		DB::table('pages')->insert([
			"id" => 5,
			"name" => "Contact",
			"slug" => "contact",
			"published" => "draft",
			"page_category_id" => 0,
			"content" => null,
			"created_at" => "2018-10-12 17:04:00",
			"updated_at" => "2018-10-12 17:04:00",
			"allow_add_contents" => 0
		]);
		DB::table('page_contents')->insert([
			"id" => 1,
			"page_id" => 5,
			"name" => "Banners",
			"slug" => "banners",
			"title" => "",
			"description" => "",
			"bg_image" => 0,
			"published" => "draft",
			"order" => 1,
			"created_at" => "2018-10-12 14:02:16",
			"updated_at" => "2018-10-12 14:02:19",
			"deleted_at" => null,
			"enabled" => 1,
			"editable" => 1,
			"allow_add_items" => 0
		]);
		DB::table('page_contents')->insert([
			"id" => 2,
			"page_id" => 5,
			"name" => "Form",
			"slug" => "form",
			"title" => "",
			"description" => "",
			"bg_image" => 0,
			"published" => "draft",
			"order" => 1,
			"created_at" => "2018-10-12 14:02:19",
			"updated_at" => "2018-10-12 14:02:19",
			"deleted_at" => null,
			"enabled" => 1,
			"editable" => 1,
			"allow_add_items" => 0
		]);
		DB::table('page_content_items')->insert([
			"id" => 1,
			"content_id" => 2,
			"title" => "",
			"description" => "",
			"image" => 0,
			"bg_image" => 0,
			"type" => "default",
			"published" => "draft",
			"order" => 0,
			"created_at" => "2018-10-12 14:02:19",
			"updated_at" => "2018-10-12 14:02:19",
			"deleted_at" => null,
			"name" => "Form Elements",
			"slug" => "form-elements",
			"enabled" => 1,
			"editable" => 1
		]);
		DB::table('page_controls')->insert([
			"id" => 1,
			"reference_id" => 1,
			"reference_type" => "App\PageContentItem",
			"name" => "name",
			"label" => "Name",
			"type" => "text",
			"options_json" => "",
			"required" => 0,
			"order" => 0,
			"value" => "",
			"created_at" => "2018-10-12 14:10:13",
			"updated_at" => "2018-10-12 15:10:25"
		]);

		DB::table('page_controls')->insert([
			"id" => 2,
			"reference_id" => 1,
			"reference_type" => "App\PageContentItem",
			"name" => "email",
			"label" => "Email",
			"type" => "text",
			"options_json" => "",
			"required" => 0,
			"order" => 0,
			"value" => "",
			"created_at" => "2018-10-12 14:10:13",
			"updated_at" => "2018-10-12 15:10:25"
		]);
		DB::table('page_controls')->insert([
			"id" => 3,
			"reference_id" => 1,
			"reference_type" => "App\PageContentItem",
			"name" => "contact_number",
			"label" => "Contact Number",
			"type" => "text",
			"options_json" => "",
			"required" => 0,
			"order" => 0,
			"value" => "",
			"created_at" => "2018-10-12 14:10:13",
			"updated_at" => "2018-10-12 15:10:25"
		]);
		DB::table('page_controls')->insert([
			"id" => 4,
			"reference_id" => 1,
			"reference_type" => "App\PageContentItem",
			"name" => "address",
			"label" => "Address",
			"type" => "textarea",
			"options_json" => "",
			"required" => 0,
			"order" => 0,
			"value" => "",
			"created_at" => "2018-10-12 14:10:13",
			"updated_at" => "2018-10-12 15:10:25"
		]);
		
		DB::table('page_controls')->insert([
			"id" => 5,
			"reference_id" => 1,
			"reference_type" => "App\PageContentItem",
			"name" => "message",
			"label" => "Message",
			"type" => "textarea",
			"options_json" => "",
			"required" => 0,
			"order" => 0,
			"value" => "",
			"created_at" => "2018-10-12 14:10:13",
			"updated_at" => "2018-10-12 15:10:25"
		]);
		DB::table('page_controls')->insert([
			"id" => 6,
			"reference_id" => 1,
			"reference_type" => "App\PageContentItem",
			"name" => "subject",
			"label" => "Subject",
			"type" => "select",
			"options_json" => '[{"value":"hiring","label":"Hiring"},{"value":"inquiry","label":"Inquiry"}]',
			"required" => 0,
			"order" => 0,
			"value" => "",
			"created_at" => "2018-10-12 14:10:13",
			"updated_at" => "2018-10-12 15:10:25"
		]);
		DB::table('page_controls')->insert([
			"id" => 8,
			"reference_id" => 1,
			"reference_type" => "App\PageContent",
			"name" => "image",
			"label" => "Image",
			"type" => "asset",
			"options_json" => "",
			"required" => 0,
			"order" => 0,
			"value" => "",
			"created_at" => "2018-10-12 14:10:13",
			"updated_at" => "2018-10-12 15:10:25"
		]);
		DB::table('page_controls')->insert([
			"id" => 9,
			"reference_id" => 1,
			"reference_type" => "App\PageContent",
			"name" => "video",
			"label" => "Video",
			"type" => "video",
			"options_json" => "",
			"required" => 0,
			"order" => 0,
			"value" => "",
			"created_at" => "2018-10-12 14:10:13",
			"updated_at" => "2018-10-12 15:10:25"
		]);
		DB::table('page_controls')->insert([
			"id" => 10,
			"reference_id" => 1,
			"reference_type" => "App\PageContent",
			"name" => "link",
			"label" => "link",
			"type" => "text",
			"options_json" => "",
			"required" => 0,
			"order" => 0,
			"value" => "",
			"created_at" => "2018-10-12 14:10:13",
			"updated_at" => "2018-10-12 15:10:25"
		]);	
    }
}
