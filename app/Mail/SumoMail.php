<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SumoMail extends Mailable
{
    use Queueable, SerializesModels;

    public $view_name;
    public $view_data;
    public $email_params;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($view_name, array $view_data, array $email_params)
    {
        $this->view_name = $view_name;
        $this->view_data = $view_data;
        $this->email_params = $email_params;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        if ( isset($this->email_params['cc']) || @$this->email_params['cc'] != ""){
            $this->cc($this->email_params['cc']);
        }

        if (isset($this->email_params['bcc']) || @$this->email_params['bcc']!= ""){
            $this->bcc($this->email_params['bcc']);
        }

        if (isset($this->email_params['attach']) || @$this->email_params['attach'] != ""){
            if (is_array($this->email_params['attach'])) {
                foreach ($this->email_params['attach'] as $key => $value) {
                    $this->attach(public_path() . '/' . $value);
                }
            } else {
                $this->attach(public_path() . '/' . $this->email_params['attach']);
            }
        }

        if (!isset($this->email_params['replyTo']) || $this->email_params['replyTo'] == ""){
            $this->email_params['replyTo']=$this->email_params['from_email'];
        }

        if (@$this->email_params['replyTo_name'] == ""){
            $this->email_params['replyTo_name']=$this->email_params['from_name'];
        }	

        //use the admin set email or use the defaults
        if (isset($this->email_params['from_email']) || @$this->email_params['from_email'] == ""){
            if (!isset($this->email_params['from_name']) || @$this->email_params['from_name'] == ""){
                $this->from($this->email_params['from_email']);
            }else{
                $this->from($this->email_params['from_email'], $this->email_params['from_name']);
            }
        }

        $this->to($this->email_params['to']);
        $this->replyTo($this->email_params['replyTo'],$this->email_params['replyTo_name']);
        $this->subject($this->email_params['subject']);

        return $this->view($this->view_name)->with($this->view_data);
    }
}
