<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class OrderTransaction extends BaseModel
{
    
    protected $fillable = [
    	'order_number',

        'transaction_number',

        'transaction_date',

        'transaction_type',

        'fee_name',

        'amount',

        'paid_status',

        'comment',
    	];
    
    
    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }
    
    public function activities()
    {
        return $this->morphMany('App\Activity', 'loggable');
    }
    public function order() 
    {
        return $this->hasOne('App\Order', 'order_number', 'order_number');
    }
}
