<?php

namespace App\Imports;

use App\LazadaTransaction;
use App\Order;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Carbon;

class LazadaTransactionsImport implements ToCollection, WithStartRow, WithHeadingRow
{
    private $transactions;

    public function __construct()
    {
        $this->transactions = LazadaTransaction::get();
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            if ($row && @$row['transaction_number'] && @$row['transaction_number'] != '') {
                $transaction = $this->transactions
                                ->where('transaction_number', $row['transaction_number'])
                                ->where('fee_name', $row['fee_name'])
                                ->where('transaction_type', $row['transaction_type'])
                                ->where('lazada_sku', $row['lazada_sku'])
                                ->where('seller_sku', $row['seller_sku'])
                                ->first();
                if (!$transaction) {
                    $transaction = new LazadaTransaction;  
                }
                $transaction->transaction_number = $row['transaction_number'];
                $transaction->order_number = $row['order_no'];
                $transaction->order_item_number = $row['order_item_no'];
                $transaction->transaction_date = Carbon::parse($row['transaction_date']);
                $transaction->transaction_type = $row['transaction_type'];
                $transaction->fee_name = $row['fee_name'];
                $transaction->seller_sku = $row['seller_sku'];
                $transaction->lazada_sku = $row['lazada_sku'];
                $transaction->amount = $row['amount'];
                $transaction->vat_amount = $row['vat_in_amount'];
                $transaction->wht_amount = $row['wht_amount'];
                $transaction->wht_included = $row['wht_included_in_amount'];
                $transaction->statement = $row['statement'];
                $transaction->paid_status = $row['paid_status'];
                $transaction->order_item_status = $row['order_item_status'];
                $transaction->shipping_provider = $row['shipping_provider'];
                $transaction->shipping_speed = $row['shipping_speed'];
                $transaction->shipment_type = $row['shipment_type'];
                $transaction->reference_number = $row['reference'];
                $transaction->payment_reference_id = $row['paymentrefid'];
                $transaction->comment = $row['comment'];
                $transaction->save();
            }
        }
    }

}