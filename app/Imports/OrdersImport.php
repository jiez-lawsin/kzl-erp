<?php

namespace App\Imports;

use App\MasterProduct;
use App\Order;
use App\OrderItem;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Carbon; 

class OrdersImport implements ToCollection, WithStartRow, WithHeadingRow
{
    private $orders;
    private $master_products;

    public function __construct()
    {
        $this->master_products = MasterProduct::get();
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }
    
    public function generateSKU() {
        $today = date("ymd");
        $rand = strtoupper(substr(uniqid(sha1(time())),0,2));
        $trx_no = 'P-' . $today . '-' . time() . '-' . $rand;
        return $trx_no;
    }

    public function collection(Collection $rows)
    {
        $orderItem = null;
        $orderNumber = '';
        foreach ($rows as $row) 
        {
            if ($row) {
                if (@$row['order_id'] != '') {
                    $orderNumber = $row['order_id'];
                    $order = Order::where('order_number', $orderNumber)->first();
                    if (!$order) $order = new Order;
                    
                    $order->order_number = $orderNumber;
                    $order->order_creation_origin = 'ginee';
                    $order->channel= $row['channel'];
                    $order->store_name = $row['store_name'];
                    $order->status = $row['status'];
                    $order->payment = $row['payment'];
                    $order->buyer_name = $row['buyer_name'];
                    $order->recipient_name = $row['recipient_name'];
                    $order->recipient_phone = $row['recipient_phone_no'];
                    $order->recipient_address = $row['recipient_address'];
                    $order->courier = $row['courier'];
                    $order->tracking_number = $row['awbtracking_code'];
                    $order->subtotal = $row['subtotal'];
                    $order->shipping_fee = $row['shipping_fee'];
                    $order->total = $row['total'];
                    $order->order_creation_date = Carbon::createFromFormat('d-m-Y H:i', $row['create_time']);
                    $order->order_update_date = Carbon::createFromFormat('d-m-Y H:i', $row['create_time']);
                    $order->save();
                }
                if ($orderNumber != '') {
                    $product = $row['product_name'];
                    if ($product != '') {
                        $newOrderItem = OrderItem::where('order_number', $orderNumber)->where('seller_sku', $row['sku'])->first();
                        if (!$newOrderItem) $newOrderItem = new OrderItem;
                        
                        $newOrderItem->quantity = $row['qty'];
                        $newOrderItem->order_number = $orderNumber;
                        $newOrderItem->name = $product;
                        $newOrderItem->seller_sku = $row['sku'];
                        $newOrderItem->original_price = $row['product_original_price'];
                        $newOrderItem->discounted_price = $row['product_discounted_price'];
                        $msku = $this->master_products->where('master_sku', $row['inventory_sku'])->first();
                        if (@$msku) {
                            $newOrderItem->master_sku = @$msku->master_sku;
                        } else {
                            if (@$row['sku'] && $row['sku'] != '') {
                                $newOrderItem->master_sku = @$row['sku'];
                            } else {
                                $newOrderItem->master_sku = @$this->generateSKU();
                            }
                        }
                        $newOrderItem->capital_price = @$msku->purchase_price ? @$msku->purchase_price : 0;
                        $newOrderItem->save();
                        $orderItem = clone $newOrderItem;
                    } else {
                        $orderItem->quantity += $row['qty'];
                        $orderItem->save();
                    }
                }
            }
        }
    }
}