<?php

namespace App\Imports;

use App\ShopeeTransaction;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Carbon;
class ShopeeTransactionsImport implements ToCollection, WithStartRow, WithHeadingRow
{
    private $transactions;

    public function __construct()
    {
        $this->transactions = ShopeeTransaction::get();
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            if ($row && @$row['order_id'] && @$row['order_id'] != '') {
                $transaction = $this->transactions->where('order_number', $row['order_id'])->first();
                if (!$transaction) {
                    $transaction = new ShopeeTransaction;  
                }
                $transaction->order_number = $row['order_id'];
                $transaction->refund_id = $row['refund_id'];
                $transaction->username = $row['username_buyer'];
                $transaction->order_creation_date = Carbon::parse($row['order_creation_date']);
                $transaction->payout_completed_date = Carbon::parse($row['payout_completed_date']);
                $transaction->original_price = $row['original_product_price'];
                $transaction->seller_product_promotion = $row['seller_product_promotion'];
                $transaction->buyer_refund_amount = $row['refund_amount_to_buyer'];
                $transaction->product_discount_rebate = $row['product_discount_rebate_from_shopee'];
                $transaction->seller_voucher_discount = $row['seller_voucher_discount'];
                $transaction->seller_absorbed_coin_cashback = $row['seller_absorbed_coin_cashback'];
                $transaction->buyer_paid_shipping_fee = $row['buyer_paid_shipping_fee'];
                $transaction->shipping_fee_rebate = $row['shipping_fee_rebate_from_shopee'];
                $transaction->third_party_logistics_shipping = $row['3rd_party_logistics_defined_shipping_fee'];
                $transaction->reverse_shipping_fee = $row['reverse_shipping_fee'];
                $transaction->commission_fee = $row['commission_fee'];
                $transaction->service_fee = $row['service_fee'];
                $transaction->transaction_fee = $row['transaction_fee'];
                $transaction->total_released_amount = $row['total_released_amount'];
                $transaction->lost_compensation = $row['lost_compensation'];
                $transaction->seller_shipping_fee_promotion = $row['shipping_fee_promotion_by_seller'];
                $transaction->seller_voucher_code = $row['seller_voucher_code'];
                $transaction->shipping_provider = $row['shipping_provider'];
                $transaction->courier_name = $row['courier_name'];
                $transaction->save();
            }
        }
    }

}