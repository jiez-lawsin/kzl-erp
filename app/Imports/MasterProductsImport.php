<?php

namespace App\Imports;

use App\MasterProduct;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MasterProductsImport implements ToCollection, WithStartRow, WithHeadingRow
{
    private $master_products;

    public function __construct()
    {
        $this->master_products = MasterProduct::get();
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            if ($row && @$row['master_sku'] && @$row['master_sku'] != '') {
                $master_product = $this->master_products->where('master_sku', $row['master_sku'])->first();
                if (!$master_product) {
                    $master_product = new MasterProduct;  
                }
                $master_product->master_sku = $row['master_sku'];
                $master_product->name = $row['master_product_name'];
                $master_product->variant_name = $row['variant_name'];
                if (@$row['purchase_price'] && @$row['purchase_price'] != '' && @$row['purchase_price'] != 0 && @$row['purchase_price'] != '0' && @$row['purchase_price'] != '0.00') {
                    $master_product->purchase_price = @$row['purchase_price'] ? @$row['purchase_price'] : 0;
                }
                $master_product->save();
            }
        }
    }

}