<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MasterProduct;

class MasterProductController extends Controller
{
    public function search(Request $request)
    {
        $response = [];
        $input = $request->all();
        $data = MasterProduct::select('master_sku', 'variant_name', 'name', 'id')->where('name', 'LIKE', '%' . @$input['keyword'] . '%')->get();

        if (! is_null($data)) {
            $response['data'] = $data;
            $response['message'] = 'Successfully retrieved data.';
            $status = 200;
        } else {
            $response['message'] = 'Failed to retrieve data.';
            $status = 404;
        }

        return response($response, $status);
    }

    public function find(Request $request)
    {
        $response = [];
        $input = $request->all();
        $data = MasterProduct::find($input['id']);

        if (! is_null($data)) {
            $response['data'] = $data;
            $response['message'] = 'Successfully retrieved data.';
            $status = 200;
        } else {
            $response['message'] = 'Failed to retrieve data.';
            $status = 404;
        }

        return response($response, $status);
    }

    public function get(Request $request)
    {
        $response = [];
        $input = $request->all();

        $data = new MasterProduct;
        if (isset($input['skip']) && isset($input['take'])) {
            $data = $data->skip($input['skip'])->take($input['take']);
        }
        // FILTER DATA
        // $data->whereParam1('value1');
        // $data->whereParam2('value2');
        $data = $data->_paginate($data, $request, $input);

        if (! is_null($data)) {
            $response['data'] = $data;
            $response['message'] = 'Successfully retrieved data.';
            $status = 200;
        } else {
            $response['message'] = 'Failed to retrieve data.';
            $status = 400;
        }


        return response($response, $status);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $data = MasterProduct::create($input);

        if ($data) {
            $response = [];
            $response['message'] = 'Successfully created data.';
            $response['data'] = $data;
            $status = 201;
        } else {
            $response = [];
            $response['message'] = 'Failed to create data.';
            $status = 400;
        }

        return response($response, $status);
    }

    public function update(Request $request)
    {
        $input = $request->all();
        $data = MasterProduct::find($input['id']);
        $data->update($input);

        if (! is_null($data)) {
            $response = [];
            $response['message'] = 'Successfully updated data.';
            $response['data'] = $data;
            $status = 200;
        } else {
            $response = [];
            $response['message'] = 'Failed to update data.';
            $status = 400;
        }

        return response($response, $status);
    }

    public function destroy(Request $request)
    {
        $input = $request->all();
        MasterProduct::destroy($input['id']);

        $response = [];
        $response['message'] = 'Successfully deleted data.';
        $status = 200;

        return response($response, $status);
    }

/** Copy/paste these lines to app\Http\routes.api.php 
Route::get('api/master_products/find', array('as'=>'apiMasterProductsFind','uses'=>'MasterProductController@find', 'middleware'=>'cors'));
Route::get('api/master_products/get', array('as'=>'apiMasterProductsGet','uses'=>'MasterProductController@get', 'middleware'=>'cors'));
Route::post('api/master_products/store', array('as'=>'apiMasterProductsStore','uses'=>'MasterProductController@store', 'middleware'=>'cors'));
Route::post('api/master_products/update', array('as'=>'apiMasterProductsUpdate','uses'=>'MasterProductController@update', 'middleware'=>'cors'));
Route::post('api/master_products/destroy', array('as'=>'apiMasterProductsDestroy','uses'=>'MasterProductController@destroy', 'middleware'=>'cors'));
*/
}
