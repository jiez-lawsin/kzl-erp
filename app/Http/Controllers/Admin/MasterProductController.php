<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\MasterProductRequest;

use Acme\Facades\Activity;
use App\Exports\MasterProductsExport;
use App\Imports\MasterProductsImport;
use App\MasterProduct;
use App\Seo;
use Excel;
use Maatwebsite\Excel\Facades\Excel as FacadesExcel;

class MasterProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        $data = MasterProduct::with('order_items');
        if ($master_sku = $request->master_sku) {
            if ($request->msku_prefix) {
                $data = $data->where('master_sku', 'LIKE', '%' . $master_sku . '%');
            } else {
                $data = $data->where('master_sku', 'LIKE', $master_sku . '%');
            }
        }
        if ($unset_capital = $request->unset_capital) {
            $data = $data->whereHas('order_items', function($query)  {
                $query->where('capital_price', 0);
            });
        }

        $data = $data->paginate(25);
        $pagination = $data->appends($request->except('page'))->links();

        return view('admin/master_products/index')
            ->with('title', 'MasterProducts')
            ->with('menu', 'master_products')
            ->with('request', $request)
            ->with('unset_capital', $request->unset_capital)
            ->with('data', $data)
            ->with('pagination', $pagination);
    }

    public function export(Request $request)
    {
        return FacadesExcel::download(new MasterProductsExport, 'Master Products.xlsx');
    }

    public function create()
    {
        return view('admin/master_products/create')
            ->with('title', 'Create master_product')
            ->with('menu', 'master_products');
    }
    
    public function store(MasterProductRequest $request)
    {
        $input = $request->all();
        $master_product = MasterProduct::create($input);
        
        // $log = 'creates a new master_product "' . $master_product->name . '"';
        // Activity::create($log);

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to edit.',
            'resetForm'=>true,
            'redirect'=>route('adminMasterProductsEdit', [$master_product->id])
        ];

        return response()->json($response);
    }
    
    public function show($id)
    {
        return view('admin/master_products/show')
            ->with('title', 'Show master_product')
            ->with('data', MasterProduct::findOrFail($id));
    }

    public function view($id)
    {
        return view('admin/master_products/view')
            ->with('title', 'View master_product')
            ->with('menu', 'master_products')
            ->with('data', MasterProduct::findOrFail($id));
    }
    
    public function edit($id)
    {
        $data = MasterProduct::findOrFail($id);
        $seo = $data->seo()->first();

        return view('admin/master_products/edit')
            ->with('title', 'Edit master_product')
            ->with('menu', 'master_products')
            ->with('data', $data)
            ->with('seo', $seo);
    }
    
    public function update(MasterProductRequest $request, $id)
    {
        $input = $request->all();
        $master_product = MasterProduct::findOrFail($id);
        $master_product->update($input);

        // $log = 'edits a master_product "' . $master_product->name . '"';
        // Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }

    public function updateFromModal(MasterProductRequest $request)
    {
        $input = $request->all();
        $master_product = MasterProduct::where('master_sku', @$input['master_sku'])->first();
        if ($master_product) {
            $master_product->update($input);
            $response = [
                'master_product'=>$master_product,
                'notifTitle'=>'Save successful.',
            ];
        } else {
            $response = [
                'notifStatus' =>'error',
                'notifTitle'=>'Save successful.',
            ];
        }

        return response()->json($response);
    }

    public function seo(Request $request)
    {
        $input = $request->all();

        $data = MasterProduct::findOrFail($input['seoable_id']);
        $seo = Seo::whereSeoable_id($input['seoable_id'])->whereSeoable_type($input['seoable_type'])->first();
        if (is_null($seo)) {
            $seo = new Seo;
        }
        $seo->title = $input['title'];
        $seo->description = $input['description'];
        $seo->image = $input['image'];
        $data->seo()->save($seo);

        $response = [
            'notifTitle'=>'SEO Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        $data = MasterProduct::whereIn('id', $input['ids'])->get();
        $names = [];
        foreach ($data as $d) {
            $names[] = $d->master_sku;
        }
        // $log = 'deletes a new master_product "' . implode(', ', $names) . '"';
        // Activity::create($log);

        MasterProduct::destroy($input['ids']);

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminMasterProducts')
        ];

        return response()->json($response);
    }

    public function import(Request $request)
    {
        $input = $request->all();
        Excel::import(new MasterProductsImport(), $request->file('file'));
        // $rows = Excel::toArray(new MasterProductsExport, $request->file('file'));
        // dd($rows);

        // $log = 'edits a master_product "' . $master_product->name . '"';
        // Activity::create($log);

        // $response = [
        //     'notifTitle'=>'Save successful.',
        // ];

        // return response()->json($response);
        return redirect(route('adminMasterProducts'));
    }
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/master_products', array('as'=>'adminMasterProducts','uses'=>'Admin\MasterProductController@index'));
Route::get('admin/master_products/create', array('as'=>'adminMasterProductsCreate','uses'=>'Admin\MasterProductController@create'));
Route::post('admin/master_products/', array('as'=>'adminMasterProductsStore','uses'=>'Admin\MasterProductController@store'));
Route::get('admin/master_products/{id}/show', array('as'=>'adminMasterProductsShow','uses'=>'Admin\MasterProductController@show'));
Route::get('admin/master_products/{id}/view', array('as'=>'adminMasterProductsView','uses'=>'Admin\MasterProductController@view'));
Route::get('admin/master_products/{id}/edit', array('as'=>'adminMasterProductsEdit','uses'=>'Admin\MasterProductController@edit'));
Route::patch('admin/master_products/{id}', array('as'=>'adminMasterProductsUpdate','uses'=>'Admin\MasterProductController@update'));
Route::post('admin/master_products/seo', array('as'=>'adminMasterProductsSeo','uses'=>'Admin\MasterProductController@seo'));
Route::delete('admin/master_products/destroy', array('as'=>'adminMasterProductsDestroy','uses'=>'Admin\MasterProductController@destroy'));
*/
}
