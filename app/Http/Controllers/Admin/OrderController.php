<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;

use Acme\Facades\Activity;
use App\Exports\OrdersExport;
use App\Imports\OrdersImport;
use App\MasterProduct;
use App\Order;
use App\OrderItem;
use App\OrderTransaction;
use App\Seo;
use Excel;
use Carbon;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        $input = $request->all();
        $data = Order::with('items.master')->orderBy('order_creation_date', 'desc');
        if ($date_range = $request->date_range) {
            $date_range = explode(" - ", $date_range);
            if (@$date_range[0] && @$date_range[1]) {
                $start = Carbon::parse($date_range[0]);
                $end = Carbon::parse($date_range[1]);
                $data = $data->where('order_creation_date', '>=', $start)->where('order_creation_date', '<=', $end);
            }
        } else {
            $start = Carbon::now()->subMonths(2)->startOfMonth();
            $end = Carbon::now()->endOfMonth();
            $data = $data->where('order_creation_date', '>=', $start)->where('order_creation_date', '<=', $end);
            $input['date_range'] = $start->format('m/d/Y') . ' - ' . $end->format('m/d/Y');
        }
        if ($channel = $request->channel) {
            $data = $data->where('channel', $channel);
        }
        if ($store = $request->store) {
            $data = $data->where('store_name', $store);
        }
        if ($status = $request->status) {
            $data = $data->where('status', $status);
        }
        if ($order_number = $request->order_number) {
            $data = $data->where('order_number', 'LIKE', '%' . $order_number . '%');
        }
        if ($product_name = $request->product_name) {
            $data = $data->whereHas('items', function($query) use ($product_name) {
                $query->where('name', 'LIKE', '%' . $product_name . '%')->orWhereHas('master', function($mQuery) use ($product_name) {
                    $mQuery->where('name', 'LIKE', '%' . $product_name . '%');
                });
            });
        }
        if ($master_sku = $request->master_sku) {
            $data = $data->whereHas('items', function($query) use ($master_sku) {
                $query->where('master_sku', 'LIKE', '%' . $master_sku . '%');;
            });
        }
        $data = $data->paginate(25);
        $pagination = $data->appends($request->except('page'))->links();

        $stores = Order::select('store_name')->groupBy('store_name')->get();
        $channels = Order::select('channel')->groupBy('channel')->get();
        $statuses = Order::select('status')->groupBy('status')->get();
        return view('admin/orders/index')
            ->with('title', 'Orders')
            ->with('menu', 'orders')
            ->with('keyword', $request->order_number)
            ->with('data', $data)
            ->with('stores', $stores)
            ->with('channels', $channels)
            ->with('statuses', $statuses)
            ->with('request', $input)
            ->with('pagination', $pagination);
    }

    public function create()
    {
        $order_number = $this->generateOrderNumber();
        $order = Order::select('order_number')->where('order_number', $order_number)->get();
        while(count($order) > 0) {
            $order_number = $this->generateOrderNumber();
            $order = Order::select('order_number')->where('order_number', $order_number)->get();
        }
        return view('admin/orders/create')
            ->with('title', 'Create Order')
            ->with('order_number', $order_number)
            ->with('menu', 'orders');
    }

    public function generateOrderNumber() {
        $today = date("ymd");
        $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
        $order_number = $today . time() . $rand;
        return $order_number;
    }
    
    public function generateTransactionNumber() {
        $today = date("ymd");
        $rand = strtoupper(substr(uniqid(sha1(time())),0,2));
        $trx_no = 'OTRX-' . $today . time() . $rand;
        return $trx_no;
    }
    
    public function save(OrderRequest $request)
    {
        $input = $request->all();
        if (@$input['master_sku'] && @count($input['master_sku']) > 0) {
            if (@$input['order_number']) {
                $order = Order::where('order_number', $input['order_number'])->first();
                if (!$order) {
                    $order = new Order;
                    $order->order_number = $input['order_number'];
                    $order->order_creation_origin = 'manual';
                    $order->user_id = Auth::user()->id;
                }
                $order->channel = @$input['channel'];
                $order->store_name = @$input['store_name'];
                $order->status = @$input['status'];
                $order->payment = @$input['payment'];
                $order->buyer_name = @$input['buyer_name'];
                $order->recipient_name = @$input['recipient_name'] ? @$input['recipient_name'] : '';
                $order->recipient_phone = @$input['recipient_phone'] ? @$input['recipient_phone'] : '';
                $order->recipient_address = @$input['recipient_address'] ? @$input['recipient_address'] : '';;
                $order->courier = @$input['courier'] ? @$input['courier'] : '';;
                $order->tracking_number = @$input['tracking_number'] ? @$input['tracking_number'] : '';;
                $order->sales_rep = @$input['sales_rep'];
                $order->remarks = @$input['remarks'];
                $order->order_creation_date = Carbon::parse(@$input['order_creation_date']);
                $order->save();

                $order_items = OrderItem::where('order_number', $order->order_number)->get();
                foreach ($order_items as $item) {
                    $isStillIncluded = false;
                    foreach ($input['master_sku'] as $msku) {
                        if ($item->master_sku == $msku) $isStillIncluded = true;
                    }
                    if (!$isStillIncluded) $item->delete();
                }
                foreach ($input['master_sku'] as $key => $msku) {
                    $master_product = MasterProduct::where('master_sku', $msku)->first();
                    $order_item = $order_items->where('master_sku', $msku)->first();
                    if (!$order_item) {
                        $order_item = new OrderItem;
                        $order_item->order_number = $order->order_number;
                        $order_item->master_sku = $msku;
                    }
                    $order_item->original_price = @$input['item_price'][$key] ? $input['item_price'][$key] : 0;
                    $order_item->discounted_price = @$input['item_price'][$key] ? $input['item_price'][$key] : 0;
                    $order_item->capital_price = $master_product->purchase_price;
                    $order_item->name = $master_product->name;
                    $order_item->quantity = @$input['item_quantity'][$key] ? $input['item_quantity'][$key] : 1;
                    $order_item->save();
                }
                OrderTransaction::where('order_number', $order->order_number)->delete();
                foreach ($input['transaction_type'] as $key => $type) {
                    $transaction = new OrderTransaction;
                    $transaction->order_number = $order->order_number;
                    $transaction->transaction_date = Carbon::now();
                    $transaction->transaction_type = $type;
                    $transaction->amount = @$input['transaction_amount'][$key] ? $input['transaction_amount'][$key] : 0;
                    $transaction->comment = @$input['transaction_comment'][$key] ? $input['transaction_comment'][$key] : '';
                    $transaction->save();
                }
            } else {
                $response = [
                    'notifStatus'=>'error',
                    'notifTitle'=>'Something went wrong.',
                    'notifMessage'=>'No order number generated.',
                ];
            }
        } else {
            $response = [
                'notifStatus'=>'error',
                'notifTitle'=>'No products added.',
                'notifMessage'=>'Please add atleast 1 product.',
            ];
        }
        
        // $log = 'manually creates a new order "' . $order->order_number . '"';
        // Activity::create($log);

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Refreshing page.',
            'resetForm'=>true,
            'redirect'=>route('adminOrdersView', [$order->order_number])
        ];

        return response()->json($response);
    }
    
    public function show($id)
    {
        return view('admin/orders/show')
            ->with('title', 'Show order')
            ->with('data', Order::findOrFail($id));
    }

    public function view($id)
    {
        $data = Order::with('items.master', 'laz_transactions', 'shopee_transaction', 'order_transactions')->where('order_number', $id)->first();
        if (@count($data->items) > 0) {
            foreach ($data->items as $item) {
                if ($item->capital_price == 0) {
                    $item->capital_price = @$item->master->purchase_price;
                    $item->save();
                }
            }
        }
        if ($data->order_creation_origin == 'manual') {
            $return = view('admin/orders/create')
                ->with('order_number', $data->order_number);
        } else {
            $return =  view('admin/orders/view');
        }
        $return->with('menu', 'orders')
                ->with('title', 'View Order')
                ->with('data', $data);
        return $return;
    }
    
    public function edit($id)
    {
        $data = Order::findOrFail($id);
        $seo = $data->seo()->first();

        return view('admin/orders/edit')
            ->with('title', 'Edit order')
            ->with('menu', 'orders')
            ->with('data', $data)
            ->with('seo', $seo);
    }
    
    public function update(OrderRequest $request, $id)
    {
        $input = $request->all();
        $order = Order::findOrFail($id);
        $order->update($input);

        // $log = 'edits a order "' . $order->name . '"';
        // Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }

    public function seo(Request $request)
    {
        $input = $request->all();

        $data = Order::findOrFail($input['seoable_id']);
        $seo = Seo::whereSeoable_id($input['seoable_id'])->whereSeoable_type($input['seoable_type'])->first();
        if (is_null($seo)) {
            $seo = new Seo;
        }
        $seo->title = $input['title'];
        $seo->description = $input['description'];
        $seo->image = $input['image'];
        $data->seo()->save($seo);

        $response = [
            'notifTitle'=>'SEO Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        $data = Order::whereIn('id', $input['ids'])->get();
        $names = [];
        foreach ($data as $d) {
            $names[] = $d->order_number;
        }
        // $log = 'deletes a new order "' . implode(', ', $names) . '"';
        // Activity::create($log);

        Order::destroy($input['ids']);

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminOrders')
        ];

        return response()->json($response);
    }
    
    public function import(Request $request)
    {
        $input = $request->all();
        // $rows = Excel::toArray(new OrdersExport, $request->file('file'));
        // dd($rows);
        Excel::import(new OrdersImport(), $request->file('file'));
        return redirect(route('adminOrders'));
    }

    public function updatePurchasePriceFromModal(Request $request)
    {
        $input = $request->all();
        $orderItem = OrderItem::findOrFail($input['id']);
        if ($orderItem) {
            $orderItem->capital_price = $input['purchase_price'];
            $orderItem->save();
            $response = [
                'order_item'=>$orderItem,
                'notifTitle'=>'Save successful.',
            ];
        } else {
            $response = [
                'notifStatus' =>'error',
                'notifTitle'=>'Save successful.',
            ];
        }

        return response()->json($response);
    }
    public function updatePurchasePriceFromModalMultiple(Request $request)
    {
        $input = $request->all();
        
        if ($input['order_item_capital'] && count($input['order_item_capital']) && $input['order_item_id'] && count($input['order_item_id'])) {
            foreach ($input['order_item_id'] as $key => $id) {
                $item = OrderItem::find($id);
                if ($item) {
                    if (@$input['order_item_capital'][$key] && (float) $input['order_item_capital'][$key] > 0) {
                        $item->capital_price = $input['order_item_capital'][$key];
                        $item->save();
                    }
                }
            }
        }
        $response = [
            'input'=>$input,
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/orders', array('as'=>'adminOrders','uses'=>'Admin\OrderController@index'));
Route::get('admin/orders/create', array('as'=>'adminOrdersCreate','uses'=>'Admin\OrderController@create'));
Route::post('admin/orders/', array('as'=>'adminOrdersStore','uses'=>'Admin\OrderController@store'));
Route::get('admin/orders/{id}/show', array('as'=>'adminOrdersShow','uses'=>'Admin\OrderController@show'));
Route::get('admin/orders/{id}/view', array('as'=>'adminOrdersView','uses'=>'Admin\OrderController@view'));
Route::get('admin/orders/{id}/edit', array('as'=>'adminOrdersEdit','uses'=>'Admin\OrderController@edit'));
Route::patch('admin/orders/{id}', array('as'=>'adminOrdersUpdate','uses'=>'Admin\OrderController@update'));
Route::post('admin/orders/seo', array('as'=>'adminOrdersSeo','uses'=>'Admin\OrderController@seo'));
Route::delete('admin/orders/destroy', array('as'=>'adminOrdersDestroy','uses'=>'Admin\OrderController@destroy'));
*/
}
