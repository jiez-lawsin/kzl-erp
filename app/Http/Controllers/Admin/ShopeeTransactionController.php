<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ShopeeTransactionRequest;

use Acme\Facades\Activity;
use App\Imports\ShopeeTransactionsImport;
use App\Order;
use App\ShopeeTransaction;
use App\Seo;
use Excel;
use Carbon;
class ShopeeTransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        $input = $request->all();
        $data = ShopeeTransaction::with('order.items.master')->orderBy('payout_completed_date', 'desc');
        if ($order_number = $request->order_number) {
            $data = $data->where('order_number', 'LIKE', '%' . $order_number . '%');
        }
        if ($date_range = $request->date_range) {
            $date_range = explode(" - ", $date_range);
            if (@$date_range[0] && @$date_range[1]) {
                $start = Carbon::parse($date_range[0]);
                $end = Carbon::parse($date_range[1]);
                $data = $data->where('payout_completed_date', '>=', $start)->where('payout_completed_date', '<=', $end);
            }
        }
        if ($store = $request->store) {
            $data = $data->whereHas('order', function($query) use ($store) {
                        $query->where('store_name', $store);
                    });
        }
        $byOrders = clone $data;
        $transactions = clone $data;
        $data = $data->paginate(25);
        $pagination = $data->appends($request->except('page'))->links();

        $stores = Order::select('store_name', 'channel')->groupBy('store_name')->get();
        return view('admin/shopee_transactions/index')
            ->with('title', 'Shopee Transactions')
            ->with('menu', 'shopee_transactions')
            ->with('keyword', $request->order_number)
            ->with('data', $data)
            ->with('transactions', $transactions->get())
            ->with('byOrders', $byOrders->groupBy('order_number')->get())
            ->with('stores', $stores)
            ->with('request', $input)
            ->with('pagination', $pagination);
    }

    public function create()
    {
        return view('admin/shopee_transactions/create')
            ->with('title', 'Create shopee_transaction')
            ->with('menu', 'shopee_transactions');
    }
    
    public function store(ShopeeTransactionRequest $request)
    {
        $input = $request->all();
        $shopee_transaction = ShopeeTransaction::create($input);
        
        // $log = 'creates a new shopee_transaction "' . $shopee_transaction->name . '"';
        // Activity::create($log);

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to edit.',
            'resetForm'=>true,
            'redirect'=>route('adminShopeeTransactionsEdit', [$shopee_transaction->id])
        ];

        return response()->json($response);
    }
    
    public function show($id)
    {
        return view('admin/shopee_transactions/show')
            ->with('title', 'Show shopee_transaction')
            ->with('data', ShopeeTransaction::findOrFail($id));
    }

    public function view($id)
    {
        return view('admin/shopee_transactions/view')
            ->with('title', 'View shopee_transaction')
            ->with('menu', 'shopee_transactions')
            ->with('data', ShopeeTransaction::findOrFail($id));
    }
    
    public function edit($id)
    {
        $data = ShopeeTransaction::findOrFail($id);
        $seo = $data->seo()->first();

        return view('admin/shopee_transactions/edit')
            ->with('title', 'Edit shopee_transaction')
            ->with('menu', 'shopee_transactions')
            ->with('data', $data)
            ->with('seo', $seo);
    }
    
    public function update(ShopeeTransactionRequest $request, $id)
    {
        $input = $request->all();
        $shopee_transaction = ShopeeTransaction::findOrFail($id);
        $shopee_transaction->update($input);

        // $log = 'edits a shopee_transaction "' . $shopee_transaction->name . '"';
        // Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }

    public function seo(Request $request)
    {
        $input = $request->all();

        $data = ShopeeTransaction::findOrFail($input['seoable_id']);
        $seo = Seo::whereSeoable_id($input['seoable_id'])->whereSeoable_type($input['seoable_type'])->first();
        if (is_null($seo)) {
            $seo = new Seo;
        }
        $seo->title = $input['title'];
        $seo->description = $input['description'];
        $seo->image = $input['image'];
        $data->seo()->save($seo);

        $response = [
            'notifTitle'=>'SEO Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        $data = ShopeeTransaction::whereIn('id', $input['ids'])->get();
        $names = [];
        foreach ($data as $d) {
            $names[] = $d->order_number;
        }
        // $log = 'deletes a new shopee_transaction "' . implode(', ', $names) . '"';
        // Activity::create($log);

        ShopeeTransaction::destroy($input['ids']);

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminShopeeTransactions')
        ];

        return response()->json($response);
    }
    public function import(Request $request)
    {
        $input = $request->all();
        Excel::import(new ShopeeTransactionsImport(), $request->file('file'));
        return redirect(route('adminShopeeTransactions'));
    }
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/shopee_transactions', array('as'=>'adminShopeeTransactions','uses'=>'Admin\ShopeeTransactionController@index'));
Route::get('admin/shopee_transactions/create', array('as'=>'adminShopeeTransactionsCreate','uses'=>'Admin\ShopeeTransactionController@create'));
Route::post('admin/shopee_transactions/', array('as'=>'adminShopeeTransactionsStore','uses'=>'Admin\ShopeeTransactionController@store'));
Route::get('admin/shopee_transactions/{id}/show', array('as'=>'adminShopeeTransactionsShow','uses'=>'Admin\ShopeeTransactionController@show'));
Route::get('admin/shopee_transactions/{id}/view', array('as'=>'adminShopeeTransactionsView','uses'=>'Admin\ShopeeTransactionController@view'));
Route::get('admin/shopee_transactions/{id}/edit', array('as'=>'adminShopeeTransactionsEdit','uses'=>'Admin\ShopeeTransactionController@edit'));
Route::patch('admin/shopee_transactions/{id}', array('as'=>'adminShopeeTransactionsUpdate','uses'=>'Admin\ShopeeTransactionController@update'));
Route::post('admin/shopee_transactions/seo', array('as'=>'adminShopeeTransactionsSeo','uses'=>'Admin\ShopeeTransactionController@seo'));
Route::delete('admin/shopee_transactions/destroy', array('as'=>'adminShopeeTransactionsDestroy','uses'=>'Admin\ShopeeTransactionController@destroy'));
*/
}
