<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PageContentItemRequest;

use Acme\Facades\Activity;
use Acme\Facades\General;
use App\PageContentItem;
use App\Seo;
use App\PageContent;
use App\PageControl;
use App\Page;
use App\PageTemplate;
use App\PageTemplateItem;

use Carbon;
use Illuminate\Support\Facades\Schema;

class PageContentItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        if ($content_id = $request->content_id) {
            $data = PageContentItem::where('content_id', 'LIKE', '%' . $content_id . '%')->orderBy('order', 'ASC')->paginate(25);
        } else {
            $data = PageContentItem::orderBy('order', 'ASC')->paginate(25);
        }
        $pagination = $data->appends($request->except('page'))->links();

        return view('admin/page_content_items/index')
            ->with('title', 'PageContentItems')
            ->with('menu', 'page_content_items')
            ->with('keyword', $request->content_id)
            ->with('data', $data)
            ->with('pagination', $pagination);
    }

    public function create($contentID = 0)
    {
        $content = PageContent::with('pageGroup')->find($contentID);
		$page = $content->pageGroup;

        if(!$content) {
            return redirect('/');
        }
        return view('admin/dev/page_content_items/create')
            ->with('title', 'Create page_content_item')
            ->with('menu', 'page_content_items')
            ->with('page', $page)
            ->with('content', $content);
    }
    
    public function store(PageContentItemRequest $request)
    {
        $input = $request->all();

        if(isset($input['enabled'])) $input['enabled'] = 1;
		else $input['enabled'] = 0;

		if(isset($input['editable'])) $input['editable'] = 1;
		else $input['editable'] = 0;

		$input['name']=$input['item_name'];
        $item = PageContentItem::create($input);

        $response = [
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to edit.',
            'notifStatus'=>'success',
            'redirect'=>route('adminPageContentItemsEdit', [$item->id])
        ];

        return response()->json($response);
        // $page_content_item = PageContentItem::create($input);
        
		// $input['slug'] = General::slug($page_content_item->content_id,$page_content_item->id);
		// $page_content_item->update($input);

        // // $log = 'creates a new page_content_item "' . $page_content_item->name . '"';
        // // Activity::create($log);

        // $response = [
        //     'notifStatus'=>'success',
        //     'notifTitle'=>'Save successful.',
        //     'notifMessage'=>'Redirecting to edit.',
        //     'resetForm'=>true,
        //     'redirect'=>route('adminPageContentItemsEdit', [$page_content_item->id])
        // ];

        // return response()->json($response);
    }
    
    public function show($id)
    {
        return view('admin/page_content_items/show')
            ->with('title', 'Show page_content_item')
            ->with('data', PageContentItem::findOrFail($id));
    }

    public function view($id)
    {
        return view('admin/page_content_items/view')
            ->with('title', 'View page_content_item')
            ->with('menu', 'page_content_items')
            ->with('data', PageContentItem::findOrFail($id));
    }
    
    public function edit($id)
    {
        $data = PageContentItem::with('contentGroup.pageGroup')->findOrFail($id);
        $seo = $data->seo()->first();
		// return $data;
        return view('admin/dev/page_content_items/edit')
            ->with('title', 'Edit Content Item')
            ->with('menu', 'dev-pages')
            ->with('content', $data->contentGroup)
            ->with('page', $data->contentGroup->pageGroup)
            ->with('data', $data);
    }
        
    //API function for ordering items
    public function order(Request $request)
    {
        $input=[];
        $data = $request->input('page_content_items');
        $newOrder=1;
        foreach($data as $d)
        {
            $input['order'] = $newOrder;
            $page_content_item = PageContentItem::findOrFail($d);
            $page_content_item->update($input);
            $newOrder++;
        }

         $response = [
            'notifTitle'=>'Order updated.',
        ];
        return response()->json($response);
    }

    public function update(Request $request, $id)
    {
		$input = $request->all();
		// return $input;
		// return strval(Schema::hasColumn('contacts', 'subject'));
        $content = PageContent::findOrFail($input['content_id']);
        $item = PageContentItem::findOrFail($id);
        
        $hasError = false;
        if (isset($input['control']) && count(@$input['control'])) {
			if ($this->array_has_dupes($input['name'])) {
				$hasError = true;
				$response = [
					'notifTitle'=>'Saving Failed.',
					'notifMessage'=>'Duplicate form control name.',
					'notifStatus'=>'error'
				];
			} else {
				$controlsAsOf = $item->controls->toArray();
				foreach ($input['control'] as $key => $control_id) {
					$control = $item->controls()->find($control_id);
					if (!$control) {
						$control = new PageControl;
					}
					$control->name = preg_replace('/\s+/', '', $input['name'][$key]);
					$control->label = $input['label'][$key];
					$control->type = $input['type'][$key];
					$control->order = $key;
					$control->required = 0;
					if (isset($input['required']) && count(@$input['required'])) {
						$requiredKey = array_search($control_id, $input['required']);
						if ($requiredKey !== false) $control->required = 1;
					}
					if ($input['type'][$key] == 'select') $control->options_json = $input['options_json'][$key];
					$item->controls()->save($control);

					foreach ($controlsAsOf as $cKey => $cItem) {
						if ($cItem['id'] == $input['control'][$key]) $controlsAsOf[$cKey]['exists'] = true;
					}
				}
				
				foreach ($controlsAsOf as $key => $value) {
					if (!(@$value['exists'] == true)) {
						$control = $item->controls()->where('name', $value['name'])->first();
						$control->delete();
					}
				}
			}
		} else {
			$item->controls()->delete();
		}

		if (!$hasError) {
            $update = [];
            $update['published'] = @$input['published'];
			$update['name'] = @$input['item_name'];
			$update['slug'] = @$input['slug'];
			if ($content->allow_add_items === 1) {
				$update['enabled'] = 1;
				$update['editable'] = 1;
				$update['slug'] = 'template-default';
			} else {
				if(isset($input['enabled'])) $update['enabled'] = 1;
				else $update['enabled'] = 0;
				
				if(isset($input['editable'])) $update['editable'] = 1;
				else $update['editable'] = 0;
			}
			
			$item->update($update);
		
			$response = [
				'notifTitle'=>'Save successful.',
				'notifMessage'=>'Refreshing page.',
				'notifStatus'=>'success',
				'redirect'=>route('adminPageContentItemsEdit', [$item->id])
			];
		}
        // $page_content_item = PageContentItem::findOrFail($id);
        // $page_content_item->update($input);

        // // $log = 'edits a page_content_item "' . $page_content_item->name . '"';
        // // Activity::create($log);

        // $response = [
        //     'notifTitle'=>'Save successful.',
        // ];

        return response()->json($response);
    }
    function array_has_dupes($array) {
		return count($array) != count(array_unique($array));
	}
    public function seo(Request $request)
    {
        $input = $request->all();

        $data = PageContentItem::findOrFail($input['seoable_id']);
        $seo = Seo::whereSeoable_id($input['seoable_id'])->whereSeoable_type($input['seoable_type'])->first();
        if (is_null($seo)) {
            $seo = new Seo;
        }
        $seo->title = $input['title'];
        $seo->description = $input['description'];
        $seo->image = $input['image'];
        $data->seo()->save($seo);

        $response = [
            'notifTitle'=>'SEO Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        // $data = PageContentItem::whereIn('id', $input['ids'])->get();
        // $names = [];
        // foreach ($data as $d) {
        //     $names[] = $d->content_id;
        // }
        // $log = 'deletes a new page_content_item "' . implode(', ', $names) . '"';
        // Activity::create($log);

        PageContentItem::destroy($input['id']);

        // $response = [
        //     'notifTitle'=>'Delete successful.',
        //     'notifMessage'=>'Refreshing page.',
        //     'redirect'=>route('adminPageContentItems')
        // ];
        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'notifStatus'=>'success',
            'redirect'=>route('adminPageContentsEdit', $input['id'])
        ];

        return response()->json($response);
    }
    public function clientEdit(Request $request, $slug, $content, $id)
	{
        // dd($slug);
		$page = Page::where('slug', $slug)->first();
		$content = PageContent::findOrFail($content);
		$item = PageContentItem::findOrFail($id);

		if ($page->allow_add_contents == 1) {
			$contentTemplate = PageContent::where('page_id', $page->id)->where('slug', 'template-default')->first();
			if ($contentTemplate) {
				$data = PageContentItem::where('content_id', $contentTemplate->id)->where('slug', 'template-default')->first();
				foreach ($data->controls as $key => $control) {
					$control->value = @$item->controls()->where('name', $control->name)->first()->value;
				}
			} 
		} else {
			if($content->allow_add_items == 1) {
				$data = PageContentItem::where('content_id', $content['id'])->where('slug', 'template-default')->first();
				foreach ($data->controls as $key => $control) {
					$control_value = @$item->controls()->where('name', $control->name)->first();
					$control->value = @$control_value->value;
					$control->products = @$control_value->products;
				}
			}  else {
				$data = $item;
			}
		}
		
		$title = '';

		if($data->editable == 1){
			$title = 'Edit Item';
		}else{
			$title = 'View Item';
		}

		return view('admin/page_content_items/edit')
            ->with('title', $title )
            ->with('menu', 'page-'.$slug)
            ->with('page', $page)
            ->with('content', $content)
            ->with('item', $item)
            ->with('data', $data);
	}
	public function clientUpdate(Request $request, $slug, $content, $id)
	{
		$input = $request->all();
		$content = PageContent::with('controls')->findOrFail($content);
		$item = PageContentItem::findOrFail($id);

		if (!@$item->slug) {
			$item->slug = uniqid() . $item->id;
			$item->save();
		}
		
		$hasError = false;
		$message = '';
		
		if(@$input['controls']) {
			$controls = explode(',', $input['controls']);
			
			foreach($controls as $ctrl) {
				$control_input = json_decode(base64_decode($ctrl));
				$itemControl = $item->controls()->where('name', $control_input->name)->first();
				
				if (!$itemControl) {
					$itemControl = new PageControl;
					$itemControl->reference_id = $item->id;
					$itemControl->reference_type = 'App\PageContentItem';
					$itemControl->name = $control_input->name;
					$itemControl->type = $control_input->type;
					$itemControl->label = $control_input->label;
					$itemControl->required = $control_input->required;
					$itemControl->order = $control_input->order;
					$itemControl->save();
				}
			}
		}

		$form = PageContentItem::with('controls')->findOrFail($id);

		if ($content->allow_add_items == 1 && $form == null) {
			$form = PageContentItem::where('content_id', $content['id'])->where('slug', 'template-default')->first();
		}
		
		foreach ($form->controls as $key => $control) {
			$itemControl = $item->controls()->where('name', $control->name)->first();
			if (!$itemControl) {
				$itemControl = $control->replicate();
				$itemControl->reference_id = $item->id;
			}

			if (isset($input[$control->name])) {
				if ($control->required && $control->type != 'checkbox' && @$input[$control->name] == '') {
					$message .= $control->label . ' is required. ';
					$hasError = true;
				} else {
					if($control->type == 'date') {
						$input[$control->name] = Carbon::parse($input[$control->name])->format('Y-m-d');
					}
					$itemControl->value = @$input[$control->name];
				}
			} else {
				$itemControl->value = "";
			}

			$itemControl->save();
			
			// General::saveTranslations($input, $itemControl->name, 'App\PageContentItem', $itemControl->id);
		}
		
		if ($hasError) {
			$response = [
					'notifTitle'=>'Save failed.',
					'notifMessage'=>$message,
					'notifStatus'=>'error'
				];
		} else {
			$response = [
				'notifTitle'=>'Save successful.',
				'notifStatus'=>'success',
			];
		}

        return response()->json($response);
	}
    // public function clientUpdate(Request $request, $slug, $content, $id)
	// {
    //     $input = $request->all();
    //     // dd($input);
	// 	$content = PageContent::with('controls')->findOrFail($content);
		
	// 	$item = PageContentItem::findOrFail($id);
       
	// 	if (!@$item->slug) {
	// 		$item->slug = uniqid() . $item->id;
	// 		$item->save();
	// 	}
		

	// 	$hasError = false;
	// 	$message = '';
		
	// 	if(@$input['controls']) {
	// 		$controls = explode(',', $input['controls']);
			
	// 		foreach($controls as $ctrl) {
	// 			$control_input = json_decode(base64_decode($ctrl));
	// 			$itemControl = $item->controls()->where('name', $control_input->name)->first();
				
	// 			if (!$itemControl) {
	// 				$itemControl = new PageControl;
	// 				$itemControl->reference_id = $item->id;
	// 				$itemControl->reference_type = 'App\PageContentItem';
	// 				$itemControl->name = $control_input->name;
	// 				$itemControl->type = $control_input->type;
	// 				$itemControl->label = $control_input->label;
	// 				$itemControl->required = $control_input->required;
	// 				$itemControl->order = $control_input->order;
	// 				$itemControl->save();
	// 			}
	// 		}
	// 	}
	// 	$form = PageContentItem::with('controls')->where('content_id',$content->id)->firstOrFail();
	// 	// $form = PageContentItem::with('controls')->where('content_id', $content->id)->where('slug', $content->slug)->firstOrFail();
	// 	// $form = PageContentItem::with('controls')->where('content_id', $content->id)->where('slug', 'template-default')->firstOrFail();
		
	// 	if ($content->allow_add_items == 1 && $form == null) {
	// 		$form = PageContentItem::where('content_id', $content['id'])->first();
	// 	}

	// 	foreach ($form->controls as $key => $control) {
	// 		$itemControl = $item->controls()->where('name', $control->name)->first();
			
	// 		if (!$itemControl) {
	// 			$itemControl = new PageControl;
	// 			$itemControl->reference_id = $item->id;
	// 			$itemControl->reference_type = $control->reference_type;
	// 			$itemControl->name = $control->name;
	// 			$itemControl->label = $control->label;
	// 			$itemControl->options_json = $control->options_json;
	// 			$itemControl->required = $control->required;
	// 			$itemControl->order = $control->order;
	// 		}
			
	// 		if (isset($input[$control->name])) {
	// 			if ($itemControl->type == "products" && isset($input["products"])) {
	// 				if (isset($input["products"][$itemControl->id])) {
	// 					$itemControl->products()->sync($input["products"][$itemControl->id]);
	// 				}
	// 			} else {
	// 				if ($control->required && $control->type !== 'checkbox' && @$input[$control->name] == '') {
	// 					$message .= $control->label . ' is required. ';
	// 					$hasError = true;
	// 				} else {
	// 					if($control->type == 'date') {
	// 						$input[$control->name] = Carbon::parse($input[$control->name])->format('Y-m-d');
	// 					}
	
	// 					$itemControl->value = @$input[$control->name];
	// 				}
	// 			}
	// 		} 
	// 		// else {
	// 		// 	if (isset($input["products"])) {
	// 		// 		$default_content_item = PageContentItem::where('content_id', $content['id'])->where('slug', 'template-default')->first();
	// 		// 		$default_control = $default_content_item->controls()->where('name', $control->name)->first();
	// 		// 		if (isset($input["products"][$default_control->id])) {
	// 		// 			$itemControl->products()->sync($input["products"][$default_control->id]);
	// 		// 		} else {
	// 		// 			$contentControl->value = "products";
	// 		// 		}
	// 		// 	} else {
	// 		// 		if ($control->type == "products") {
	// 		// 			$contentControl->value = "products";
	// 		// 		} else {
	// 		// 			$itemControl->value = "";
	// 		// 		}
	// 		// 	}
			
			
	// 		// }
	// 		return $itemControl;
	// 		$itemControl->save();
	// 		// General::saveTranslations($input, $itemControl->name, 'App\PageContentItem', $itemControl->id);
	// 	}
		
	// 	if ($hasError) {
	// 		$response = [
	// 				'notifTitle'=>'Save failed.',
	// 				'notifMessage'=>$message,
	// 				'notifStatus'=>'error'
	// 			];
	// 	} else {
	// 		$response = [
	// 			'notifTitle'=>'Save successful.',
	// 			'notifStatus'=>'success',
	// 		];
	// 	}

    //     return response()->json($response);
	// }
    public function clientSort(Request $request, $slug, $content)
	{
		$input = $request->all();

		if (isset($input['order'])) {
			$newOrder=1;
			foreach($input['order'] as $id)
			{
				$input['order'] = $newOrder;
				$item = PageContentItem::findOrFail($id);
				$item->update($input);
				$newOrder++;
			}
		}

        $response = [
            'notifTitle'=>'Items order updated.',
            'notifStatus'=>'success',
        ];

        return response()->json($response);
    }
    
    public function clientCreate($slug, $content)
	{
		$page = Page::where('slug', $slug)->first();
		$content = PageContent::findOrFail($content);
		// return $content;
		if ($page->allow_add_contents == 1) {
			$contentTemplate = PageContent::where('page_id', $page->id)->where('slug', 'template-default')->first();
			if ($contentTemplate) {
				$data = PageContentItem::where('content_id', $contentTemplate->id)->where('slug', 'template-default')->first();
			}
		} else {
			if ($content->allow_add_items == 1) {
				
				$data = PageContentItem::where('content_id', $content->id)->first();
			} else {
				$data = $content;
			}
		}
		
		return view('admin/page_content_items/create')
            ->with('title', 'Create Item')
            ->with('menu', 'page-'.$slug)
            ->with('page', $page)
            ->with('content', $content)
            ->with('data', $data);
    }
    
    public function clientStore(Request $request, $slug, $content) 
	{
        $input = $request->all();
		$page = Page::where('slug', $slug)->first();
		$pageContent = PageContent::findOrFail($content);
		if ($page->allow_add_contents == 1) {
			$contentTemplate = PageContent::where('page_id', $page->id)->where('slug', 'template-default')->first();

			if ($contentTemplate) {
				$template = PageContentItem::where('content_id', $contentTemplate->id)->where('slug', 'template-default')->first();
			} else {
				$response = [
					'notifTitle'=>'Error Saving.',
					'notifMessage'=>'Template for content not found.',
					'notifStatus'=>'error'
				];

				return response()->json($response);
			}
		} else {
			$template = PageContentItem::where('content_id', $content)->where('slug', 'template-default')->first();
		}

		$new = $template->replicate();
		$new->content_id = $content;
		$new->order = count($pageContent->items);
		$new->save();
		$new->slug = uniqid() . $new->id;
		$new->save();
		
		foreach ($template->controls as $key => $control) {
			$newControl = $control->replicate();
			$newControl->reference_id = $new->id;
			$newControl->value = @$input[$control->name];

			if ($newControl->type == "products") {
				$newControl->value = "products";
			}
			
			$newControl->save();

			if ($newControl->type == "products") {

				if (isset($input["products"])) {
					$default_control = $template->controls()->where('name', $control->name)->first();
	
					if (isset($input["products"][$default_control->id])) {
						$newControl->products()->sync($input["products"][$default_control->id]);
					}
				}
			}
		}

		$response = [
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to edit.',
            'notifStatus'=>'success',
            'redirect'=>route('adminClientPageContentItemEdit', [$slug, $content, $new->id])
        ];

        return response()->json($response);
    }
    
    public function clientDestroy(Request $request, $id)
	{
		$item = PageContentItem::findOrFail($id);
		$item->controls()->delete();
		$item->delete();

		$response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'notifStatus'=>'success',
        ];

        return response()->json($response);
	}
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/page_content_items', array('as'=>'adminPageContentItems','uses'=>'Admin\PageContentItemController@index'));
Route::get('admin/page_content_items/create', array('as'=>'adminPageContentItemsCreate','uses'=>'Admin\PageContentItemController@create'));
Route::post('admin/page_content_items/', array('as'=>'adminPageContentItemsStore','uses'=>'Admin\PageContentItemController@store'));
Route::get('admin/page_content_items/{id}/show', array('as'=>'adminPageContentItemsShow','uses'=>'Admin\PageContentItemController@show'));
Route::get('admin/page_content_items/{id}/view', array('as'=>'adminPageContentItemsView','uses'=>'Admin\PageContentItemController@view'));
Route::get('admin/page_content_items/{id}/edit', array('as'=>'adminPageContentItemsEdit','uses'=>'Admin\PageContentItemController@edit'));
Route::patch('admin/page_content_items/{id}', array('as'=>'adminPageContentItemsUpdate','uses'=>'Admin\PageContentItemController@update'));
Route::post('admin/page_content_items/seo', array('as'=>'adminPageContentItemsSeo','uses'=>'Admin\PageContentItemController@seo'));
Route::delete('admin/page_content_items/destroy', array('as'=>'adminPageContentItemsDestroy','uses'=>'Admin\PageContentItemController@destroy'));
Route::get('admin/page_content_items/order', array('as'=>'adminPageContentItemsOrder','uses'=>'Admin\PageContentItemController@order'));
*/
}
