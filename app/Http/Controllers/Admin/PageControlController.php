<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PageControlRequest;

use Acme\Facades\Activity;

use App\PageControl;
use App\Seo;

class PageControlController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        if ($reference_id = $request->reference_id) {
            $data = PageControl::where('reference_id', 'LIKE', '%' . $reference_id . '%')->orderBy('order', 'ASC')->paginate(25);
        } else {
            $data = PageControl::orderBy('order', 'ASC')->paginate(25);
        }
        $pagination = $data->appends($request->except('page'))->links();

        return view('admin/page_controls/index')
            ->with('title', 'PageControls')
            ->with('menu', 'page_controls')
            ->with('keyword', $request->reference_id)
            ->with('data', $data)
            ->with('pagination', $pagination);
    }

    public function create()
    {
        return view('admin/page_controls/create')
            ->with('title', 'Create page_control')
            ->with('menu', 'page_controls');
    }
    
    public function store(PageControlRequest $request)
    {
        $input = $request->all();
        $page_control = PageControl::create($input);
        
        // $log = 'creates a new page_control "' . $page_control->name . '"';
        // Activity::create($log);

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to edit.',
            'resetForm'=>true,
            'redirect'=>route('adminPageControlsEdit', [$page_control->id])
        ];

        return response()->json($response);
    }
    
    public function show($id)
    {
        return view('admin/page_controls/show')
            ->with('title', 'Show page_control')
            ->with('data', PageControl::findOrFail($id));
    }

    public function view($id)
    {
        return view('admin/page_controls/view')
            ->with('title', 'View page_control')
            ->with('menu', 'page_controls')
            ->with('data', PageControl::findOrFail($id));
    }
    
    public function edit($id)
    {
        $data = PageControl::findOrFail($id);
        $seo = $data->seo()->first();

        return view('admin/page_controls/edit')
            ->with('title', 'Edit page_control')
            ->with('menu', 'page_controls')
            ->with('data', $data)
            ->with('seo', $seo);
    }
        
    //API function for ordering items
    public function order(Request $request)
    {
        $input=[];
        $data = $request->input('page_controls');
        $newOrder=1;
        foreach($data as $d)
        {
            $input['order'] = $newOrder;
            $page_control = PageControl::findOrFail($d);
            $page_control->update($input);
            $newOrder++;
        }

         $response = [
            'notifTitle'=>'PageControl order updated.',
        ];
        return response()->json($response);
    }

    public function update(PageControlRequest $request, $id)
    {
        $input = $request->all();
        $page_control = PageControl::findOrFail($id);
        $page_control->update($input);

        // $log = 'edits a page_control "' . $page_control->name . '"';
        // Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }

    public function seo(Request $request)
    {
        $input = $request->all();

        $data = PageControl::findOrFail($input['seoable_id']);
        $seo = Seo::whereSeoable_id($input['seoable_id'])->whereSeoable_type($input['seoable_type'])->first();
        if (is_null($seo)) {
            $seo = new Seo;
        }
        $seo->title = $input['title'];
        $seo->description = $input['description'];
        $seo->image = $input['image'];
        $data->seo()->save($seo);

        $response = [
            'notifTitle'=>'SEO Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        $data = PageControl::whereIn('id', $input['ids'])->get();
        $names = [];
        foreach ($data as $d) {
            $names[] = $d->reference_id;
        }
        // $log = 'deletes a new page_control "' . implode(', ', $names) . '"';
        // Activity::create($log);

        PageControl::destroy($input['ids']);

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminPageControls')
        ];

        return response()->json($response);
    }
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/page_controls', array('as'=>'adminPageControls','uses'=>'Admin\PageControlController@index'));
Route::get('admin/page_controls/create', array('as'=>'adminPageControlsCreate','uses'=>'Admin\PageControlController@create'));
Route::post('admin/page_controls/', array('as'=>'adminPageControlsStore','uses'=>'Admin\PageControlController@store'));
Route::get('admin/page_controls/{id}/show', array('as'=>'adminPageControlsShow','uses'=>'Admin\PageControlController@show'));
Route::get('admin/page_controls/{id}/view', array('as'=>'adminPageControlsView','uses'=>'Admin\PageControlController@view'));
Route::get('admin/page_controls/{id}/edit', array('as'=>'adminPageControlsEdit','uses'=>'Admin\PageControlController@edit'));
Route::patch('admin/page_controls/{id}', array('as'=>'adminPageControlsUpdate','uses'=>'Admin\PageControlController@update'));
Route::post('admin/page_controls/seo', array('as'=>'adminPageControlsSeo','uses'=>'Admin\PageControlController@seo'));
Route::delete('admin/page_controls/destroy', array('as'=>'adminPageControlsDestroy','uses'=>'Admin\PageControlController@destroy'));
Route::get('admin/page_controls/order', array('as'=>'adminPageControlsOrder','uses'=>'Admin\PageControlController@order'));
*/
}
