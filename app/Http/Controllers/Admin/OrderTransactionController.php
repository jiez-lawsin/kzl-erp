<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderTransactionRequest;

use Acme\Facades\Activity;

use App\OrderTransaction;
use App\Seo;

class OrderTransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        if ($order_number = $request->order_number) {
            $data = OrderTransaction::where('order_number', 'LIKE', '%' . $order_number . '%')->paginate(25);
        } else {
            $data = OrderTransaction::paginate(25);
        }
        $pagination = $data->appends($request->except('page'))->links();

        return view('admin/order_transactions/index')
            ->with('title', 'OrderTransactions')
            ->with('menu', 'order_transactions')
            ->with('keyword', $request->order_number)
            ->with('data', $data)
            ->with('pagination', $pagination);
    }

    public function create()
    {
        return view('admin/order_transactions/create')
            ->with('title', 'Create order_transaction')
            ->with('menu', 'order_transactions');
    }
    
    public function store(OrderTransactionRequest $request)
    {
        $input = $request->all();
        $order_transaction = OrderTransaction::create($input);
        
        // $log = 'creates a new order_transaction "' . $order_transaction->name . '"';
        // Activity::create($log);

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to edit.',
            'resetForm'=>true,
            'redirect'=>route('adminOrderTransactionsEdit', [$order_transaction->id])
        ];

        return response()->json($response);
    }
    
    public function show($id)
    {
        return view('admin/order_transactions/show')
            ->with('title', 'Show order_transaction')
            ->with('data', OrderTransaction::findOrFail($id));
    }

    public function view($id)
    {
        return view('admin/order_transactions/view')
            ->with('title', 'View order_transaction')
            ->with('menu', 'order_transactions')
            ->with('data', OrderTransaction::findOrFail($id));
    }
    
    public function edit($id)
    {
        $data = OrderTransaction::findOrFail($id);
        $seo = $data->seo()->first();

        return view('admin/order_transactions/edit')
            ->with('title', 'Edit order_transaction')
            ->with('menu', 'order_transactions')
            ->with('data', $data)
            ->with('seo', $seo);
    }
    
    public function update(OrderTransactionRequest $request, $id)
    {
        $input = $request->all();
        $order_transaction = OrderTransaction::findOrFail($id);
        $order_transaction->update($input);

        // $log = 'edits a order_transaction "' . $order_transaction->name . '"';
        // Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }

    public function seo(Request $request)
    {
        $input = $request->all();

        $data = OrderTransaction::findOrFail($input['seoable_id']);
        $seo = Seo::whereSeoable_id($input['seoable_id'])->whereSeoable_type($input['seoable_type'])->first();
        if (is_null($seo)) {
            $seo = new Seo;
        }
        $seo->title = $input['title'];
        $seo->description = $input['description'];
        $seo->image = $input['image'];
        $data->seo()->save($seo);

        $response = [
            'notifTitle'=>'SEO Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        $data = OrderTransaction::whereIn('id', $input['ids'])->get();
        $names = [];
        foreach ($data as $d) {
            $names[] = $d->order_number;
        }
        // $log = 'deletes a new order_transaction "' . implode(', ', $names) . '"';
        // Activity::create($log);

        OrderTransaction::destroy($input['ids']);

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminOrderTransactions')
        ];

        return response()->json($response);
    }
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/order_transactions', array('as'=>'adminOrderTransactions','uses'=>'Admin\OrderTransactionController@index'));
Route::get('admin/order_transactions/create', array('as'=>'adminOrderTransactionsCreate','uses'=>'Admin\OrderTransactionController@create'));
Route::post('admin/order_transactions/', array('as'=>'adminOrderTransactionsStore','uses'=>'Admin\OrderTransactionController@store'));
Route::get('admin/order_transactions/{id}/show', array('as'=>'adminOrderTransactionsShow','uses'=>'Admin\OrderTransactionController@show'));
Route::get('admin/order_transactions/{id}/view', array('as'=>'adminOrderTransactionsView','uses'=>'Admin\OrderTransactionController@view'));
Route::get('admin/order_transactions/{id}/edit', array('as'=>'adminOrderTransactionsEdit','uses'=>'Admin\OrderTransactionController@edit'));
Route::patch('admin/order_transactions/{id}', array('as'=>'adminOrderTransactionsUpdate','uses'=>'Admin\OrderTransactionController@update'));
Route::post('admin/order_transactions/seo', array('as'=>'adminOrderTransactionsSeo','uses'=>'Admin\OrderTransactionController@seo'));
Route::delete('admin/order_transactions/destroy', array('as'=>'adminOrderTransactionsDestroy','uses'=>'Admin\OrderTransactionController@destroy'));
*/
}
