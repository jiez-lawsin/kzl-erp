<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\LazadaTransactionRequest;

use Acme\Facades\Activity;
use App\Imports\LazadaTransactionsImport;
use App\LazadaTransaction;
use App\Order;
use App\Seo;

use Excel;
use Carbon;

class LazadaTransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        $input = $request->all();
        $data = LazadaTransaction::select('lazada_transactions.*', 'order_items.name as item_name', 'order_items.master_sku', 'order_items.capital_price', 'order_items.discounted_price', 'master_products.name as master_product_name', 'master_products.variant_name')->with('order.items')
                ->leftJoin('order_items', function($join) {
                    $join->on('lazada_transactions.order_number', '=', 'order_items.order_number');
                    $join->on('lazada_transactions.seller_sku', '=', 'order_items.seller_sku');
                })
                ->leftJoin('master_products', function($join) {
                    $join->on('master_products.master_sku', '=', 'order_items.master_sku');
                })
                ->orderBy('lazada_transactions.transaction_date', 'desc');
        if ($order_number = $request->order_number) {
            $data = $data->where('lazada_transactions.order_number', 'LIKE', '%' . $order_number . '%');
        }
        if ($master_sku = $request->master_sku) {
            $data = $data->where(function($query) use ($master_sku) {
                        return $query->where('order_items.master_sku', 'LIKE', $master_sku . '%');
                    });
        }
        if ($product_name = $request->product_name) {
            $data = $data->where(function($query) use ($product_name) {
                        return $query->where('order_items.name', 'LIKE', '%' . $product_name . '%')->orWhere('master_products.name', 'LIKE', '%' . $product_name . '%');
                    });
        }
        if ($date_range = $request->date_range) {
            $date_range = explode(" - ", $date_range);
            if (@$date_range[0] && @$date_range[1]) {
                $start = Carbon::parse($date_range[0]);
                $end = Carbon::parse($date_range[1]);
                $data = $data->where('lazada_transactions.transaction_date', '>=', $start)->where('lazada_transactions.transaction_date', '<=', $end);
            }
        }
        if ($store = $request->store) {
            $data = $data->whereHas('order', function($query) use ($store) {
                        $query->where('store_name', $store);
                    });
        }
        // if ($channel = $request->channel) {
        //     $data = $data->whereHas('order', function($query) use ($channel) {
        //         $query->where('channel', $channel);
        //     });
        // }
        if ($status = $request->status) {
            $data = $data->where('lazada_transactions.paid_status', $status);
        }
        if ($statement = $request->statement) {
            $data = $data->where('lazada_transactions.statement', 'LIKE', $statement);
        }

        $byOrders = clone $data;
        $transactions = clone $data;
        $data = $data->paginate(25);
        $pagination = $data->appends($request->except('page'))->links();
        $byOrders = $byOrders->groupBy('lazada_transactions.order_number')->get();
        $statements = LazadaTransaction::select('statement', 'transaction_date')->groupBy('statement')->orderBy('lazada_transactions.transaction_date', 'desc')->get();;
        $stores = Order::select('store_name', 'channel')->groupBy('store_name')->get();
        $stores = Order::select('store_name', 'channel')->groupBy('store_name')->get();
        return view('admin/lazada_transactions/index')
            ->with('title', 'Lazada Transactions')
            ->with('menu', 'lazada_transactions')
            ->with('keyword', $request->order_number)
            ->with('statements', $statements)
            ->with('stores', $stores)
            ->with('byOrders', $byOrders)
            ->with('data', $data)
            ->with('transactions', $transactions->get())
            ->with('request', $input)
            ->with('pagination', $pagination);
    }

    public function create()
    {
        return view('admin/lazada_transactions/create')
            ->with('title', 'Create lazada_transaction')
            ->with('menu', 'lazada_transactions');
    }
    
    public function store(LazadaTransactionRequest $request)
    {
        $input = $request->all();
        $lazada_transaction = LazadaTransaction::create($input);
        
        // $log = 'creates a new lazada_transaction "' . $lazada_transaction->name . '"';
        // Activity::create($log);

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to edit.',
            'resetForm'=>true,
            'redirect'=>route('adminLazadaTransactionsEdit', [$lazada_transaction->id])
        ];

        return response()->json($response);
    }
    
    public function show($id)
    {
        return view('admin/lazada_transactions/show')
            ->with('title', 'Show lazada_transaction')
            ->with('data', LazadaTransaction::findOrFail($id));
    }

    public function view($id)
    {
        return view('admin/lazada_transactions/view')
            ->with('title', 'View lazada_transaction')
            ->with('menu', 'lazada_transactions')
            ->with('data', LazadaTransaction::findOrFail($id));
    }
    
    public function edit($id)
    {
        $data = LazadaTransaction::findOrFail($id);
        $seo = $data->seo()->first();

        return view('admin/lazada_transactions/edit')
            ->with('title', 'Edit lazada_transaction')
            ->with('menu', 'lazada_transactions')
            ->with('data', $data)
            ->with('seo', $seo);
    }
    
    public function update(LazadaTransactionRequest $request, $id)
    {
        $input = $request->all();
        $lazada_transaction = LazadaTransaction::findOrFail($id);
        $lazada_transaction->update($input);

        // $log = 'edits a lazada_transaction "' . $lazada_transaction->name . '"';
        // Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }

    public function seo(Request $request)
    {
        $input = $request->all();

        $data = LazadaTransaction::findOrFail($input['seoable_id']);
        $seo = Seo::whereSeoable_id($input['seoable_id'])->whereSeoable_type($input['seoable_type'])->first();
        if (is_null($seo)) {
            $seo = new Seo;
        }
        $seo->title = $input['title'];
        $seo->description = $input['description'];
        $seo->image = $input['image'];
        $data->seo()->save($seo);

        $response = [
            'notifTitle'=>'SEO Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        $data = LazadaTransaction::whereIn('id', $input['ids'])->get();
        $names = [];
        foreach ($data as $d) {
            $names[] = $d->order_number;
        }
        // $log = 'deletes a new lazada_transaction "' . implode(', ', $names) . '"';
        // Activity::create($log);

        LazadaTransaction::destroy($input['ids']);

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminLazadaTransactions')
        ];

        return response()->json($response);
    }
    
    public function import(Request $request)
    {
        $input = $request->all();
        Excel::import(new LazadaTransactionsImport(), $request->file('file'));
        return back()->withInput();
    }
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/lazada_transactions', array('as'=>'adminLazadaTransactions','uses'=>'Admin\LazadaTransactionController@index'));
Route::get('admin/lazada_transactions/create', array('as'=>'adminLazadaTransactionsCreate','uses'=>'Admin\LazadaTransactionController@create'));
Route::post('admin/lazada_transactions/', array('as'=>'adminLazadaTransactionsStore','uses'=>'Admin\LazadaTransactionController@store'));
Route::get('admin/lazada_transactions/{id}/show', array('as'=>'adminLazadaTransactionsShow','uses'=>'Admin\LazadaTransactionController@show'));
Route::get('admin/lazada_transactions/{id}/view', array('as'=>'adminLazadaTransactionsView','uses'=>'Admin\LazadaTransactionController@view'));
Route::get('admin/lazada_transactions/{id}/edit', array('as'=>'adminLazadaTransactionsEdit','uses'=>'Admin\LazadaTransactionController@edit'));
Route::patch('admin/lazada_transactions/{id}', array('as'=>'adminLazadaTransactionsUpdate','uses'=>'Admin\LazadaTransactionController@update'));
Route::post('admin/lazada_transactions/seo', array('as'=>'adminLazadaTransactionsSeo','uses'=>'Admin\LazadaTransactionController@seo'));
Route::delete('admin/lazada_transactions/destroy', array('as'=>'adminLazadaTransactionsDestroy','uses'=>'Admin\LazadaTransactionController@destroy'));
*/
}
