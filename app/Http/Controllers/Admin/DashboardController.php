<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;

use App\Activity;
use App\LazadaTransaction;
use App\MasterProduct;
use App\Order;
use App\OrderItem;
use App\OrderTransaction;
use App\ShopeeTransaction;
use Illuminate\Support\Facades\DB;
class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index(Request $request)
    {
        $input = $request->all();
        if ($date_range = $request->date_range) {
            $date_range = explode(" - ", $date_range);
            if (@$date_range[0] && @$date_range[1]) {
                $start = Carbon::parse($date_range[0]);
                $end = Carbon::parse($date_range[1]);
            }
        } else {
            $start = Carbon::now()->startOfMonth();
            $end = Carbon::now()->endOfMonth();
            $input['date_range'] = $start->format('m/d/Y') . ' - ' . $end->format('m/d/Y');
        }
        $ordersQuery = Order::where('order_creation_date', '>=', $start)->where('order_creation_date', '<=', $end);

        $orders = clone $ordersQuery;
        $validOrders = clone $ordersQuery;
        $manualOrders = clone $ordersQuery;
        $manualOrders->where('order_creation_origin', 'manual');

        $orderItems = OrderItem::select('id', 'quantity', 'discounted_price', 'capital_price')->whereHas('order', function ($query) use ($start, $end) {
            $query->where('order_creation_date', '>=', $start)->where('order_creation_date', '<=', $end);
        });
        $validOrders->whereIn('status', ['paid', 'ready to ship', 'shipping', 'delivered']);
        $validOrderItems = OrderItem::select('id', 'quantity', 'discounted_price', 'capital_price')->whereHas('order', function ($query) use ($start, $end) {
            $query->whereIn('status', ['paid', 'ready to ship', 'shipping', 'delivered'])
                ->where('order_creation_date', '>=', $start)->where('order_creation_date', '<=', $end);
        });
        $lazTransactions = LazadaTransaction::whereHas('order', function ($query) use ($start, $end) {
            $query->where('order_creation_date', '>=', $start)->where('order_creation_date', '<=', $end);
        });
        $shopeeTransactions = ShopeeTransaction::whereHas('order', function ($query) use ($start, $end) {
            $query->where('order_creation_date', '>=', $start)->where('order_creation_date', '<=', $end);
        });

        $manualOrderAmountReceived = 0;
        // $completedTotalOrderCost = 0;
        foreach ($manualOrders->where('status', 'delivered')->get() as $mO) {
            foreach ($mO->items as $item) {
                $manualOrderAmountReceived += $item->discounted_price;
                // $completedTotalOrderCost += $item->capital_price;
            }
            foreach ($mO->order_transactions as $trx) {
                $manualOrderAmountReceived += $trx->amount;
            }
        }

        $validTotalOrderQuantity = 0;
        $validTotalOrderPriceIncome = 0;
        $validTotalOrderCost = 0;
        foreach ($validOrderItems->get() as $item) {
            $validTotalOrderQuantity += $item->quantity;
            $validTotalOrderPriceIncome += $item->discounted_price * $item->quantity;
            $validTotalOrderCost += $item->capital_price;
        }

        $stores = Order::select('store_name', 'channel')->groupBy('store_name', 'channel')->get();
        foreach ($stores as $key => $store) {
            $storeOrders = clone $orders;
            $storeValidOrders = clone $validOrders;
            $store->total_orders = $storeOrders->where('store_name', $store->store_name)->where('channel', $store->channel)->count();
            $store->valid_orders = $storeValidOrders->where('store_name', $store->store_name)->where('channel', $store->channel)->count();
            $storeValidOrderItems = OrderItem::select('id', 'quantity', 'discounted_price', 'capital_price')->whereHas('order', function ($query) use ($store, $start, $end) {
                $query->where('store_name', $store->store_name)->where('channel', $store->channel)
                    ->whereIn('status', ['paid', 'ready to ship', 'shipping', 'delivered'])
                    ->where('order_creation_date', '>=', $start)->where('order_creation_date', '<=', $end);
            });

            $store->quantity_sales = 0;
            $store->order_price_income = 0;
            $store->total_orders_cost = 0;
            foreach ($storeValidOrderItems->get() as $item) {
                $store->quantity_sales += $item->quantity;
                $store->order_price_income += $item->discounted_price * $item->quantity;
                $store->total_orders_cost += $item->capital_price;
            }
        }
        
        // $product_sales = MasterProduct::get();
        $products_sales = $orderItems->select('master_sku', 'name', DB::raw('count(*) as total'))
                        ->groupBy('master_sku')
                        ->with('master')
                        ->where('master_sku', '!=', NULL)
                        ->orderBy('total', 'desc')
                        ->get()->take(20);
        foreach($products_sales as $ps) {
            $validOIClone = clone $validOrderItems;
            $validPs = $validOIClone->where('master_sku', $ps->master_sku)->get();
            $ps->valid_orders = count($validPs);
            $ps->total_order_qty = 0;
            $ps->total_order_price_income = 0;
            foreach ($validPs as $vps) {
                $ps->total_order_qty += $vps->quantity;
                $ps->total_order_price_income += $vps->discounted_price * $vps->quantity;
            }
        }
        // dd($products_sales);
        return view('admin/dashboard/index')
            ->with('title', 'Dashboard')
            ->with('request', $input)
            ->with('orderCount', $orders->count())
            ->with('validOrdersCount', $validOrders->count())
            ->with('validTotalOrderQuantity', $validTotalOrderQuantity)
            ->with('validTotalOrderPriceIncome', $validTotalOrderPriceIncome)
            ->with('validTotalOrderCost', $validTotalOrderCost)
            // ->with('completedTotalOrderCost', $completedTotalOrderCost)
            ->with('lazTransactions', $lazTransactions->get())
            ->with('shopeeTransactions', $shopeeTransactions->get())
            ->with('manualOrderAmountReceived', $manualOrderAmountReceived)
            ->with('products_sales', $products_sales)
            ->with('stores', $stores)
            ->with('menu', 'dashboard');
    }

    public function productSales(Request $request)
    {
        $input = $request->all();
        $data = OrderItem::selectRaw('sum(quantity) as total_qty, orders.status, order_items.name as item_name, order_items.master_sku, order_items.capital_price, order_items.discounted_price, master_products.name as master_product_name, master_products.variant_name')
                ->leftJoin('master_products', function($join) {
                    $join->on('master_products.master_sku', '=', 'order_items.master_sku');
                })
                ->leftJoin('orders', function($join) {
                    $join->on('orders.order_number', '=', 'order_items.order_number');
                })
                ->where('order_items.master_sku', '!=', NULL)
                ->where('order_items.master_sku', '!=', '')
                ->whereIn('orders.status', ['paid', 'ready to ship', 'shipping', 'delivered'])
                ->groupBy('master_sku')
                ->orderBy('total_qty', 'desc');
        if ($master_sku = $request->master_sku) {
            $data = $data->where(function($query) use ($master_sku) {
                        return $query->where('order_items.master_sku', 'LIKE', $master_sku . '%');
                    });
        }
        if ($product_name = $request->product_name) {
            $data = $data->where(function($query) use ($product_name) {
                        return $query->where('order_items.name', 'LIKE', '%' . $product_name . '%')->orWhere('master_products.name', 'LIKE', '%' . $product_name . '%');
                    });
        }
        // if ($date_range = $request->date_range) {
        //     $date_range = explode(" - ", $date_range);
        //     if (@$date_range[0] && @$date_range[1]) {
        //         $start = Carbon::parse($date_range[0]);
        //         $end = Carbon::parse($date_range[1]);
        //         $data = $data->where('lazada_transactions.transaction_date', '>=', $start)->where('lazada_transactions.transaction_date', '<=', $end);
        //     }
        // }
        if ($store = $request->store) {
            $data = $data->whereHas('order', function($query) use ($store) {
                        $query->where('store_name', $store);
                    });
        }
        // if ($channel = $request->channel) {
        //     $data = $data->whereHas('order', function($query) use ($channel) {
        //         $query->where('channel', $channel);
        //     });
        // }
        // if ($status = $request->status) {
        //     $data = $data->where('lazada_transactions.paid_status', $status);
        // }
        // if ($statement = $request->statement) {
        //     $data = $data->where('lazada_transactions.statement', 'LIKE', $statement);
        // }

        $byOrders = clone $data;
        $transactions = clone $data;
        $data = $data->paginate(25);
        $pagination = $data->appends($request->except('page'))->links();
        // $byOrders = $byOrders->groupBy('lazada_transactions.order_number')->get();
        $stores = Order::select('store_name', 'channel')->groupBy('store_name')->get();
        
        return view('admin/dashboard/products')
            ->with('title', 'Products Sales')
            ->with('keyword', $request->order_number)
            ->with('stores', $stores)
            // ->with('byOrders', $byOrders)
            ->with('data', $data)
            ->with('transactions', $transactions->get())
            ->with('request', $input)
            ->with('pagination', $pagination)
                ->with('menu', 'dashboard');
    }
}
