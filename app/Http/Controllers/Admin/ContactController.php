<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;

use Acme\Facades\Activity;

use App\Contact;
use App\Page;
use App\PageContact;
use App\PageContentItem;
use App\PageControl;
use App\Seo;
use Carbon;
use DB;
use App\Exports\ContactsExport;

use Maatwebsite\Excel\Facades\Excel;
// use App\Exports\UsersExport;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function export()
    {
        return Excel::download(new ContactsExport, 'contacts.xlsx');
    }

    public function index(Request $request)
    {
        if ($name = $request->name) {
            $data = Contact::where('name', 'LIKE', '%' . $name . '%')->orderBy('order', 'ASC')->paginate(25);
        } else {
            $data = Contact::orderBy('order', 'ASC')->paginate(25);
        }
        $pagination = $data->appends($request->except('page'))->links();
        
        $test = DB::select('select created_at from contacts');
      

        
        $today = today(); 
        $dates = []; 
        $inquiries = [];
        
        for($i=1; $i < $today->daysInMonth + 1; ++$i) {
            $dates[] = Carbon::createFromDate($today->year, $today->month, $i)->format('Y-m-d');
            $dateFormat[] = Carbon::createFromDate($today->year, $today->month, $i)->format('M-d');
        }
        for($i=1; $i < $today->daysInMonth + 1; ++$i) {
            $inquiries[] = Contact::select('created_at')->whereDate('created_at', '=', $dates[$i-1])->count();
        }

        $num = Contact::whereDate('created_at', Carbon::today())->count();
        
        $subject = Page::where('slug', 'like', '%contact%')
            ->first();
        if($subject){
            $subject = $subject->pageContent->last()->items->first()->controls->where('name','subject')->first()->options_json;
        }
        $selectedSubject= "";
        return view('admin/contacts/index')
            ->with('title', 'Contacts')
            ->with('menu', 'contact_analytics')
            ->with('keyword', $request->name)
            ->with('data', $data)
            ->with('dates', $dateFormat)
            ->with('inquiries', $inquiries)
            ->with('subject', $subject)
            ->with('selectedSubject', $selectedSubject)
            ->with('inquiry',$num)
            ->with('pagination', $pagination);
    }

    public function filterByDate(Request $request)
    {
        $input = $request->all();
        $filterDate = date("Y-m-d",strtotime($input['date']));
        
        $data = Contact::whereRaw('DATE(created_at) = ?',date("Y-m-d",strtotime($input['date'])))->paginate(25);
     
        $pagination = $data->appends($request->except('page'))->links();

        $today = today(); 
        $dates = []; 
        $inquiries = [];
        $dateFormat='';
        // for($i=1; $i < $today->daysInMonth + 1; ++$i) {
        //     $dates[] = Carbon::createFromDate($today->year, $today->month, $i)->format('Y-m-d');
        //     $dateFormat[] = Carbon::createFromDate($today->year, $today->month, $i)->format('M-d');
        // }
        // for($i=1; $i < $today->daysInMonth + 1; ++$i) {
        //     $inquiries[] = Contact::select('created_at')->where('subject', $input['subject'])->whereDate('created_at', '=', $dates[$i-1])->count();
        // }
        
        $subject = Page::where('slug', 'like', '%contact%')
        ->first();
        $subject = $subject->pageContent->last()->items->first()->controls->where('name','subject')->first()->options_json;
        $selectedSubject= 'hiring';
        $num = Contact::whereDate('created_at', Carbon::today())->count();
        return view('admin/contacts/index')
        ->with('title', 'Contacts')
        ->with('menu', 'contact_analytics')
        ->with('keyword', $request->name)
        ->with('dates', $dateFormat)
        ->with('data', $data)
        ->with('filterDate', $filterDate)
        ->with('inquiries', $inquiries)
        ->with('subject', $subject)
        ->with('selectedSubject', $selectedSubject)
        ->with('inquiry',$num)
        ->with('pagination', $pagination);
    }

    public function filterResults(Request $request) 
    {
        $input = $request->all();

        $data = Contact::where('subject', $input['subject'])->paginate(25);
        $pagination = $data->appends($request->except('page'))->links();

        $today = today(); 
        $dates = []; 
        $inquiries = [];
        
        for($i=1; $i < $today->daysInMonth + 1; ++$i) {
            $dates[] = Carbon::createFromDate($today->year, $today->month, $i)->format('Y-m-d');
            $dateFormat[] = Carbon::createFromDate($today->year, $today->month, $i)->format('M-d');
        }
        for($i=1; $i < $today->daysInMonth + 1; ++$i) {
            $inquiries[] = Contact::select('created_at')->where('subject', $input['subject'])->whereDate('created_at', '=', $dates[$i-1])->count();
        }
        
        $subject = Page::where('slug', 'like', '%contact%')
        ->first();
        $subject = $subject->pageContent->last()->items->first()->controls->where('name','subject')->first()->options_json;
        $selectedSubject= $input['subject'];
        $num = Contact::whereDate('created_at', Carbon::today())->count();
        return view('admin/contacts/index')
        ->with('title', 'Contacts')
        ->with('menu', 'contact_analytics')
        ->with('keyword', $request->name)
        ->with('dates', $dateFormat)
        ->with('data', $data)
        ->with('inquiries', $inquiries)
        ->with('subject', $subject)
        ->with('selectedSubject', $selectedSubject)
        ->with('inquiry',$num)
        ->with('pagination', $pagination);
    }

    public function create()
    {
        return view('admin/contacts/create')
            ->with('title', 'Create contact')
            ->with('menu', 'contacts');
    }
    
    public function store(ContactRequest $request)
    {
        $input = $request->all();
        $contact = Contact::create($input);
        
        // $log = 'creates a new contact "' . $contact->name . '"';
        // Activity::create($log);

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to edit.',
            'resetForm'=>true,
            'redirect'=>route('adminContactsEdit', [$contact->id])
        ];

        return response()->json($response);
    }
    
    public function show($id)
    {
        return view('admin/contacts/show')
            ->with('title', 'Show contact')
            ->with('data', Contact::findOrFail($id));
    }

    public function view($id)
    {
        return view('admin/contacts/view')
            ->with('title', 'View contact')
            ->with('menu', 'contacts')
            ->with('data', Contact::findOrFail($id));
    }
    
    public function edit($id)
    {
        $data = Contact::findOrFail($id);
        $seo = $data->seo()->first();

        return view('admin/contacts/edit')
            ->with('title', 'Edit contact')
            ->with('menu', 'contacts')
            ->with('data', $data)
            ->with('seo', $seo);
    }
        
    //API function for ordering items
    public function order(Request $request)
    {
        $input=[];
        $data = $request->input('contacts');
        $newOrder=1;
        foreach($data as $d)
        {
            $input['order'] = $newOrder;
            $contact = Contact::findOrFail($d);
            $contact->update($input);
            $newOrder++;
        }

         $response = [
            'notifTitle'=>'Contact order updated.',
        ];
        return response()->json($response);
    }

    public function update(ContactRequest $request, $id)
    {
        $input = $request->all();
        $contact = Contact::findOrFail($id);
        $contact->update($input);

        // $log = 'edits a contact "' . $contact->name . '"';
        // Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }

    public function seo(Request $request)
    {
        $input = $request->all();

        $data = Contact::findOrFail($input['seoable_id']);
        $seo = Seo::whereSeoable_id($input['seoable_id'])->whereSeoable_type($input['seoable_type'])->first();
        if (is_null($seo)) {
            $seo = new Seo;
        }
        $seo->title = $input['title'];
        $seo->description = $input['description'];
        $seo->image = $input['image'];
        $data->seo()->save($seo);

        $response = [
            'notifTitle'=>'SEO Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        $data = Contact::whereIn('id', $input['ids'])->get();
        $names = [];
        foreach ($data as $d) {
            $names[] = $d->name;
        }
        // $log = 'deletes a new contact "' . implode(', ', $names) . '"';
        // Activity::create($log);

        Contact::destroy($input['ids']);

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminContacts')
        ];

        return response()->json($response);
    }
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/contacts', array('as'=>'adminContacts','uses'=>'Admin\ContactController@index'));
Route::get('admin/contacts/create', array('as'=>'adminContactsCreate','uses'=>'Admin\ContactController@create'));
Route::post('admin/contacts/', array('as'=>'adminContactsStore','uses'=>'Admin\ContactController@store'));
Route::get('admin/contacts/{id}/show', array('as'=>'adminContactsShow','uses'=>'Admin\ContactController@show'));
Route::get('admin/contacts/{id}/view', array('as'=>'adminContactsView','uses'=>'Admin\ContactController@view'));
Route::get('admin/contacts/{id}/edit', array('as'=>'adminContactsEdit','uses'=>'Admin\ContactController@edit'));
Route::patch('admin/contacts/{id}', array('as'=>'adminContactsUpdate','uses'=>'Admin\ContactController@update'));
Route::post('admin/contacts/seo', array('as'=>'adminContactsSeo','uses'=>'Admin\ContactController@seo'));
Route::delete('admin/contacts/destroy', array('as'=>'adminContactsDestroy','uses'=>'Admin\ContactController@destroy'));
Route::get('admin/contacts/order', array('as'=>'adminContactsOrder','uses'=>'Admin\ContactController@order'));
*/
}
