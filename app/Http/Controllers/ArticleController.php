<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use Acme\Facades\Seo;
use App\Sample;
use App\Article;

class ArticleController extends Controller
{

    private function getView($viewName)
    {
        if (request()->segment(1) == 'amp') {
            if (view()->exists($viewName . '-amp')) {
                $viewName .= '-amp';
            }
        }
        return $viewName;
    }

    public function index()
    {
        $canonicalLink = url()->current();

        $blog = Article::where('published', 'published')->orderBy('date', 'desc')->paginate(5);

        return view($this->getView('app/articles'))
                ->with('menu','article')
                ->with('canonicalLink', $canonicalLink)
                ->with('articles', $articles);
    }

    public function view($slug='')
    {
        $canonicalLink = url()->current();

        $article = Article::where('slug', $slug)->first();
        $seo = $article->seo()->first();


        return view($this->getView('app/article_view'))
                ->with('menu','article')
                ->with('seo', $seo)
                ->with('slug', $slug)
                ->with('canonicalLink', $canonicalLink)
                ->with('article', $article);
    }

    public function seo()
    {
        $sample = Sample::findOrFail(1);
        $seo = Seo::get($sample);

        return view('app/home');
    }
}
