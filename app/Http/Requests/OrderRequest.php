<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OrderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_number' => 'required|max:255',
            'channel' => 'required|max:255',
            'store_name' => 'required|max:255',
            'status' => 'required|max:255',
            'payment' => 'required|max:255',
            'buyer_name' => 'required|max:255',
            'sales_rep' => 'required|max:255',
            'order_creation_date' => 'required',
        ];
    }
}
