<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OrderItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_number' => 'required|max:255',
            'order_item_id' => 'required|max:255',
            'name' => 'required',
            'seller_sku' => 'required',
            'master_sku' => 'required',
            'original_price' => 'required',
            'discounted_price' => 'required',
            'capital_price' => 'required',
            'quantity' => 'required|integer',
        ];
    }
}
