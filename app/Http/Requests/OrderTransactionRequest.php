<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OrderTransactionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_number' => 'required|max:255',
            'transaction_number' => 'required|max:255',
            'transaction_date' => 'required',
            'transaction_type' => 'required|max:255',
            'fee_name' => 'required|max:255',
            'amount' => 'required',
            'paid_status' => 'required|max:255',
            'comment' => 'required',
        ];
    }
}
