<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MasterProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'master_sku' => 'required',

            'name' => 'required',
            'purchase_price' => 'required',

            'variant_name' => 'required|max:255',
        ];
    }
}
