<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PreviewRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => 'required|max:255',
            'value' => 'required',
            'status' => 'required|max:255',
            'action' => 'required|max:255',
            'reference_type' => 'required|max:255',
            'reference_id' => 'required',
        ];
    }
}
