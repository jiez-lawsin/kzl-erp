<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LazadaTransactionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_number' => 'required|max:255',
            'order_item_number' => 'required|max:255',
            'transaction_number' => 'required|max:255',
            'transaction_date' => 'required',
            'transaction_type' => 'required|max:255',
            'fee_name' => 'required|max:255',
            'seller_sku' => 'required|max:255',
            'lazada_sku' => 'required|max:255',
            'amount' => 'required',
            'vat_amount' => 'required',
            'wht_amount' => 'required',
            'wht_included' => 'required|max:255',
            'statement' => 'required|max:255',
            'paid_status' => 'required|max:255',
            'order_item_status' => 'required|max:255',
            'shipping_provider' => 'required|max:255',
            'shipping_speed' => 'required|max:255',
            'shipment_type' => 'required|max:255',
            'reference_number' => 'required|max:255',
            'payment_reference_id' => 'required|max:255',
            'comment' => 'required',
        ];
    }
}
