<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ShopeeTransactionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_number' => 'required|max:255',
            'refund_id' => 'required|max:255',
            'username' => 'required|max:255',
            'order_creation_date' => 'required',
            'payout_completed_date' => 'required',
            'original_price' => 'required',
            'seller_product_promotion' => 'required',
            'buyer_refund_amount' => 'required',
            'product_discount_rebate' => 'required',
            'seller_voucher_discount' => 'required',
            'seller_absorbed_coin_cashback' => 'required',
            'buyer_paid_shipping_fee' => 'required',
            'shipping_fee_rebate' => 'required',
            'third_party_logistics_shipping' => 'required',
            'reverse_shipping_fee' => 'required',
            'commission_fee' => 'required',
            'service_fee' => 'required',
            'transaction_fee' => 'required',
            'total_released_amount' => 'required',
            'lost_compensation' => 'required',
            'seller_shipping_fee_promotion' => 'required',
            'seller_voucher_code' => 'required',
            'shipping_provider' => 'required|max:255',
            'courier_name' => 'required|max:255',
        ];
    }
}
