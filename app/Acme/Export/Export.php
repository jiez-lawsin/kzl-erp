<?php 

namespace App\Acme\Export;

use Illuminate\Support\Collection;

use Excel;
use Carbon;



class Export
{
    /*
        Save Group of Assets 
        @param db = Instance of the related model
        @param data = json encoded data of the group to be created/updated
                      including it's assets
        @param action = download or store file
    */
    public function download($data = "", $filename = "")
    { 
        if (!$filename) {
            $filename = 'export-' . Carbon::now()->format("m-d-Y");
        }
        $file = Excel::download($data, $filename);
        return $file;
    }
    
}