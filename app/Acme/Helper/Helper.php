<?php 

namespace App\Acme\Helper;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\View;

use App\Acme\Facades\Option as OptionFacades;
use App\Option;
use Carbon;
use SumoMail;
use Auth;
use QrCode;
use Str;

class Helper
{
  public function upload_file($request, $upload_path) {
    $file_path = null;

    if ($request->hasFile('file')) {
      $file = $request->file('file');

      $origFileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
      $filename = $origFileName . '-' . str_random(8) . '.' . $file->getClientOriginalExtension();

      $file->move('.'.$upload_path, $filename);
      $file_path = $upload_path . '/' . $filename;
    }

    return $file_path;
  }
}