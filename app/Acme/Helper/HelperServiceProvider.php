<?php

namespace App\Acme\Helper;

use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider 
{
	public function register()
	{
		$this->app->bind('helper', 'App\Acme\Helper\Helper');
	}
}