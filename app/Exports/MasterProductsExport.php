<?php

namespace App\Exports;

use App\MasterProduct;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class MasterProductsExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return MasterProduct::all();
    }

    public function view(): View
    {
        return view('admin.master_products.export', [
            'master_products' => MasterProduct::all()
        ]);
    }

}
