<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class NavbarLink extends BaseModel
{
    
    protected $fillable = [
        'name',
        
        'type',

        'link',

        'reference_type',

        'reference_id',

        'hide',

        'new_tab',

        'parent_id',
        
        'order',

        'disable_link'
    	];
    
    
    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }
    
    public function activities()
    {
        return $this->morphMany('App\Activity', 'loggable');
    }

}
