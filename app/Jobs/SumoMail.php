<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Mail\SumoMail as SumoMailMailable;

use App\Email;

use Mail;
use Carbon;

class SumoMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Queue config
     */
    public $tries = 3;

    /**
     * Queue variables
     */
    public $mail_config;
    public $email_id;
    public $view_name;
    public $view_data;
    public $email_params;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $mail_config, $email_id, $view_name, array $view_data, array $email_params)
    {
        $this->mail_config = $mail_config;
        $this->email_id = $email_id;
        $this->view_name = $view_name;
        $this->view_data = $view_data;
        $this->email_params = $email_params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email_data = Email::findOrFail($this->email_id);

        try {
            \Config::set('mail.driver', $this->mail_config['driver']);
            \Config::set('mail.host', $this->mail_config['host']);
            \Config::set('mail.port', $this->mail_config['port']);
            \Config::set('mail.username', $this->mail_config['username']);
            \Config::set('mail.password', $this->mail_config['password']);
            \Config::set('mail.encryption', $this->mail_config['encryption']);

            $email = new SumoMailMailable($this->view_name, $this->view_data, $this->email_params);
            Mail::send($email);
    
            //Updates status of sent and saves sent date
            $params['status'] = 'sent';
            $date = Carbon::now();
            $date->setTimezone('UTC');
            $params['sent'] = $date;
        } catch (\Throwable $th) {
            //Updates status of failed and saves failed date
            $params['status'] = 'failed';
        }

        $email_data->update($params);
    }
}
