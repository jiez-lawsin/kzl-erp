<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class Order extends BaseModel
{
    
    protected $fillable = [
    	'order_number',

        'channel',

        'store_name',

        'status',

        'payment',

        'buyer_name',

        'recipient_name',

        'recipient_phone',

        'recipient_address',

        'courier',

        'tracking_number',

        'subtotal',

        'shipping_fee',

        'total',

        'order_creation_date',

        'order_update_date',
        'sales_rep',
        'remarks',
        'user_id',
        'order_creation_origin',
    	];
    
    
    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }
    
    public function items() 
    {
        return $this->hasMany('App\OrderItem', 'order_number', 'order_number');
    }

    public function laz_transactions() 
    {
        return $this->hasMany('App\LazadaTransaction', 'order_number', 'order_number');
    }

    public function shopee_transaction() 
    {
        return $this->hasOne('App\ShopeeTransaction', 'order_number', 'order_number');
    }
    public function order_transactions() 
    {
        return $this->hasMany('App\OrderTransaction', 'order_number', 'order_number');
    }
}
