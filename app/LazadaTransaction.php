<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class LazadaTransaction extends BaseModel
{
    use \Awobaz\Compoships\Compoships;

    protected $fillable = [
    	'order_number',

        'order_item_number',

        'transaction_number',

        'transaction_date',

        'transaction_type',

        'fee_name',

        'seller_sku',

        'lazada_sku',

        'amount',

        'vat_amount',

        'wht_amount',

        'wht_included',

        'statement',

        'paid_status',

        'order_item_status',

        'shipping_provider',

        'shipping_speed',

        'shipment_type',

        'reference_number',

        'payment_reference_id',

        'comment',
    	];
    
    
    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }
    
    public function activities()
    {
        return $this->morphMany('App\Activity', 'loggable');
    }

    public function order() 
    {
        return $this->hasOne('App\Order', 'order_number', 'order_number');
    }
    public function order_item() 
    {
        return $this->hasOne('App\OrderItem', 'order_number', 'order_number');
    }
    public function order_items() 
    {
        return $this->hasMany('App\OrderItem', 'order_number', 'order_number');
    }
    public function orders() 
    {
        return $this->hasMany('App\Order', 'order_number', 'order_number');
    }
    
}
