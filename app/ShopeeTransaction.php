<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class ShopeeTransaction extends BaseModel
{
    
    protected $fillable = [
    	'order_number',

        'refund_id',

        'username',

        'order_creation_date',

        'payout_completed_date',

        'original_price',

        'seller_product_promotion',

        'buyer_refund_amount',

        'product_discount_rebate',

        'seller_voucher_discount',

        'seller_absorbed_coin_cashback',

        'buyer_paid_shipping_fee',

        'shipping_fee_rebate',

        'third_party_logistics_shipping',

        'reverse_shipping_fee',

        'commission_fee',

        'service_fee',

        'transaction_fee',

        'total_released_amount',

        'lost_compensation',

        'seller_shipping_fee_promotion',

        'seller_voucher_code',

        'shipping_provider',

        'courier_name',
    	];
    
    
    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }

    public function order() 
    {
        return $this->hasOne('App\Order', 'order_number', 'order_number');
    }
    public function order_item() 
    {
        return $this->hasOne('App\OrderItem', 'order_number', 'order_number');
    }
    public function order_items() 
    {
        return $this->hasMany('App\OrderItem', 'order_number', 'order_number');
    }
    public function orders() 
    {
        return $this->hasMany('App\Order', 'order_number', 'order_number');
    }
}
