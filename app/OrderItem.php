<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends BaseModel
{
    
    protected $fillable = [
    	'order_number',

        'order_item_id',

        'name',

        'seller_sku',

        'master_sku',

        'original_price',

        'discounted_price',

        'capital_price',

        'quantity',
    	];
    
    
    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }
    
    public function master() 
    {
        return $this->hasOne('App\MasterProduct', 'master_sku', 'master_sku');
    }
    public function order() 
    {
        return $this->hasOne('App\Order', 'order_number', 'order_number');
    }
}
