<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class MasterProduct extends BaseModel
{
    
    protected $fillable = [
    	'master_sku',

        'name',

        'variant_name',
        'purchase_price',
    	];
    
    
    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }
    public function order_items() 
    {
        return $this->hasMany('App\OrderItem', 'master_sku', 'master_sku');
    }
    
}
