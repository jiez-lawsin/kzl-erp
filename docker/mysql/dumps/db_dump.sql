-- Adminer 4.7.7 MySQL dump

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--
CREATE DATABASE IF NOT EXISTS `laravel` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `laravel`;

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `activities`;
CREATE TABLE `activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `log` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `value_from` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `value_to` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `loggable_id` int(11) NOT NULL,
  `loggable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identifier_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `activities`;

DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blurb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `featured` tinyint(4) DEFAULT NULL,
  `published` enum('draft','published') COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_thumbnail` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `amp_content` text COLLATE utf8mb4_unicode_ci,
  `enable_amp` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `articles`;

DROP TABLE IF EXISTS `assets`;
CREATE TABLE `assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `medium_thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `small_thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'admin',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caption` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `assets`;
INSERT INTO `assets` (`id`, `path`, `medium_thumbnail`, `small_thumbnail`, `type`, `source`, `name`, `caption`, `alt`, `created_at`, `updated_at`) VALUES
(1,	'img/admin/logo.png',	'',	'',	'image',	'admin',	'Think Sumo Logo',	'text',	'',	'2020-08-25 21:46:17',	'2020-08-25 21:46:17');

DROP TABLE IF EXISTS `asset_groups`;
CREATE TABLE `asset_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference_id` int(11) NOT NULL,
  `reference_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `asset_groups`;

DROP TABLE IF EXISTS `asset_group_items`;
CREATE TABLE `asset_group_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `asset_group_items`;

DROP TABLE IF EXISTS `asset_samples_type`;
CREATE TABLE `asset_samples_type` (
  `asset_id` int(11) NOT NULL,
  `samples_type_id` int(11) NOT NULL,
  `order` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `asset_samples_type`;

DROP TABLE IF EXISTS `banners`;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caption` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published` enum('draft','published') COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) DEFAULT '100',
  `image` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_thumbnail` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `banners`;

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `options_json` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `newsletter` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `contacts`;

DROP TABLE IF EXISTS `emails`;
CREATE TABLE `emails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cc` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bcc` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_name` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `replyTo` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attach` text COLLATE utf8mb4_unicode_ci,
  `status` enum('pending','sent','failed') COLLATE utf8mb4_unicode_ci NOT NULL,
  `sent` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `emails`;

DROP TABLE IF EXISTS `long_names`;
CREATE TABLE `long_names` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `integer` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enum` enum('one','two') COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `long_names`;

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `migrations`;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1,	'2015_10_12_000000_create_users_table',	1),
(2,	'2015_10_12_100000_create_password_resets_table',	1),
(3,	'2016_03_10_080158_create_samples_table',	1),
(4,	'2016_03_17_061913_create_assets_table',	1),
(5,	'2016_03_28_070259_create_activities_table',	1),
(6,	'2016_05_18_143629_create_tests_table',	1),
(7,	'2016_05_20_145249_create_long_names_table',	1),
(8,	'2016_06_20_132823_create_options_table',	1),
(9,	'2016_06_20_151131_create_seos_table',	1),
(10,	'2017_01_16_170121_add_detail_fields_to_activity_table',	1),
(11,	'2017_01_30_211922_add_identifier_value_to_activities',	1),
(12,	'2017_03_02_164615_create_page_categories_table',	1),
(13,	'2017_03_02_171100_create_pages_table',	1),
(14,	'2017_03_03_121410_create_user_permissions_table',	1),
(15,	'2017_03_03_141048_create_banners_table',	1),
(16,	'2017_03_03_163858_create_user_roles_table',	1),
(17,	'2017_03_03_165458_create_user_role_permissions_table',	1),
(18,	'2017_03_06_162057_update_image_varchar_limits',	1),
(19,	'2017_03_06_162117_add_user_role_column_to_users_table',	1),
(20,	'2017_03_06_190950_create_articles_table',	1),
(21,	'2017_03_12_135328_create_emails_table',	1),
(22,	'2017_03_22_172506_update_options_table',	1),
(23,	'2017_05_21_144600_create_tags_table',	1),
(24,	'2017_08_17_123934_add_type_column_to_assets_table',	1),
(25,	'2017_08_18_114352_add_thumbnail_fields_to_assets_table',	1),
(26,	'2017_08_24_143840_create_samples_types_table',	1),
(27,	'2017_08_24_143924_create_samples_types_pivot_table_for_assets',	1),
(28,	'2017_09_01_102353_create_asset_groups_table',	1),
(29,	'2017_09_04_111644_create_asset_group_items_table',	1),
(30,	'2019_10_27_113252_add_source_field_to_assets_table',	1),
(31,	'2019_12_13_141427_create_video_banners_table',	1),
(32,	'2019_12_19_174253_create_video_options_table',	1),
(33,	'2020_01_17_155335_add_video_to_banners_table',	1),
(34,	'2020_01_30_164604_create_page_contents_table',	1),
(35,	'2020_01_30_164655_create_page_content_items_table',	1),
(36,	'2020_01_30_165155_create_page_controls_table',	1),
(37,	'2020_01_31_163741_add_allow_to_pages_table',	1),
(38,	'2020_02_03_160403_alter_page_controls_type_field_enum_options',	1),
(39,	'2020_02_05_143328_create_navbar_links_table',	1),
(40,	'2020_02_05_164812_add_order_on_navbar_links_table',	1),
(41,	'2020_02_06_172339_remove_slug_on_navbar_links_table',	1),
(42,	'2020_02_07_111756_add_order_on_pages_table',	1),
(43,	'2020_02_10_125745_add_fields_on_navbar_links_table',	1),
(44,	'2020_02_14_160949_create_contacts_table',	1),
(45,	'2020_02_20_155145_add_video_to_control_type',	1),
(46,	'2020_02_20_162543_add_field_on_articles_table',	1),
(47,	'2020_02_20_164856_add_fields_to_contacts',	1),
(48,	'2020_02_20_170855_update_fields_to_contacts',	1),
(49,	'2020_03_05_161342_create_previews_table',	1);

DROP TABLE IF EXISTS `navbar_links`;
CREATE TABLE `navbar_links` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_id` int(11) NOT NULL DEFAULT '0',
  `hide` tinyint(4) NOT NULL DEFAULT '0',
  `new_tab` tinyint(4) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disable_link` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `navbar_links`;

DROP TABLE IF EXISTS `options`;
CREATE TABLE `options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent` int(11) NOT NULL,
  `type` enum('text','asset','bool') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `asset` text COLLATE utf8mb4_unicode_ci,
  `help` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category` enum('general','email','admin','site') COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `options`;
INSERT INTO `options` (`id`, `name`, `slug`, `permanent`, `type`, `value`, `asset`, `help`, `created_at`, `updated_at`, `category`) VALUES
(1,	'Meta Title',	'meta-title',	1,	'text',	'Meta Title',	'',	NULL,	NULL,	NULL,	'general'),
(2,	'Meta Description',	'meta-description',	1,	'text',	'Meta Description',	'',	NULL,	NULL,	NULL,	'general'),
(3,	'Meta Image',	'meta-image',	0,	'asset',	'',	'',	NULL,	NULL,	NULL,	'general'),
(4,	'Contact Number',	'contact-number',	1,	'text',	'',	'',	NULL,	NULL,	NULL,	'general'),
(5,	'Contact Email',	'contact-email',	1,	'text',	'no-reply@sumofy.me',	'',	NULL,	NULL,	NULL,	'general'),
(6,	'Facebook Page',	'facebook-page',	0,	'text',	'',	'',	NULL,	NULL,	NULL,	'general'),
(7,	'Twitter Page',	'twitter-page',	0,	'text',	'',	'',	NULL,	NULL,	NULL,	'general'),
(8,	'Google Page',	'google-page',	0,	'text',	'',	'',	NULL,	NULL,	NULL,	'general'),
(9,	'Terms Link',	'terms-link',	0,	'text',	'',	'',	NULL,	NULL,	NULL,	'general'),
(10,	'Privacy Link',	'privacy-link',	0,	'text',	'',	'',	NULL,	NULL,	NULL,	'general'),
(11,	'Unsubscribe Link',	'unsubscribe-link',	0,	'text',	'',	'',	NULL,	NULL,	NULL,	'general'),
(12,	'Analytics Script',	'analytics-script',	0,	'text',	'',	'',	'The GA Code (ex: UA-xxxxxx)',	NULL,	NULL,	'general'),
(13,	'Footer Script',	'footer-script',	0,	'text',	'',	'',	'The GA Code (ex: UA-xxxxxx)',	NULL,	NULL,	'general'),
(14,	'Header Script',	'header-script',	0,	'text',	'',	'',	'The GA Code (ex: UA-xxxxxx)',	NULL,	NULL,	'general'),
(15,	'Instagram Feed Token',	'instagram-feed-token',	0,	'text',	'',	'',	'The IG token that will be used for IG feed extraction <br/><a href=\"https://public.3.basecamp.com/p/qmRMy9T4mjNyTrDQGXQ7UbhU\">Click here for more info</a>',	NULL,	NULL,	'general'),
(16,	'Email Driver',	'mail-driver',	1,	'text',	'smtp',	'',	NULL,	NULL,	NULL,	'email'),
(17,	'Email Host',	'mail-host',	1,	'text',	'email-smtp.us-west-2.amazonaws.com',	'',	NULL,	NULL,	NULL,	'email'),
(18,	'Email Port',	'mail-port',	1,	'text',	'587',	'',	NULL,	NULL,	NULL,	'email'),
(19,	'Email Username',	'mail-username',	1,	'text',	'AKIAJOT4XWHN6CZ4ERRA',	'',	NULL,	NULL,	NULL,	'email'),
(20,	'Email Password',	'mail-pass',	1,	'text',	'Ak6ckxl/Ypond/pmIqOkBJ0THsI3x9iLiF6XT13Ftiby',	'',	NULL,	NULL,	NULL,	'email'),
(21,	'Email Encryption',	'mail-encryption',	1,	'text',	'tls',	'',	NULL,	NULL,	NULL,	'email'),
(22,	'Email Image',	'email-image',	1,	'asset',	'',	'1',	NULL,	NULL,	NULL,	'email'),
(23,	'Sender Name',	'sender-name',	1,	'text',	'System Admin',	'',	NULL,	NULL,	NULL,	'email'),
(24,	'Sender Email',	'sender-email',	1,	'text',	'no-reply@sumofy.me',	'',	'The email that users will see when receiving emails from notifications',	NULL,	NULL,	'email'),
(25,	'Receiver Email',	'receiver-email',	1,	'text',	'contact@sumofy.me',	'',	'The email address that will receive inquiries in contact-us',	NULL,	NULL,	'email');

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published` enum('draft','published') COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_category_id` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `allow_add_contents` tinyint(4) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `pages`;
INSERT INTO `pages` (`id`, `name`, `slug`, `published`, `page_category_id`, `content`, `created_at`, `updated_at`, `allow_add_contents`, `order`) VALUES
(1,	'Home',	'home',	'draft',	0,	NULL,	'2018-10-12 14:01:57',	'2018-10-12 14:01:57',	0,	0),
(2,	'About',	'about',	'draft',	0,	NULL,	'2018-10-12 14:43:28',	'2018-10-12 14:43:28',	0,	0),
(3,	'Privacy Policy',	'privacy-policy',	'draft',	0,	NULL,	'2018-10-12 17:02:40',	'2018-10-12 17:02:40',	0,	0),
(4,	'Terms and Conditions',	'terms',	'draft',	0,	NULL,	'2018-10-12 17:04:00',	'2018-10-12 17:04:00',	0,	0),
(5,	'Contact',	'contact',	'draft',	0,	NULL,	'2018-10-12 17:04:00',	'2018-10-12 17:04:00',	0,	0);

DROP TABLE IF EXISTS `page_categories`;
CREATE TABLE `page_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `page_categories`;

DROP TABLE IF EXISTS `page_contents`;
CREATE TABLE `page_contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `reference_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `bg_image` int(11) NOT NULL,
  `published` enum('draft','published') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'draft',
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `editable` tinyint(4) NOT NULL DEFAULT '1',
  `allow_add_items` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `page_contents_page_id_index` (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `page_contents`;
INSERT INTO `page_contents` (`id`, `page_id`, `reference_id`, `reference_type`, `name`, `slug`, `title`, `description`, `bg_image`, `published`, `order`, `created_at`, `updated_at`, `deleted_at`, `enabled`, `editable`, `allow_add_items`) VALUES
(1,	5,	0,	'',	'Banners',	'banners',	'',	'',	0,	'draft',	1,	'2018-10-12 14:02:16',	'2018-10-12 14:02:19',	NULL,	1,	1,	0),
(2,	5,	0,	'',	'Form',	'form',	'',	'',	0,	'draft',	1,	'2018-10-12 14:02:19',	'2018-10-12 14:02:19',	NULL,	1,	1,	0);

DROP TABLE IF EXISTS `page_content_items`;
CREATE TABLE `page_content_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` int(11) NOT NULL,
  `bg_image` int(11) NOT NULL,
  `type` enum('default','image','input','file upload') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default',
  `published` enum('draft','published') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'draft',
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `editable` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `page_content_items_content_id_index` (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `page_content_items`;
INSERT INTO `page_content_items` (`id`, `content_id`, `title`, `description`, `image`, `bg_image`, `type`, `published`, `order`, `created_at`, `updated_at`, `deleted_at`, `name`, `slug`, `enabled`, `editable`) VALUES
(1,	2,	'',	'',	0,	0,	'default',	'draft',	0,	'2018-10-12 14:02:19',	'2018-10-12 14:02:19',	NULL,	'Form Elements',	'form-elements',	1,	1);

DROP TABLE IF EXISTS `page_controls`;
CREATE TABLE `page_controls` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reference_id` int(11) NOT NULL,
  `reference_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('text','number','checkbox','textarea','asset','select','color','date','time','date_time','video','tags') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `options_json` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(4) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_controls_reference_id_reference_type_index` (`reference_id`,`reference_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `page_controls`;
INSERT INTO `page_controls` (`id`, `reference_id`, `reference_type`, `name`, `label`, `type`, `options_json`, `required`, `order`, `value`, `created_at`, `updated_at`) VALUES
(1,	1,	'App\\PageContentItem',	'name',	'Name',	'text',	'',	0,	0,	'',	'2018-10-12 14:10:13',	'2018-10-12 15:10:25'),
(2,	1,	'App\\PageContentItem',	'email',	'Email',	'text',	'',	0,	0,	'',	'2018-10-12 14:10:13',	'2018-10-12 15:10:25'),
(3,	1,	'App\\PageContentItem',	'contact_number',	'Contact Number',	'text',	'',	0,	0,	'',	'2018-10-12 14:10:13',	'2018-10-12 15:10:25'),
(4,	1,	'App\\PageContentItem',	'address',	'Address',	'textarea',	'',	0,	0,	'',	'2018-10-12 14:10:13',	'2018-10-12 15:10:25'),
(5,	1,	'App\\PageContentItem',	'message',	'Message',	'textarea',	'',	0,	0,	'',	'2018-10-12 14:10:13',	'2018-10-12 15:10:25'),
(6,	1,	'App\\PageContentItem',	'subject',	'Subject',	'select',	'[{\"value\":\"hiring\",\"label\":\"Hiring\"},{\"value\":\"inquiry\",\"label\":\"Inquiry\"}]',	0,	0,	'',	'2018-10-12 14:10:13',	'2018-10-12 15:10:25'),
(8,	1,	'App\\PageContent',	'image',	'Image',	'asset',	'',	0,	0,	'',	'2018-10-12 14:10:13',	'2018-10-12 15:10:25'),
(9,	1,	'App\\PageContent',	'video',	'Video',	'video',	'',	0,	0,	'',	'2018-10-12 14:10:13',	'2018-10-12 15:10:25'),
(10,	1,	'App\\PageContent',	'link',	'link',	'text',	'',	0,	0,	'',	'2018-10-12 14:10:13',	'2018-10-12 15:10:25');

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `password_resets`;

DROP TABLE IF EXISTS `previews`;
CREATE TABLE `previews` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'create',
  `reference_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `previews_reference_type_reference_id_index` (`reference_type`,`reference_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `previews`;

DROP TABLE IF EXISTS `samples`;
CREATE TABLE `samples` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `range` enum('S','M','L') COLLATE utf8mb4_unicode_ci NOT NULL,
  `runes` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `embedded_rune` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `evaluation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `samples`;
INSERT INTO `samples` (`id`, `name`, `range`, `runes`, `embedded_rune`, `evaluation`, `image`, `image_thumbnail`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	'Riou',	'M',	'Bright Shield Rune, Darkness Rune, Blue Gate Rune',	'Kindness Rune',	'He\'s the hero of the game, so you\'ll know him better than anyone else.  He\'s good offensively and defensively.  He\'s fast and has the Bright Shield Rune, which is great for healing your party members.',	'',	NULL,	NULL,	NULL,	NULL),
(2,	'Tir McDohl',	'M',	'Rage Rune, Soul Eater, Flowing Rune',	'Poison/Down Rune',	'He\'s as good as he was before, if not better.  Tir is back, but only if you load a completed Suikoden I file.  There\'s a nice FAQ about it here at GameFAQs.  Anyways, the Soul Eater might be a rival for the best rune in the game.  And in the hands of Tir, you\'ll have a hugely destructive party member.  Otherwise, all of his stats are extremely high and his Double Leader Unite attack with Riou is also incredibly powerful.',	'',	NULL,	NULL,	NULL,	NULL),
(3,	'Kahn',	'M',	'Darkness Rune, Resurrection Rune, Flowing Rune',	'Magic Drain Rune',	'Kahn is one of the best fighters in the game.  He\'s got all the bases covered, three runes, and lot of HP and MP.  Make sure you give him your best runes, such as a Flowing Rune.',	'',	NULL,	NULL,	NULL,	NULL),
(4,	'Sierra',	'S',	'Mother Earth Rune, Darkness Rune',	'Thunder Rune',	'Her weapon isn\'t as sharp as it could be.  But still, she\'s probably going to do some spell casting, anyways.  Lots of MP and two rune slots are nice.  High stats give her a higher standing, too.  She\'s definitely one of the best in the game.',	'',	NULL,	NULL,	NULL,	NULL),
(5,	'Nina',	'M',	'Lightning Rune, Fire Rune',	'Water Rune',	'Wow, Nina was the character I\'d least expect to be in this class of characters.  But she\'s very well-rounded and has high stats overall. She\'s got a lot of MP, Strength, and Defense, too.  I guess that Nina is an Easter Egg that the designers threw in.',	'',	NULL,	NULL,	NULL,	NULL),
(6,	'Sigfried',	'M',	'Lightning Rune, White Saint Rune, ????????',	'None',	'His gender isn\'t quite known.  Richmond refers to him as a \'he,\'but when you take a bath with him, he\'s in the girls\' side.  I\'ll just say that he\'s male, but he likes his women.  His attack power is through the roof and he has a unique rune alongside that.  But, due to his large animal status, you might replace him for two other strong magicians instead. Stat-wise, he has no major problems, but he\'s a little weak on the physical defense side.  Also, if he gets a critical hit, he not only stabs the enemy, but he pummels it with his front hooves.  It should result in around 1300+ damage.  On a side note, he gets a third rune slot sometime between level 60 and level 99.',	'',	NULL,	NULL,	NULL,	NULL),
(7,	'Zamza',	'S',	'Fire Dragon Rune, Fire Sealing Rune',	'Fire Lizard Rune',	'As much as I hate to admit it, he\'s actually a good fighter. He\'s much like Siegfried, only with less attack and more defense.  He\'s a good magician, but you might want to equip him with a Fire Sealing Rune to compliment his Fire Dragon Rune.',	'',	NULL,	NULL,	NULL,	NULL),
(8,	'Flik',	'S',	'Thunder Rune, Rage Rune',	'Exertion Rune',	'Flik will serve you well throughout the game.  His high MP and high stats all-around make him a steady party member.  Replace his Lightning Rune and give him a Thunder and Rage Rune if you find some.  He\'ll be an offensive machine.',	'',	NULL,	NULL,	NULL,	NULL),
(9,	'Sheena',	'S',	'Thunder Rune, Earth Rune, Fire Rune',	'Down Rune',	'He has high stats in every area, and if you transferred him in from Suikoden I, he\'ll have a sharp weapon and high level.  He gets 3 runes, but his magical power is lower than other magic users.  So, his magic attack strength isn\'t as strong as some of the other people\'s magic strength.',	'',	NULL,	NULL,	NULL,	NULL),
(10,	'Camus',	'S',	'Rage Rune, Resurrection Rune',	'None',	'He gets more spells and is the better magically than Miklotov. He\'s weaker physically, though, and can only get two runes.  His Rage Rune is a good compliment, though, and could very well combine with a Thunder Rune.',	'',	NULL,	NULL,	NULL,	NULL),
(11,	'Anita',	'S',	'Falcon Rune, Water Rune',	'Down Rune',	'She has high stats in all areas.  None are astronomical, but high nonetheless.  I gave her a Water Rune so she does some damage with her Falcon Rune and heals when the party is low on HP.',	'',	NULL,	NULL,	NULL,	NULL),
(12,	'Tengaar',	'L',	'Lightning Rune, Mother Earth Rune',	'Wind Rune',	'Although her HP is kinda low, her MP is quite high.  Give her a Mother Earth Rune and she\'ll be a good back-up support magician.  Give her a Lightning Rune and she\'ll have good offensive strength as well.',	'',	NULL,	NULL,	NULL,	NULL),
(13,	'Eilie',	'L',	'Fire Rune, Darkness Rune',	'Lightning Rune',	'Eilie gets a bit of MP if you level her up enough.  It\'s not as much as some of the more magical people, but she still is a useful character. She\'s got high stats in all areas, and she\'s good offensively and defensively.',	'',	NULL,	NULL,	NULL,	NULL),
(14,	'Luc',	'S',	'Blue Gate Rune, Wind Rune, Darkness Rune',	'Wind Rune',	'Luc is a good character early on in the game.  He\'s got a weak defense and offense, so he does best in the back row.  Give him a Blue Gate Rune when you first get him (before Two River) and give it to Riou once he reaches level 40.',	'',	NULL,	NULL,	NULL,	NULL),
(15,	'Mazus',	'S',	'Blue Gate Rune, Thunder Rune, Wizard Rune',	'Lightning Rune',	'Mazus is probably the second strongest Full-Fledged Magician on the offensive side.  He has high HP and Strength.  He is very weak offensively, though.  Keep him in the back row and have him cast spells until the battle is over.',	'',	NULL,	NULL,	NULL,	NULL),
(16,	'Viki',	'S',	'Lightning Rune, Blinking Rune',	'Earth Rune',	'Even though she\'s not the brightest character in the world, she does have some strong magic skills.  She even has a unique Blinking Rune, which is quite rare.  You can get another Blinking Rune in Rokkaku, though, so it\'s not totally unique.',	'',	NULL,	NULL,	NULL,	NULL),
(17,	'Viktor',	'S',	'Double-Beat Rune, Fury Rune',	'Friendship Rune',	'Ahh, Viktor.  Get personality and his fighting ability matches it.  Early in the game, get a Double-Beat Rune from the Cut Rabbits in the North Sparrow Pass.  Once you have access to the Highland Garrison north of Muse, make sure you buy a Fury Rune from the Muse Runemaster.',	'',	NULL,	NULL,	NULL,	NULL),
(18,	'Pesmerga',	'S',	'Killer, Double-Beat, Fury Rune',	'Rage Rune',	'First off, replace that wimpy Counter Rune with a more powerful rune.  A Killer Rune would be nice, and you can get one from either Tai-Ho or the Woodpeckers in the passage to Tsai\'s house.  Now, you can do some serious damage.',	'',	NULL,	NULL,	NULL,	NULL),
(19,	'Georg',	'S',	'Killer Rune',	'Down Rune',	'Georg was a favorite of mine when I first played the game.  He did quite well in Rockaxe and later.  I did some Richmond investigations and found out that he\'s got quite a history behind him.',	'',	NULL,	NULL,	NULL,	NULL),
(20,	'L. C. Chan',	'S',	'None',	'None',	'An interesting thing about Wakaba and Long Chan-Chan is that their MP acquisition pattern is different from all of the other characters. They get as much MP as if they had a Magic stat of 100, but their stats are at 50+.',	'',	NULL,	NULL,	NULL,	NULL),
(21,	'Nanami',	'M',	'Water Rune, Balance/Spark Rune',	'Earth Rune',	'Throughout the game, you\'ll have to have Nanami as a party member.  But, her MP is not as high as it should be, and when she really starts to get some MP, it becomes obsolete due to story purposes.  She\'s got some good stats in the other areas, but more or less she\'s a much weaker version of Riou.',	'',	NULL,	NULL,	NULL,	NULL),
(22,	'Sasuke',	'L',	'Killer Rune',	'Sleep Rune',	'He\'s got a ton of Speed and Technique, so he\'s fast, dodges well, and always seems to get a hit.  His Killer rune also allows him to get critical hits all the time.  But, he\'s limited to just one rune slot.  But still, he\'s a really good fighter.',	'',	NULL,	NULL,	NULL,	NULL),
(23,	'Shiro',	'S',	'Counter Rune',	'None',	'ell, all of his stats are quite high.  But, due to his animal status, he can\'t equip weapons nor armor.  He has a lower HP than others, but his MP is high enough that he can cast a level 4 spell.',	'',	NULL,	NULL,	NULL,	NULL),
(24,	'Clive',	'L',	'Kite Rune',	'Fire Rune',	'Clive isn\'t very much different from his Suikoden I counterpart. His level will be greatly reduced from your S1 file, starting around level 20.',	'',	NULL,	NULL,	NULL,	NULL),
(25,	'Stallion',	'L',	'True Holy Rune, Champion\'s Rune',	'Sleep Rune',	' give Stallion the Champion\'s Rune that you get in L\'Renouille. That way, you can put him in your party and go wherever you need to go.  The trip to Sajah will go much faster with this combination.  Average fighter overall.  Excellent Speed.',	'',	NULL,	NULL,	NULL,	NULL),
(26,	'Simone',	'L',	'Wind Rune',	'Earth Rune',	'Although he has a lot of MP, his low HP, Defense, Attack and one-rune limit bring him down to the Average Fighter class.  He\'s pretty bland if you ask me.',	'',	NULL,	NULL,	NULL,	NULL),
(27,	'Tuta',	'L',	'Chimera Rune, Medicine Rune',	'Earth Rune',	'Make sure to embed an earth rune on his weapon.  It\'ll give some extra defense which he desperately needs.  A super-low HP count as well as almost no power makes him a person you will never choose unless you want a challenge.',	'',	NULL,	NULL,	NULL,	NULL),
(28,	'Hai Yo',	'S',	'Hazy Rune, Barrier Rune, ?????????',	'Down Rune',	'Most of the good stats border around 100, which is not very good. Two rune are nice, but Hai Yo is best left in the kitchen.  Also, he gets a third rune slot between level 60 and level 99.',	'',	NULL,	NULL,	NULL,	NULL),
(29,	'Mukumuku',	'L',	'Fire Rune',	'None',	'Pretty average in most areas, but he has a low HP and magical prowess.  He\'s like all of the other squirrels, is not a good party member. His only usefulness is to recruit him in Kyaro Town to make those battles much easier due to your lack of characters.',	'',	NULL,	NULL,	NULL,	NULL),
(30,	'Chuchara',	'S',	'Barrier Rune',	'None',	'These stats may not be accurate, as I imported Chuchara via a Codebreaker.  If they ARE accurate, Chuchara is probably the weakest character in the game.  But then, after boosting his level to 99, take a look at his stats.',	'',	NULL,	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `samples_types`;
CREATE TABLE `samples_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sample_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `samples_types`;

DROP TABLE IF EXISTS `seos`;
CREATE TABLE `seos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seoable_id` int(11) NOT NULL,
  `seoable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `seos`;

DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `taggable_id` int(11) NOT NULL,
  `taggable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `tags`;

DROP TABLE IF EXISTS `tests`;
CREATE TABLE `tests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `tinyint` tinyint(4) DEFAULT '0',
  `order` int(11) DEFAULT '100',
  `integer` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_thumbnail` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enum` enum('one','two') COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `tests`;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cms` tinyint(1) NOT NULL,
  `verified` tinyint(1) NOT NULL,
  `status` enum('active','blocked') COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('super','admin','normal') COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `users`;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `cms`, `verified`, `status`, `type`, `last_login`, `last_ip`, `remember_token`, `token`, `email_verified_at`, `created_at`, `updated_at`, `user_role_id`) VALUES
(1,	'ThinkSumo Developer',	'dev@sumofy.me',	'$2y$10$EmwYsIIl6Ktmi3gquXvfv.vhRkMfemuR/5O9qzc5ivEHLrXECSRXG',	1,	1,	'active',	'super',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `user_permissions`;
CREATE TABLE `user_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL,
  `master` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `user_permissions`;
INSERT INTO `user_permissions` (`id`, `name`, `slug`, `description`, `parent`, `master`, `order`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1,	'Dashboard',	'dashboard-1',	'Dashboard access',	0,	1,	1,	NULL,	NULL,	NULL),
(2,	'Articles',	'articles-2',	'Articles Admin Access',	0,	2,	2,	NULL,	NULL,	NULL),
(3,	'Create',	'create-3',	'Create Articles Access',	2,	2,	1,	NULL,	NULL,	NULL),
(4,	'Update',	'update-4',	'Update Articles Access',	2,	2,	2,	NULL,	NULL,	NULL),
(5,	'Delete',	'delete-5',	'Delete Articles Access',	2,	2,	3,	NULL,	NULL,	NULL),
(6,	'Publish',	'publish-6',	'Publish Articles Access',	2,	2,	4,	NULL,	NULL,	NULL),
(7,	'Banners',	'banners-7',	'Banners Access',	0,	7,	3,	NULL,	NULL,	NULL),
(8,	'Page Categories',	'page-categories-8',	'Page Categories Access',	0,	8,	4,	NULL,	NULL,	NULL),
(9,	'Create',	'create-9',	'Create Page Category Access',	9,	8,	1,	NULL,	NULL,	NULL),
(10,	'Create',	'create-10',	'Create Page Category Access',	8,	8,	1,	NULL,	NULL,	NULL),
(11,	'Update',	'update-11',	'Update Page Category Access',	8,	8,	2,	NULL,	NULL,	NULL),
(12,	'Delete',	'delete-12',	'Delete Page Category Access',	8,	8,	3,	NULL,	NULL,	NULL),
(13,	'Pages',	'pages-13',	'Page Access',	0,	13,	5,	NULL,	NULL,	NULL),
(14,	'Create',	'create-14',	'Create Page Access',	13,	13,	1,	NULL,	NULL,	NULL),
(15,	'Update',	'update-15',	'Update Page Access',	13,	13,	2,	NULL,	NULL,	NULL),
(16,	'Delete',	'delete-16',	'Delete Page Access',	13,	13,	3,	NULL,	NULL,	NULL),
(17,	'Publish',	'publish-17',	'Publish Page Access',	13,	13,	4,	NULL,	NULL,	NULL),
(18,	'Users',	'users-18',	'User Access',	0,	18,	6,	NULL,	NULL,	NULL),
(19,	'User Roles',	'create-19',	'User Roles Access',	19,	19,	7,	NULL,	NULL,	NULL),
(20,	'Create',	'create-20',	'Create User Access',	18,	18,	1,	NULL,	NULL,	NULL),
(21,	'Update',	'update-21',	'Update User Access',	18,	18,	2,	NULL,	NULL,	NULL),
(22,	'Delete',	'delete-22',	'Delete User Access',	18,	18,	3,	NULL,	NULL,	NULL),
(23,	'User Roles',	'user-roles-23',	'User Roles Access',	0,	23,	7,	NULL,	NULL,	NULL),
(24,	'Create',	'create-24',	'Create User Roles Access',	23,	23,	1,	NULL,	NULL,	NULL),
(25,	'Update',	'update-25',	'Update User Roles Access',	23,	23,	2,	NULL,	NULL,	NULL),
(26,	'Delete',	'delete-26',	'Delete Create User Roles Access',	23,	23,	3,	NULL,	NULL,	NULL),
(27,	'Functions',	'functions-27',	'Functions Access',	0,	27,	8,	NULL,	NULL,	NULL),
(28,	'Create',	'create-28',	'Create Functions Access',	27,	27,	1,	NULL,	NULL,	NULL),
(29,	'Update',	'update-29',	'Update Functions Access',	27,	27,	2,	NULL,	NULL,	NULL),
(30,	'Delete',	'delete-30',	'Delete Functions Access',	27,	27,	3,	NULL,	NULL,	NULL),
(31,	'Email Logs',	'email-logs-31',	'Email Logs Access',	0,	31,	9,	NULL,	NULL,	NULL),
(32,	'View',	'view-32',	'View Email Logs',	31,	31,	1,	NULL,	NULL,	NULL),
(33,	'Options',	'options-33',	'Options Access',	0,	33,	10,	NULL,	NULL,	NULL),
(34,	'Create',	'create-34',	'Create Options Access',	34,	33,	1,	NULL,	NULL,	NULL),
(35,	'Create',	'create-35',	'Create Options Access',	33,	33,	1,	NULL,	NULL,	NULL),
(36,	'Update',	'update-36',	'Update Options Access',	36,	33,	2,	NULL,	NULL,	NULL),
(37,	'Update',	'update-37',	'Update Options Access',	33,	33,	2,	NULL,	NULL,	NULL),
(38,	'Delete',	'delete-38',	'Delete Options Access',	33,	33,	3,	NULL,	NULL,	NULL),
(39,	'Create',	'create-39',	'Create Banners Access',	7,	7,	1,	NULL,	NULL,	NULL),
(40,	'Edit',	'edit-40',	'Edit Banners Access',	7,	7,	2,	NULL,	NULL,	NULL),
(41,	'Delete',	'delete-41',	'Delete Banner Access',	7,	7,	3,	NULL,	NULL,	NULL),
(42,	'View',	'view-42',	'View Dashboard',	1,	1,	1,	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `user_roles`;
INSERT INTO `user_roles` (`id`, `name`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1,	'Super Admin',	'Access to all',	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `user_role_permissions`;
CREATE TABLE `user_role_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_role_id` int(11) NOT NULL,
  `user_permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `user_role_permissions`;
INSERT INTO `user_role_permissions` (`id`, `user_role_id`, `user_permission_id`, `created_at`, `updated_at`) VALUES
(1,	1,	1,	NULL,	NULL),
(2,	1,	2,	NULL,	NULL),
(3,	1,	3,	NULL,	NULL),
(4,	1,	4,	NULL,	NULL),
(5,	1,	5,	NULL,	NULL),
(6,	1,	6,	NULL,	NULL),
(7,	1,	7,	NULL,	NULL),
(8,	1,	39,	NULL,	NULL),
(9,	1,	40,	NULL,	NULL),
(10,	1,	41,	NULL,	NULL),
(11,	1,	8,	NULL,	NULL),
(12,	1,	10,	NULL,	NULL),
(13,	1,	11,	NULL,	NULL),
(14,	1,	12,	NULL,	NULL),
(15,	1,	13,	NULL,	NULL),
(16,	1,	14,	NULL,	NULL),
(17,	1,	15,	NULL,	NULL),
(18,	1,	16,	NULL,	NULL),
(19,	1,	17,	NULL,	NULL),
(20,	1,	18,	NULL,	NULL),
(21,	1,	20,	NULL,	NULL),
(22,	1,	21,	NULL,	NULL),
(23,	1,	22,	NULL,	NULL),
(24,	1,	23,	NULL,	NULL),
(25,	1,	24,	NULL,	NULL),
(26,	1,	25,	NULL,	NULL),
(27,	1,	26,	NULL,	NULL),
(28,	1,	27,	NULL,	NULL),
(29,	1,	28,	NULL,	NULL),
(30,	1,	29,	NULL,	NULL),
(31,	1,	30,	NULL,	NULL),
(32,	1,	31,	NULL,	NULL),
(33,	1,	32,	NULL,	NULL),
(34,	1,	33,	NULL,	NULL),
(35,	1,	35,	NULL,	NULL),
(36,	1,	37,	NULL,	NULL),
(37,	1,	38,	NULL,	NULL),
(38,	1,	42,	NULL,	NULL);

DROP TABLE IF EXISTS `video_banners`;
CREATE TABLE `video_banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frametag` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `video_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `video_banners`;

DROP TABLE IF EXISTS `video_options`;
CREATE TABLE `video_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `autoplay` tinyint(4) DEFAULT NULL,
  `loop` tinyint(4) DEFAULT NULL,
  `controls` tinyint(4) DEFAULT NULL,
  `mute` tinyint(4) DEFAULT NULL,
  `fullscreen` tinyint(4) DEFAULT NULL,
  `frameborder` tinyint(4) DEFAULT NULL,
  `related_videos` tinyint(4) DEFAULT NULL,
  `gyroscope` tinyint(4) DEFAULT NULL,
  `accelerometer` tinyint(4) DEFAULT NULL,
  `picture` tinyint(4) DEFAULT NULL,
  `encrypt_media` tinyint(4) DEFAULT NULL,
  `start` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `portrait` tinyint(4) DEFAULT NULL,
  `title` tinyint(4) DEFAULT NULL,
  `byline` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `video_options`;

-- 2020-08-25 13:49:14