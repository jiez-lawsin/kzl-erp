<nav class="navbar navbar-expand-md fixed-top">
    <div class="container">
        <a class="navbar-brand" href="{{ route('homePage') }}">
            <img src="{{ asset('img/app/Qbit-Logo.png') }}" alt="">
        </a>
        <button class="navbar-toggler" type="button" onclick="openNav()">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About Us</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown-services" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">Our Services</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown-services">
                        <a class="nav-link" href="#">Cyber Security</a>
                        <a class="nav-link" href="#">IT Support</a>
                        <a class="nav-link" href="#">IT Solutions</a>
                        <a class="nav-link" href="#">Industry Specialists</a>
                        <a class="nav-link" href="#">Free Audit</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Blog</a>
                </li>
            </ul>
            <ul class="navbar-nav navbar-nav-buttons">
                <li>
                    <a href="#" class="btn btn-primary btn-primary-50 uppercase">Client Tools</a>
                </li>
                <li>
                    <a href="#" class="btn btn-primary btn-primary uppercase">Contact Us &nbsp;(08) 6364 0600</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- changeable to left/right -->
<aside id="sidenav" class="bg-dark left">
    <button class="btn btn-transparent" onclick="closeNav()"><i class="far fa-times"></i></button>
    <ul>
        @foreach (General::get_menu() as $menu)
            @if (!$menu->hide)
                @if (count($menu->children) > 0)
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#collapse-{{ $menu->id }}">
                            {{ $menu->name }}
                            <i class="far fa-chevron-down"></i>
                        </a>
                        <ul class="collapse-list collapse no-icon" id="collapse-{{ $menu->id }}">
                            @foreach ($menu->children as $child)
                                <li class="nav-item">
                                    <a class="nav-link" target="{{ $menu->new_tab ? '_blank' : '' }}"
                                        href="{{ $child->type == 'external' ? $child->link : $child->page->slug }}">{{ $child->name }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @else
                    <li class="nav-item active">
                        <a class="nav-link" target="{{ $menu->new_tab ? '_blank' : '' }}"
                            href="{{ $menu->type == 'external' ? $menu->link : $menu->page->slug }}">{{ $menu->name }}</a>
                    </li>
                @endif
            @endif
        @endforeach
    </ul>
</aside>
