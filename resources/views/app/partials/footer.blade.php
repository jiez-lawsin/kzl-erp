<footer style="background-image: url('{{ asset('img/app/Mask Group 10.png') }}')">
    <div class="container footer-container">
        <div class="footer-header">
            <div class="logo-container">
                <img src="{{ asset('img/app/Qbit-Logo.png') }}" alt="">
            </div>
            <div class="actions-container">
                <button class="btn btn-primary">Contact Us</button>
            </div>
        </div>
        <div class="footer-links">
            <ul>
                <li><div class="link-title">IT Support</div></li>
                <li><a href="#">Managed Services</a></li>
                <li><a href="#">Service Bank</a></li>
                <li><a href="#">Microsoft 365 Management</a></li>
            </ul>
            <ul>
                <li><div class="link-title">IT Solutions</div></li>
                <li><a href="#">Server Upgrade</a></li>
                <li><a href="#">Business Continuity Plan</a></li>
                <li><a href="#">Security Tab</a></li>
                <li><a href="#">Hardware & Software</a></li>
                <li><a href="#">Business Phone Solutions</a></li>
                <li><a href="#">Microsoft 365 Plans</a></li>
                <li><a href="#">Business Grade NBN</a></li>
                <li><a href="#">Sharepoint</a></li>
                <li><a href="#">Qbit Cloud</a></li>
                <li><a href="#">Web Hosting Services</a></li>
            </ul>
            <ul>
                <li><div class="link-title">Industry Specialists</div></li>
                <li><a href="#">IT Support for Accountants</a></li>
                <li><a href="#">IT Support for Aged Care Specialists</a></li>
                <li><a href="#">IT Support for Mining</a></li>
                <li><a href="#">IT Support for Legal Practices</a></li>
                <li><a href="#">IT Support for Healthcare Industries</a></li>
                <li><a href="#">IT Support for Real Estate</a></li>
                <li><a href="#">IT Support for Engineering</a></li>
            </ul>
            <ul>
                <li><div class="link-title">Cyber Security</div></li>
                <li><a href="#">Notifiable Data Breaches</a></li>
                <li><a href="#">Responding to Data Breaches</a></li>
                <li><a href="#">Darkweb Monitoring</a></li>
                <li><a href="#">Phishing Training</a></li>
                <li><a href="#">Spam Protection</a></li>
            </ul>
        </div>
        <div class="footer-links-inline">
            <div class="footer-links-inline--left">
                <div class="inline-links">
                    <a href="#">About Us</a>
                    <a href="#">Blog/News</a>
                    <a href="#">IT Specials</a>
                    <a href="#">Privacy</a>
                    <a href="#">Terms and Conditions</a>
                </div>
                <div class="footer-copyright">
                    Copyright © 2021 Qbit | Design by Think Sumo
                </div>
            </div>
            <div class="footer-links-inline--right">
                <div class="footer-socials">
                    <a href="#" target="_blank"><span><i class="fab fa-facebook-f"></i></span></a>
                    <a href="#" target="_blank"><span><i class="fab fa-linkedin-in"></i></span></a>
                        <a href="#" target="_blank"><span><i class="fab fa-youtube"></i></span></a>
                </div>
            </div>
        </div>
    </div>
</footer>