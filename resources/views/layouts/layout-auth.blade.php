<!DOCTYPE html>
<html>
    <head>
        <!-- CHANGE TITLE WITH FROM FACADE -->
        <title>KZL ERP</title> 
        <meta name="viewport" content="width=device-width">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {!! Html::style('css/caboodle.css') !!}
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>

    <body class="layout-auth" style="background-image: url('img/admin/auth-bg.jpg'); background-size: cover; background-repeat: no-repeat;">
        @if ($error = $errors->first('message'))
        <div class="caboodle-alert warning" role="alert">
        {{$error}}
        </div>
        @endif
        @if (session('status'))
        <div class="caboodle-alert success" role="alert">
        {{ session('status') }}
        </div>
        @endif
        @if (session('errorToken'))
        <div class="caboodle-alert warning" role="alert">
        {{ session('errorToken') }}
        </div>
        @endif
        <nav class="navbar-auth navbar navbar-expand-lg" style="background: #333;">
            <a class="navbar-brand"><img style="width: auto; height: 20px;" src="{{ asset('img/admin/KZL-UNLMTD_LOGO_FINAL-03.png') }}" alt=""></a>
          </nav>
        <main class="auth-main no-margin" style="height: 100vh; display: flex;">
            <div class="content-container">
                @yield('content')
            </div>
        </main>
        @yield('added-scripts')
    </body>
</html>
