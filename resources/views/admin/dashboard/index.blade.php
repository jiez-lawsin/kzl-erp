@extends('layouts.admin')

@section('header')
    <header class="flex-center">
        <h1>{{ $title }}</h1>
        <div class="float-right">
            {!! Form::open(['route' => 'adminDashboard', 'method' => 'get', 'class' => 'no-margin']) !!}
            <div class="no-margin caboodle-form-group caboodle-flex caboodle-flex-row caboodle-flex-left">
                <label class="single-search no-padding no-margin" for="date_range" style="width: 210px;">
                    {!! Form::text('date_range', @$request['date_range'], ['autocomplete' => 'off', 'class' => 'input-date-range form-control input-sm no-margin', 'placeholder' => 'Date Range']) !!}
                </label>
                &nbsp; <button type="submit"
                    class="caboodle-btn caboodle-btn-small caboodle-btn-primary mdc-button mdc-button--unelevated "
                    data-mdc-auto-init="MDCRipple">Filter</button>
            </div>
            {!! Form::close() !!}
        </div>
    </header>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-3">
            <div class="caboodle-card dashboard-card">
                <h4>Total Orders</h4>
                <h1 class="color-secondary">{{ $orderCount }}</h1>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="caboodle-card dashboard-card">
                <h4>Valid Orders</h4>
                <h1 class="color-green">{{ $validOrdersCount }}</h1>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="caboodle-card dashboard-card">
                <h4>Quantity Sales</h4>
                <h1 class="color-purple">{{ $validTotalOrderQuantity }}</h1>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="caboodle-card dashboard-card">
                <h4>Effectivity</h4>
                <h1 class="color-primary">
                    @if ($orderCount > 0)
                        {{ number_format(($validOrdersCount / $orderCount) * 100) }}%
                    @else
                        0%
                    @endif
                </h1>
            </div>
        </div>

        <?php
        $amountReleased = 0;
        $amountReleasedForSetCapital = 0;
        $completedTotalOrderCost = 0;
        $hasUnencodedOrderCapital = false;
        foreach ($lazTransactions as $trx) {
            $amountReleased += $trx->amount;
            if ($trx->fee_name == 'Item Price Credit' || $trx->fee_name == 'Lost Claim') {
                if (@$trx->order && @$trx->order->items && count($trx->order->items) > 0) {
                    $isTrxOrderCapitalSet = true;
                    foreach ($trx->order_items as $item) {
                        $completedTotalOrderCost += @$item->capital_price;
                        if ($item->capital_price == 0 || !$item->capital_price) {
                            $hasUnencodedOrderCapital = true;
                            $isTrxOrderCapitalSet = false;
                        }
                    }
                    if ($isTrxOrderCapitalSet) {
                        $amountReleasedForSetCapital += $trx->amount;
                    }
                }
            }
        }
        foreach ($shopeeTransactions as $trx) {
            $amountReleased += $trx->total_released_amount;
            if (@$trx->order && @$trx->order->items && count($trx->order->items) > 0) {
                $isTrxOrderCapitalSet = true;
                foreach ($trx->order_items as $item) {
                    $completedTotalOrderCost += @$item->capital_price;
                    if ($item->capital_price == 0 || !$item->capital_price) {
                        $hasUnencodedOrderCapital = true;
                        $isTrxOrderCapitalSet = false;
                    }
                }
                if ($isTrxOrderCapitalSet) {
                    $amountReleasedForSetCapital += $trx->total_released_amount;
                }
            }
        }
        $amountReleased += $manualOrderAmountReceived;
        ?>
        <div class="col-sm-3">
            <div class="caboodle-card dashboard-card" title="From orders with valid transactions">
                <h5>Total Sales</h5>
                <h2 class="color-purple">₱ {{ number_format($amountReleased, 2) }}</h2>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="caboodle-card dashboard-card" title="From orders with valid transactions">
                <h5 class="flex-center">
                    Gross Sales Profit
                    @if ($hasUnencodedOrderCapital)
                        &nbsp;<i class="far fa-exclamation-circle color-red"
                            title="Some order items has 0 capital price"></i>
                    @endif
                </h5>
                <h2 class="color-green">₱
                    {{ number_format($amountReleasedForSetCapital - $completedTotalOrderCost, 2) }}</h2>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="caboodle-card dashboard-card" title="From valid orders">
                <h5>Order Price Income</h5>
                <h2 class="color-secondary">₱ {{ number_format($validTotalOrderPriceIncome, 2) }}</h2>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="caboodle-card dashboard-card" title="From valid orders">
                <h5>Total Orders Cost</h5>
                <h2 class="color-red">₱ {{ number_format($validTotalOrderCost, 2) }}</h2>
            </div>
        </div>
    </div>
    <div class="caboodle-card">
        <div class="caboodle-card-header no-bottom-padding">
            <h4>Store Sales</h4>
            <br>
        </div>
        <div class="caboodle-card-body no-top-padding">
            <table class="caboodle-table">
                <thead>
                    <th class="caboodle-table-col-header">Store Name</th>
                    <th class="caboodle-table-col-header text-center">Valid Orders</th>
                    <th class="caboodle-table-col-header text-center">Total Orders</th>
                    <th class="caboodle-table-col-header text-center">Effectivity (%)</th>
                    <th class="caboodle-table-col-header text-center">Qty Sales</th>
                    <th class="caboodle-table-col-header text-center">Order Price Income</th>
                    <th class="caboodle-table-col-header text-center">Total Orders Cost</th>
                </thead>
                <tbody>
                    @foreach ($stores as $store)
                        <tr>
                            <td class="uppercase sub-text-2 vertical-align-top">
                                <span>
                                    @if (strtolower($store->channel) == 'lazada')
                                        <img class="channel-logo" src="{{ asset('img/admin/laz.png') }}">
                                    @elseif (strtolower($store->channel) == 'shopee')
                                        <img class="channel-logo"
                                            src="{{ asset('img/admin/shopee.png') }}">
                                    @elseif (strtolower($store->channel) == 'fb marketplace')
                                        <img class="channel-logo" src="{{ asset('img/admin/fb.png') }}">
                                    @else
                                        <img class="channel-logo" src="{{ asset('img/admin/person.png') }}">
                                    @endif
                                </span>
                                {{ $store->store_name }}
                            </td>
                            <td class="text-center sub-text-1">{{ $store->total_orders }}</td>
                            <td class="text-center sub-text-1">{{ $store->valid_orders }}</td>
                            <td class="text-center sub-text-1">
                                @if ($store->total_orders > 0)
                                    {{ number_format(($store->valid_orders / $store->total_orders) * 100) }}%
                                @else
                                    0%
                                @endif
                            </td>
                            <td class="text-center sub-text-1">{{ $store->quantity_sales }}</td>
                            <td class="text-center sub-text-1">₱ {{ number_format($store->order_price_income, 2) }}</td>
                            <td class="text-center sub-text-1">₱ {{ number_format($store->total_orders_cost, 2) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="caboodle-card">
        <div class="caboodle-card-header no-bottom-padding">
            <h4>Top Product Sales</h4>
            <br>
            <div class="filters no-padding">
                {!! Form::open(['route' => 'adminLazadaTransactions', 'method' => 'get', 'class' => 'no-margin']) !!}
                <div
                    class="caboodle-form-group caboodle-flex caboodle-flex-row caboodle-flex-left caboodle-form-control-connected">
                    <label class="no-padding" for="product_name">
                        {!! Form::text('product_name', @$request['product_name'], ['class' => 'form-control input-sm no-margin', 'placeholder' => 'Product Name']) !!}
                    </label>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="caboodle-card-body no-top-padding">
            <table class="caboodle-table">
                <thead>
                    <th class="caboodle-table-col-header">Product Name</th>
                    <th class="caboodle-table-col-header text-center">Total Orders</th>
                    <th class="caboodle-table-col-header text-center">Valid Orders</th>
                    <th class="caboodle-table-col-header text-center">Qty Sales</th>
                    <th class="caboodle-table-col-header">Product Price Income</th>
                </thead>
                <tbody>
                    @foreach ($products_sales as $p)
                        <tr>
                            <td class="sub-text-2 color-secondary">
                                {{ $p->master->name }}
                                @if ($p->master->variant_name != '' && $p->master->variant_name != '-')
                                    <br><span class="color-grey">{{ $p->master->variant_name }}<span>
                                @endif
                            </td>
                            <td class="sub-text-1 text-center">{{ $p->total }}</td>
                            <td class="sub-text-1 text-center">{{ $p->valid_orders }}</td>
                            <td class="sub-text-1 text-center">{{ $p->total_order_qty }}</td>
                            <td class="sub-text-1">₱ {{ number_format($p->total_order_price_income, 2) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
@section('added-scripts')
    <script>
        $(function() {
            $('input.input-date-range').daterangepicker({
                opens: 'left',
                autoUpdateInput: false,
            }, function(start, end, label) {
                $('input.input-date-range').val(start.format('MM/DD/YYYY') + ' - ' + end.format(
                    'MM/DD/YYYY'));
            });
        });
    </script>
@endsection
