@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Lazada Transactions</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
      <a
          class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
          data-mdc-auto-init="MDCRipple"
          href="#"
          data-toggle="modal" data-target="#importTransactions">
          Import
      </a>
  </div>
</header>
<div class="modal fade" id="importTransactions" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="importTransactionsLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="importTransactionsLabel">Import Lazada Transactions</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <form action="{{route('adminLazadaTransactionsImport')}}" method="post" enctype="multipart/form-data">
            @csrf
          <div class="modal-body">
            <p>Export your transactions from your Seller Center</p>
            <div class="form-group">
              <input type="file" name="file" class="" id="chooseFile">
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-dismiss="modal">Cancel</button>
              <button type="submit" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated " data-mdc-auto-init="MDCRipple">Import</button>
          </div>
          </form>
      </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="caboodle-card">
      <div class="caboodle-card-header">
        <div class="filters no-padding">
          {!! Form::open(['route'=>'adminLazadaTransactions', 'method' => 'get', 'class'=>'no-margin']) !!}
            <div class="caboodle-form-group caboodle-flex caboodle-flex-row caboodle-flex-left caboodle-form-control-connected">
                <label class="no-padding" for="order_number">
                    {!! Form::text('order_number', @$request['order_number'], ['class'=>'form-control input-sm no-margin', 'placeholder'=>'Order Number']) !!}
                </label>
                &nbsp;
                <label class="no-padding" for="master_sku">
                    {!! Form::text('master_sku', @$request['master_sku'], ['class'=>'form-control input-sm no-margin', 'placeholder'=>'MSKU']) !!}
                </label>
                <label class="no-padding" for="product_name">
                    {!! Form::text('product_name', @$request['product_name'], ['class'=>'form-control input-sm no-margin', 'placeholder'=>'Product Name']) !!}
                </label>
                &nbsp;
                <label class="single-search no-padding" for="date_range">
                    {!! Form::text('date_range', @$request['date_range'], ['autocomplete'=>'off', 'class'=>'input-date-range form-control input-sm no-margin', 'placeholder'=>'Date Range']) !!}
                </label>
                &nbsp;
                <label class="no-padding"  for="store">
                    <select class="form-control input-sm no-margin" name="store">
                      <option value="" disabled selected>Select Store</option>
                      <option value="">[All]</option>
                      @foreach ($stores as $store)
                          <option @if(@$request['store'] == $store->store_name) selected @endif value="{{ $store->store_name }}">
                            {{ $store->store_name }}
                          </option>
                      @endforeach
                    </select>
                </label>
                &nbsp;
                <label class="no-padding" for="status">
                    <select class="form-control input-sm no-margin" name="status">
                      <option value="" disabled selected>Status</option>
                      <option value="">[All]</option>
                      <option value="paid" @if(strtolower(@$request['status']) == 'paid') selected @endif>Paid</option>
                      <option value="not paid" @if(strtolower(@$request['status']) == 'not paid') selected @endif>Not Paid</option>
                    </select>
                </label>
                &nbsp;
                <button type="submit" class="caboodle-btn caboodle-btn-small caboodle-btn-primary mdc-button mdc-button--unelevated " data-mdc-auto-init="MDCRipple">Filter</button>
                <!-- {!! Form::hidden('sort', @$sort) !!} {!! Form::hidden('sortBy', @$sortBy) !!} -->
            </div>
          {!! Form::close() !!}
        </div>
      </div>
      @if(count($transactions) > 0)
      <div class="caboodle-card-body">
        <?php
            $capital = 0;
            $profit = 0;
            $amountReleased = 0;
            $amountReleasedForSetCapital = 0;

            $totalFees = 0;
            $totalSales = 0;
            $hasOrderUnsetCapital = false;
            foreach ($transactions as $trx) {
              if ($trx->amount > 0) {
                $totalSales += $trx->amount;
              }
              if ($trx->amount <= 0) {
                $totalFees += $trx->amount;
              }
              $amountReleased += $trx->amount; 
              
              if (@$trx->order && @$trx->order->items && count($trx->order->items) > 0) {
                $isTrxOrderCapitalSet = true;
                foreach ($trx->order->items as $item) {
                  if ($trx->fee_name == 'Item Price Credit') $capital += $item->capital_price;
                  if ($item->capital_price == 0) {
                    $hasOrderUnsetCapital = true;
                    $isTrxOrderCapitalSet = false;
                  }
                }
                if ($isTrxOrderCapitalSet) $amountReleasedForSetCapital += $trx->amount;
              }
            }
        ?>
        {{-- <div class="row">
          <div class="col-sm-4">
            <div class="caboodle-card user-card ">
                <h4>Total Amount Released</h4>
                <h1 class="color-purple">₱ {{ number_format($amountReleased, 2) }}</h1>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="caboodle-card user-card ">
                <h4>Total Sales</h4>
                <h1 class="color-green">₱ {{ number_format($totalSales, 2) }}</h1>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="caboodle-card user-card ">
                <h4>Total Lazada Fees</h4>
                <h1 class="color-red">₱ {{ number_format($totalFees, 2) }}</h1>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="caboodle-card user-card ">
              <h4 class="flex-center">
                Total Orders Cost
                @if($hasOrderUnsetCapital)
                    &nbsp;<i class="far fa-exclamation-circle color-red" title="Some order items has 0 capital price"></i>
                @endif
              </h4>
              <h1 class="color-purple">₱ {{ number_format($capital, 2) }}</h1>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="caboodle-card user-card ">
                <h4 class="flex-center">
                  Gross Profit
                  @if($hasOrderUnsetCapital)
                      &nbsp;<i class="far fa-exclamation-circle color-red" title="Some order items has 0 capital price"></i>
                  @endif
                </h4>
                <h1 class="color-secondary">₱ {{ number_format($amountReleasedForSetCapital - $capital, 2) }}</h1>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="caboodle-card user-card ">
                <h4>Total Orders</h4>
                <h1 class="color-green">{{ @count($byOrders) }}</h1>
            </div>
          </div>
        </div> --}}
      </div>
      @endif
      <div class="caboodle-card-body">
        @if(count($data) > 0)
          {!! Form::open(['route'=>'adminLazadaTransactionsDestroy', 'method' => 'delete', 'class'=>'form form-parsley form-delete']) !!}
            <table class="caboodle-table">
              <thead>
                <tr>
                  <th width="50px">
                    <div class="mdc-form-field" data-toggle="tooltip" title="Select All">
                        <div class="mdc-checkbox caboodle-table-select-all">
                            <input type="checkbox" class="mdc-checkbox__native-control" name="select_all" />
                            <div class="mdc-checkbox__background">
                                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                </svg>
                                <div class="mdc-checkbox__mixedmark"></div>
                            </div>
                        </div>
                    </div>
                  </th>
                  <th class="caboodle-table-col-action">
                    <a class="caboodle-btn caboodle-btn-icon caboodle-btn-danger mdc-button mdc-ripple-upgraded mdc-button--unelevated x-small uppercase" 
                            data-mdc-auto-init="MDCRipple"
                            href="{{ route('adminLazadaTransactionsDestroy') }}"
                            method="DELETE"
                            data-toggle-alert="warning"
                            data-alert-form-to-submit=".form-delete"
                            permission-action="delete"
                            data-notif-message="Deleting...">
                        <i class="fas fa-trash"></i>
                    </a>
                  </th>
                  <th class="caboodle-table-col-header hide" >Product</th>
                  <th style="width: 100px;" class="caboodle-table-col-header hide" >QTY</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $d)
                  <tr>
                    <td>
                      <div class="mdc-form-field">
                        <div class="mdc-checkbox">
                            <input type="checkbox" class="mdc-checkbox__native-control" name="ids[]" value="{{ $d->id }}" />
                            <div class="mdc-checkbox__background">
                                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                </svg>
                                <div class="mdc-checkbox__mixedmark"></div>
                            </div>
                        </div>
                      </div>
                    </td>
                    <td class="sub-text-2 color-secondary">
                      @if (@$d->master_product_name && @$d->master_product_name != '')
                      {{@$d->master_product_name}} 
                        @if (@$d->variant_name && @$d->variant_name != '-')
                          <br> <span class="color-grey">{{ $d->variant_name }}<span> 
                        @endif
                        <div class="color-grey">MSKU: {{ $d->master_sku }}</div> 
                      @else
                      {{@$d->item_name}} 
                      @endif
                    </td>
                    <td class="sub-text-2">{{$d->total_qty}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          {!! Form::close() !!}
        @else
          <div class="empty text-center">
              No results found
          </div>
        @endif
        @if ($pagination)
          <div class="caboodle-pagination">
              {{$data->links('layouts.pagination')}}
          </div>
        @endif
      </div>
    </div>
  </div>
</div>
@stop

@section('added-scripts')
<script>
  $(function() {
    $('input.input-date-range').daterangepicker({
      opens: 'left',
      autoUpdateInput: false,
    }, function(start, end, label) { 
      $('input.input-date-range').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
      console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
    });
  });
</script>
@endsection