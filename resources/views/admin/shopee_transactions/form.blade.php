<div class="caboodle-form-group">
  <label for="order_number">Order Number</label>
  {!! Form::text('order_number', null, ['class'=>'form-control', 'id'=>'order_number', 'placeholder'=>'Order Number', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="refund_id">Refund Id</label>
  {!! Form::text('refund_id', null, ['class'=>'form-control', 'id'=>'refund_id', 'placeholder'=>'Refund Id', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="username">Username</label>
  {!! Form::text('username', null, ['class'=>'form-control', 'id'=>'username', 'placeholder'=>'Username', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="order_creation_date">Order Creation Date</label>
  {!! Form::text('order_creation_date', null, ['class'=>'form-control', 'id'=>'order_creation_date', 'placeholder'=>'Order Creation Date', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="payout_completed_date">Payout Completed Date</label>
  {!! Form::text('payout_completed_date', null, ['class'=>'form-control', 'id'=>'payout_completed_date', 'placeholder'=>'Payout Completed Date', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="original_price">Original Price</label>
  {!! Form::text('original_price', null, ['class'=>'form-control', 'id'=>'original_price', 'placeholder'=>'Original Price', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="seller_product_promotion">Seller Product Promotion</label>
  {!! Form::text('seller_product_promotion', null, ['class'=>'form-control', 'id'=>'seller_product_promotion', 'placeholder'=>'Seller Product Promotion', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="buyer_refund_amount">Buyer Refund Amount</label>
  {!! Form::text('buyer_refund_amount', null, ['class'=>'form-control', 'id'=>'buyer_refund_amount', 'placeholder'=>'Buyer Refund Amount', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="product_discount_rebate">Product Discount Rebate</label>
  {!! Form::text('product_discount_rebate', null, ['class'=>'form-control', 'id'=>'product_discount_rebate', 'placeholder'=>'Product Discount Rebate', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="seller_voucher_discount">Seller Voucher Discount</label>
  {!! Form::text('seller_voucher_discount', null, ['class'=>'form-control', 'id'=>'seller_voucher_discount', 'placeholder'=>'Seller Voucher Discount', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="seller_absorbed_coin_cashback">Seller Absorbed Coin Cashback</label>
  {!! Form::text('seller_absorbed_coin_cashback', null, ['class'=>'form-control', 'id'=>'seller_absorbed_coin_cashback', 'placeholder'=>'Seller Absorbed Coin Cashback', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="buyer_paid_shipping_fee">Buyer Paid Shipping Fee</label>
  {!! Form::text('buyer_paid_shipping_fee', null, ['class'=>'form-control', 'id'=>'buyer_paid_shipping_fee', 'placeholder'=>'Buyer Paid Shipping Fee', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="shipping_fee_rebate">Shipping Fee Rebate</label>
  {!! Form::text('shipping_fee_rebate', null, ['class'=>'form-control', 'id'=>'shipping_fee_rebate', 'placeholder'=>'Shipping Fee Rebate', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="third_party_logistics_shipping">Third Party Logistics Shipping</label>
  {!! Form::text('third_party_logistics_shipping', null, ['class'=>'form-control', 'id'=>'third_party_logistics_shipping', 'placeholder'=>'Third Party Logistics Shipping', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="reverse_shipping_fee">Reverse Shipping Fee</label>
  {!! Form::text('reverse_shipping_fee', null, ['class'=>'form-control', 'id'=>'reverse_shipping_fee', 'placeholder'=>'Reverse Shipping Fee', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="commission_fee">Commission Fee</label>
  {!! Form::text('commission_fee', null, ['class'=>'form-control', 'id'=>'commission_fee', 'placeholder'=>'Commission Fee', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="service_fee">Service Fee</label>
  {!! Form::text('service_fee', null, ['class'=>'form-control', 'id'=>'service_fee', 'placeholder'=>'Service Fee', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="transaction_fee">Transaction Fee</label>
  {!! Form::text('transaction_fee', null, ['class'=>'form-control', 'id'=>'transaction_fee', 'placeholder'=>'Transaction Fee', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="total_released_amount">Total Released Amount</label>
  {!! Form::text('total_released_amount', null, ['class'=>'form-control', 'id'=>'total_released_amount', 'placeholder'=>'Total Released Amount', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="lost_compensation">Lost Compensation</label>
  {!! Form::text('lost_compensation', null, ['class'=>'form-control', 'id'=>'lost_compensation', 'placeholder'=>'Lost Compensation', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="seller_shipping_fee_promotion">Seller Shipping Fee Promotion</label>
  {!! Form::text('seller_shipping_fee_promotion', null, ['class'=>'form-control', 'id'=>'seller_shipping_fee_promotion', 'placeholder'=>'Seller Shipping Fee Promotion', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="seller_voucher_code">Seller Voucher Code</label>
  {!! Form::textarea('seller_voucher_code', null, ['class'=>'form-control redactor', 'id'=>'seller_voucher_code', 'placeholder'=>'Seller Voucher Code', 'required', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>
<div class="caboodle-form-group">
  <label for="shipping_provider">Shipping Provider</label>
  {!! Form::text('shipping_provider', null, ['class'=>'form-control', 'id'=>'shipping_provider', 'placeholder'=>'Shipping Provider', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="courier_name">Courier Name</label>
  {!! Form::text('courier_name', null, ['class'=>'form-control', 'id'=>'courier_name', 'placeholder'=>'Courier Name', 'required']) !!}
</div>
