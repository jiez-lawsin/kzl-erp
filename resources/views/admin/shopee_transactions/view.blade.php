@extends('layouts.admin')

@section('breadcrumbs')
<ol class="breadcrumb">
  <li><a href="{{route('adminDashboard')}}">Dashboard</a></li>
  <li><a href="{{route('adminShopeeTransactions')}}">Shopee Transactions</a></li>
  <li class="active">View</li>
</ol>
@stop

@section('content')
<div class="col-md-8">
	<table class='table table-striped table-bordered table-view'>
		<tr>
			<th>Id</th>
			<td>{!!$data->id!!}</td>
		</tr>
		<tr>
			<th>Order number</th>
			<td>{!!$data->order_number!!}</td>
		</tr>
		<tr>
			<th>Refund id</th>
			<td>{!!$data->refund_id!!}</td>
		</tr>
		<tr>
			<th>Username</th>
			<td>{!!$data->username!!}</td>
		</tr>
		<tr>
			<th>Order creation date</th>
			<td>{!!$data->order_creation_date!!}</td>
		</tr>
		<tr>
			<th>Payout completed date</th>
			<td>{!!$data->payout_completed_date!!}</td>
		</tr>
		<tr>
			<th>Original price</th>
			<td>{!!$data->original_price!!}</td>
		</tr>
		<tr>
			<th>Seller product promotion</th>
			<td>{!!$data->seller_product_promotion!!}</td>
		</tr>
		<tr>
			<th>Buyer refund amount</th>
			<td>{!!$data->buyer_refund_amount!!}</td>
		</tr>
		<tr>
			<th>Product discount rebate</th>
			<td>{!!$data->product_discount_rebate!!}</td>
		</tr>
		<tr>
			<th>Seller voucher discount</th>
			<td>{!!$data->seller_voucher_discount!!}</td>
		</tr>
		<tr>
			<th>Seller absorbed coin cashback</th>
			<td>{!!$data->seller_absorbed_coin_cashback!!}</td>
		</tr>
		<tr>
			<th>Buyer paid shipping fee</th>
			<td>{!!$data->buyer_paid_shipping_fee!!}</td>
		</tr>
		<tr>
			<th>Shipping fee rebate</th>
			<td>{!!$data->shipping_fee_rebate!!}</td>
		</tr>
		<tr>
			<th>Third party logistics shipping</th>
			<td>{!!$data->third_party_logistics_shipping!!}</td>
		</tr>
		<tr>
			<th>Reverse shipping fee</th>
			<td>{!!$data->reverse_shipping_fee!!}</td>
		</tr>
		<tr>
			<th>Commission fee</th>
			<td>{!!$data->commission_fee!!}</td>
		</tr>
		<tr>
			<th>Service fee</th>
			<td>{!!$data->service_fee!!}</td>
		</tr>
		<tr>
			<th>Transaction fee</th>
			<td>{!!$data->transaction_fee!!}</td>
		</tr>
		<tr>
			<th>Total released amount</th>
			<td>{!!$data->total_released_amount!!}</td>
		</tr>
		<tr>
			<th>Lost compensation</th>
			<td>{!!$data->lost_compensation!!}</td>
		</tr>
		<tr>
			<th>Seller shipping fee promotion</th>
			<td>{!!$data->seller_shipping_fee_promotion!!}</td>
		</tr>
		<tr>
			<th>Seller voucher code</th>
			<td>{!!$data->seller_voucher_code!!}</td>
		</tr>
		<tr>
			<th>Shipping provider</th>
			<td>{!!$data->shipping_provider!!}</td>
		</tr>
		<tr>
			<th>Courier name</th>
			<td>{!!$data->courier_name!!}</td>
		</tr>
		<tr>
			<th>Created at</th>
			<td>
				@if ($data->created_at)
				<?php $created_at = new Carbon($data->created_at); ?>
				{{$created_at->toFormattedDateString() . ' ' . $created_at->toTimeString()}}
				@endif
			</td>
		</tr>
		<tr>
			<th>Updated at</th>
			<td>
				@if ($data->updated_at)
				<?php $created_at = new Carbon($data->updated_at); ?>
				{{$created_at->toFormattedDateString() . ' ' . $created_at->toTimeString()}}
				@endif
			</td>
		</tr>
	</table>
</div>
@stop