<div class="row">
  <div class="caboodle-form-group col-sm-9">
    <label for="name">Name</label>
    {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Name', 'required']) !!}
  </div>
  
  <div class="caboodle-form-group col-sm-3">
   <label for="featured"> &nbsp;</label>
   <?php $boolCheck = (@$data->featured) ? 'disabled="disabled"': ''; ?>
    {!! Form::checkbox('featured', '0', null , ['hidden',$boolCheck]) !!}
    {!! Form::checkbox('featured', '1', null , ['data-toggle'=>'toggle','data-size'=>'medium','class'=>'form-control ', 'id'=>'featured','data-onstyle'=>'warning','data-on'=>"<i class='fa fa-star'></i> Featured",'data-off'=>"<i class='fa fa-star-o'></i> Unfeatured "]) !!}
  </div>
</div>
@if (isset($data))
<div class="caboodle-form-group">
  <label for="slug">Slug</label>
  {!! Form::text('slug', null, ['class'=>'form-control', 'id'=>'slug', 'placeholder'=>'Slug', 'readonly']) !!}
</div>
@endif
<div class="caboodle-form-group">
  <label for="blurb">Blurb</label>
  {!! Form::text('blurb', null, ['class'=>'form-control', 'id'=>'blurb', 'placeholder'=>'Blurb']) !!}
</div>
<div class="caboodle-form-group">
  <label for="date">Date</label>
  {!! Form::text('date', null, ['class'=>'form-control sumodate', 'id'=>'date', 'placeholder'=>'Date', 'required']) !!}
</div>

<div class="caboodle-form-group">
  <label for="published">Published</label>
  {!! Form::select('published', ['draft' => 'draft', 'published' => 'published'], null, ['class'=>'form-control select2']) !!}
</div>
<div class="caboodle-form-group">
  <label for="content">Content</label>
  {!! Form::textarea('content', null, ['class'=>'form-control redactor', 'id'=>'content', 'placeholder'=>'Content', 'required', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>
<div class="mdc-form-field">
    <div class="mdc-checkbox">
        <input type="checkbox" class="mdc-checkbox__native-control" id="enable_amp" name="enable_amp" value="{{@$data->enable_amp}}" {{@$data->enable_amp ? 'checked':''}} />
        <div class="mdc-checkbox__background">
            <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
            </svg>
            <div class="mdc-checkbox__mixedmark"></div>
        </div>
    </div>
    <label for="enable_amp">Enable AMP <small>(Accelerated Mobile Pages)</small> <a target="_blank" href="https://amp.dev/about/how-amp-works/"><i class="far fa-question-circle"></i></a></label>
</div>
<div class="caboodle-form-group sumo-asset-select" data-crop-url="{{route('adminArticlesCropUrl')}}">
  <label for="image">Image</label>
  {!! Form::hidden('image', null, ['class'=>'sumo-asset', 'data-id'=>@$data->id, 'data-thumbnail'=>'image_thumbnail']) !!}
  <h6 class="sub-text-1" >Image size should be 100x100 pixels minimum</h6>
</div>
<div class="caboodle-form-group">
  <label for="author">Author</label>
  {!! Form::text('author', null, ['class'=>'form-control', 'id'=>'author', 'placeholder'=>'Author']) !!}
</div>
<div class="caboodle-form-group">
  <label for="tags">Tags</label>
  {!! Form::select('tags[]', @$tag_list, null, ['class'=>'select2-allow-creation', 'id'=>'tags','multiple']) !!}
</div>
{!! Form::hidden('amp_content', null, ['id'=>'amp_content']) !!}
