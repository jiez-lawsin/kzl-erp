{{-- @extends('layouts.admin-auth')

@section('content')
<div class="row justify-content-center">
  <div class="col-md-6 col-md-offset-3">
    <div class="panel">
      <div class="panel-heading text-center">Admin Login</div>
      <div class="panel-body">
        {!! Form::open(['route'=>'adminAuthenticate', 'class'=>'form']) !!}
        <div class="form-group">
          <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="{{ old('email') }}">
        </div>
        <div class="form-group">
          <input type="password" name="password" class="form-control" id="password" placeholder="Password">
        </div>
        <div class="form-group text-center">
          <button type="submit" class="btn btn-primary btn-block">Login</button>
        </div>
        <div class="form-group text-center">
          <a href="{{ url('/password/reset') }}">Forgot password?</a>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@stop --}}

@extends('layouts.layout-auth')

@section('content')
<div class="row login-auth" style="margin-top: -80px">
    <div class="col-md-6 center-block">
        <div class="caboodle-card">
            {!! Form::open(['route'=>'adminAuthenticate', 'class'=>'form']) !!} 
            <input type="hidden" name="ip">
            <input type="hidden" name="location">
            <input type="hidden" name="browser">
            <div class="caboodle-card-header text-center">
                <h2>Sign in</h2>
            </div>
            <div class="caboodle-card-body">
                <div class="caboodle-form-group">
                    <input type="email" name="email" id="email" placeholder="Email" value="{{ old('email') }}" required>
                </div>
                <div class="caboodle-form-group no-margin">
                    <input type="password" name="password" id="password" placeholder="Password" required>
                </div>
                <input type="hidden" name="store" id="store" placeholder="Store name" value="Artiphelego" required>
            </div>
            <div class="caboodle-card-footer">
                <button type="submit" data-toggle="login" class="caboodle-btn caboodle-btn-small caboodle-btn-primary mdc-button mdc-button--unelevated btn-full-width" data-mdc-auto-init="MDCRipple">Login</button>
                <div class="forgot-password text-center">
                    <a href="{{ url('/password/reset') }}" class="caboodle-link">Forgot password</a>
                </div>
            </div>
             {!! Form::close() !!} 
        </div>
        <div class="caboodle-brand" style="color: #FFF;">
            Powered by <a href="#"><img src="{{ asset('img/admin/white-Logo-with-Name-only.png') }}" alt="helloshredder"></a>
        </div>
    </div>
</div>
@stop