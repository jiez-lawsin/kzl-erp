<div class="caboodle-form-group">
  <label for="order_number">Order Number</label>
  {!! Form::text('order_number', null, ['class'=>'form-control', 'id'=>'order_number', 'placeholder'=>'Order Number', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="transaction_number">Transaction Number</label>
  {!! Form::text('transaction_number', null, ['class'=>'form-control', 'id'=>'transaction_number', 'placeholder'=>'Transaction Number', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="transaction_date">Transaction Date</label>
  {!! Form::text('transaction_date', null, ['class'=>'form-control', 'id'=>'transaction_date', 'placeholder'=>'Transaction Date', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="transaction_type">Transaction Type</label>
  {!! Form::text('transaction_type', null, ['class'=>'form-control', 'id'=>'transaction_type', 'placeholder'=>'Transaction Type', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="fee_name">Fee Name</label>
  {!! Form::text('fee_name', null, ['class'=>'form-control', 'id'=>'fee_name', 'placeholder'=>'Fee Name', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="amount">Amount</label>
  {!! Form::text('amount', null, ['class'=>'form-control', 'id'=>'amount', 'placeholder'=>'Amount', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="paid_status">Paid Status</label>
  {!! Form::text('paid_status', null, ['class'=>'form-control', 'id'=>'paid_status', 'placeholder'=>'Paid Status', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="comment">Comment</label>
  {!! Form::textarea('comment', null, ['class'=>'form-control redactor', 'id'=>'comment', 'placeholder'=>'Comment', 'required', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>
