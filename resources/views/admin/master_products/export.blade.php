<table>
    <thead>
    <tr>
        <th>Master Product Name</th>
        <th>Variant Name</th>
        <th>Master SKU</th>
        <th>Purchase Price</th>
    </tr>
    </thead>
    <tbody>
    @foreach($master_products as $p)
        <tr>
            <td>{{ $p->name }}</td>
            <td>{{ $p->variant_name }}</td>
            <td>{{ $p->master_sku }}</td>
            <td>{{ $p->purchase_price }}</td>
        </tr>
    @endforeach
    </tbody>
</table>