@extends('layouts.admin')

@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item far"><a href="{{ route('adminDashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item far active"><span>Master Products</span></li>
        </ol>
    </nav>
@stop

@section('header')
    <header class="flex-center">
        <h1>{{ $title }}</h1>
        <div class="header-actions">
            <a class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
                data-mdc-auto-init="MDCRipple" href="{{ route('adminMasterProductsExport') }}">
                Export
            </a>
            &nbsp;
            <a class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
                data-mdc-auto-init="MDCRipple" data-toggle="modal" data-target="#importMasterProductModal" href="#">
                Import
            </a>
        </div>
    </header>
    <div class="modal fade" id="importMasterProductModal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="importMasterProductModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="importMasterProductModalLabel">Import Master Products From Ginee</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('adminMasterProductsImport') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="file" name="file" class="" id="chooseFile">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button"
                            data-dismiss="modal">Cancel</button>
                        <button type="submit"
                            class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated "
                            data-mdc-auto-init="MDCRipple">Import</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="caboodle-card">
                <div class="caboodle-card-header">
                    <div class="filters no-padding">
                        {!! Form::open(['route' => 'adminMasterProducts', 'method' => 'get', 'class' => 'no-margin']) !!}
                            <div class="caboodle-form-group caboodle-flex caboodle-flex-row caboodle-flex-left caboodle-form-control-connected">
                                <?php
                                    $param = @$master_sku ? $master_sku : '';
                                    if ($param != '') {
                                        $param .= '&';
                                    }
                                    $param = @$unset_capital ? '' : 'unset_capital=true';
                                ?>
                                <a href="{{ route('adminMasterProducts') }}?{{ $param }}"
                                    class="mdc-icon-toggle animated-icon" data-toggle="tooltip"
                                    title="View orders that have unset capital price" role="button" aria-pressed="false"
                                    permission-action="edit">
                                    <i class="far fa-exclamation-circle color-red" aria-hidden="true"></i>
                                </a>
                                <label class="no-padding no-margin" for="msku_prefix">
                                    <select class="form-control input-sm no-margin" name="msku_prefix">
                                        <option selected value="">Prefix</option>
                                        <option @if(strtolower(@$request['msku_prefix']) == 'all') selected @endif value="all">All</option>
                                    </select>
                                </label>
                                <label class="no-margin single-search no-padding">
                                    {!! Form::text('master_sku', @$request['master_sku'], ['class'=>'form-control input-sm no-margin', 'placeholder'=>'MSKU']) !!}
                                </label>
                                &nbsp;
                                <button type="submit" class="caboodle-btn caboodle-btn-small caboodle-btn-primary mdc-button mdc-button--unelevated " data-mdc-auto-init="MDCRipple">Filter</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="caboodle-card-body">
                    @if (count($data) > 0)
                        {!! Form::open(['route' => 'adminMasterProductsDestroy', 'method' => 'delete', 'class' => 'form form-parsley form-delete']) !!}
                        <table class="caboodle-table">
                            <thead>
                                <tr>
                                    <th width="50px">
                                        <div class="mdc-form-field" data-toggle="tooltip" title="Select All">
                                            <div class="mdc-checkbox caboodle-table-select-all">
                                                <input type="checkbox" class="mdc-checkbox__native-control"
                                                    name="select_all" />
                                                <div class="mdc-checkbox__background">
                                                    <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                                        <path class="mdc-checkbox__checkmark-path" fill="none"
                                                            stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                                    </svg>
                                                    <div class="mdc-checkbox__mixedmark"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="caboodle-table-col-action">
                                        <a class="caboodle-btn caboodle-btn-icon caboodle-btn-danger mdc-button mdc-ripple-upgraded mdc-button--unelevated x-small uppercase"
                                            data-mdc-auto-init="MDCRipple"
                                            href="{{ route('adminMasterProductsDestroy') }}" method="DELETE"
                                            data-toggle-alert="warning" data-alert-form-to-submit=".form-delete"
                                            permission-action="delete" data-notif-message="Deleting...">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </th>
                                    <th class="caboodle-table-col-header hide">Name</th>
                                    <th class="caboodle-table-col-header hide">Master SKU</th>
                                    <th class="caboodle-table-col-header hide">Purchase Price</th>
                                    <th class="caboodle-table-col-header hide"></th>
                                    <th colspan="100%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $d)
                                    <tr id="{{ $d->master_sku }}">
                                        <td>
                                            <div class="mdc-form-field">
                                                <div class="mdc-checkbox">
                                                    <input type="checkbox" class="mdc-checkbox__native-control" name="ids[]"
                                                        value="{{ $d->id }}" />
                                                    <div class="mdc-checkbox__background">
                                                        <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                                            <path class="mdc-checkbox__checkmark-path" fill="none"
                                                                stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                                        </svg>
                                                        <div class="mdc-checkbox__mixedmark"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="master_product__name">{{ $d->name }}</span>
                                            @if ($d->variant_name != '' && $d->variant_name != '-')
                                                <div class="master_product__variant_name sub-text-2">
                                                    {{ $d->variant_name }}</div>
                                            @endif
                                        </td>
                                        <td class="master_product__master_sku uppercase color-secondary">
                                            {{ $d->master_sku }}</td>
                                        <td class="uppercase color-secondary">₱ <span
                                                class="master_product__purchase_price">{{ number_format($d->purchase_price, 2) }}</span>
                                        </td>
                                        <td>
                                            @if (count($d->order_items) > 0)
                                                <?php
                                                $unsetOrderCapital = $d
                                                    ->order_items()
                                                    ->with('order')
                                                    ->where('capital_price', 0)
                                                    ->get();
                                                ?>
                                                @if (count($unsetOrderCapital) > 0)
                                                    <a href="#" data-toggle="modal" data-target="#setOrderCapitalModal"
                                                        data-json="{{ json_encode($unsetOrderCapital) }}"
                                                        data-msku="{{ $d->master_sku }}"
                                                        class="mdc-icon-toggle animated-icon" data-toggle="tooltip"
                                                        title="Some orders have unset capital price" role="button"
                                                        aria-pressed="false" permission-action="edit">
                                                        <i class="far fa-exclamation-circle color-red"
                                                            aria-hidden="true"></i>
                                                    </a>
                                                @endif
                                            @endif
                                        </td>
                                        <td width="110px" class="text-center">
                                            @if (Auth::user()->type == 'super')
                                                <a href="#" data-toggle="modal" data-target="#editMasterProductModal"
                                                    data-json="{{ json_encode($d) }}"
                                                    class="mdc-icon-toggle animated-icon" data-toggle="tooltip"
                                                    title="Manage" role="button" aria-pressed="false"
                                                    permission-action="edit">
                                                    <i class="far fa-edit" aria-hidden="true"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! Form::close() !!}
                    @else
                        <div class="empty text-center">
                            No results found
                        </div>
                    @endif
                    @if ($pagination)
                        <div class="caboodle-pagination">
                            {{ $data->links('layouts.pagination') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editMasterProductModal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="editMasterProductLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div>
                        <h5 class="modal-title" id="editMasterProductLabel">Edit Master Product</h5>
                        <div class="sub-text-2" id="master_sku_modal"></div>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::model($data, ['route' => 'adminMasterProductsUpdateFromModal', 'files' => true, 'method' => 'patch', 'class' => 'form form-parsley form-edit']) !!}
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="master_sku">
                    <div class="caboodle-form-group">
                        <label for="name">Name</label>
                        {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Name', 'required']) !!}
                    </div>
                    <div class="caboodle-form-group">
                        <label for="variant_name">Variant Name</label>
                        {!! Form::text('variant_name', null, ['class' => 'form-control', 'id' => 'variant_name', 'placeholder' => 'Variant Name', 'required']) !!}
                    </div>
                    <div class="caboodle-form-group">
                        <label for="purchase_price">Purchase Price</label>
                        {!! Form::number('purchase_price', null, ['step' => 'any', 'class' => 'form-control', 'id' => 'purchase_price', 'placeholder' => 'Purchase Price', 'required']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button"
                        data-dismiss="modal">Cancel</button>
                    <button type="button"
                        class="form-parsley-submit caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
                        data-submit-specific=".form-edit" data-mdc-auto-init="MDCRipple">Save</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="setOrderCapitalModal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="setOrderCapitalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div>
                        <h5 class="modal-title" id="setOrderCapitalLabel">Set Order Capital</h5>
                        <div class="sub-text-2" id="master_sku_modal"></div>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::model($data, ['route' => 'adminOrderItemUpdatePurchasePriceMultiple', 'files' => true, 'method' => 'patch', 'class' => 'form form-parsley form-set-order']) !!}
                @csrf
                <div class="modal-body">
                    <form action="#" id="unsetOrderItemsForm">
                        <table class="caboodle-table">
                            <thead>
                                <th>Order Number</th>
                                <th>Store</th>
                                <th width="120px">Purchase Price</th>
                            </thead>
                            <tbody id="unsetOrderItemsTbody"></tbody>
                        </table>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button"
                        data-dismiss="modal">Cancel</button>
                    <button type="button"
                        class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
                       data-mdc-auto-init="MDCRipple" id="unsetOrderItemCapitalCopyFromMaster"><i class="fas fa-copy"></i> Copy From Master</button>
                    <button type="button"
                        class="form-parsley-submit caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
                        data-submit-specific=".form-set-order" data-mdc-auto-init="MDCRipple">Save</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop


@section('added-scripts')
    <script id="setOrderCapitalModal-template" type="text/x-handlebars-template">
        <tr class="order--item">
            <td>
                <input type="hidden" name="order_item_id[]" value="@{{ id }}">
                <div>
                    <div class="color-secondary sub-text-2 no-margin margin-right-10">
                        <a href="{{ route('adminOrders') }}/@{{ order_number }}/view"
                            target="_blank">#@{{ order_number }}</a>
                    </div>
                    <div class="sub-text-3 no-margin">
                        @{{ order_creation_date }}
                    </div>
                </div>
            </td>
            <td class="uppercase sub-text-2">
                <span>
                    <img style="width: 18px; height: 18px;" class="channel-logo" src="{{ asset('') }}@{{ channel_logo }}">
                </span>
                @{{ store_name }}
            </td>
            <td>
                <div class="caboodle-form-group no-margin">
                    <input type="decimal" class="form-control order--item-capital" required value="0" name="order_item_capital[]">
                </div>
            </td>
        </tr>
    </script>
    <script>
        $(function() {
            var modal = $('#editMasterProductModal');
            $('[data-target="#editMasterProductModal"]').on('click', function() {
                var serialized = $(this).data('json');
                var data = serialized;
                try {
                    data = JSON.parse(serialized);
                } catch (error) {}
                if (data.master_sku) {
                    modal.find('#master_sku_modal').html(data.master_sku);
                    modal.find('[name="master_sku"]').val(data.master_sku);
                    modal.find('[name="name"]').val(data.name);
                    modal.find('[name="variant_name"]').val(data.variant_name);
                    modal.find('[name="purchase_price"]').val(data.purchase_price);
                }
            });

            var setOrderCapitalSource = document.getElementById("setOrderCapitalModal-template").innerHTML;
            var setOrderCapitalSourceTemplate = Handlebars.compile(setOrderCapitalSource);
            var currentMasterSKU;
            $('[data-target="#setOrderCapitalModal"]').on('click', function() {
                $('#unsetOrderItemsTbody').html('');
                var serialized = $(this).data('json');
                currentMasterSKU = $(this).data('msku');
                var data = serialized;
                try {
                    data = JSON.parse(serialized);
                } catch (error) {}
                data.forEach(item => {
                    item.channel = item.order.channel.toLowerCase();
                    item.store_name = item.order.store_name.toLowerCase();
                    item.order_creation_date = item.order.order_creation_date;

                    if (item.channel == 'lazada') item.channel_logo = 'img/admin/laz.png';
                    else if (item.channel == 'shopee') item.channel_logo = 'img/admin/shopee.png';
                    else if (item.channel == 'fb marketplace') item.channel_logo = 'img/admin/fb.png';
                    else item.channel_logo = 'img/admin/person.png';

                    var html = setOrderCapitalSourceTemplate(item);
                    $('#unsetOrderItemsTbody').append(html);
                });
            });
            $('#unsetOrderItemCapitalCopyFromMaster').on('click', function(e) {
                e.preventDefault();
                var price = $('#' + currentMasterSKU + ' .master_product__purchase_price').html();
                $('#unsetOrderItemsTbody .order--item-capital').val(parseFloat(price.replace(/\,/g,'')).toFixed(2));
            });
        });

        function viewCallback(data, formElem) {
            if (formElem.hasClass('form-edit') && data.master_product) {
                var serialized = $('#' + data.master_product.master_sku + ' [data-target="#editMasterProductModal"]').data(
                    'json', data.master_product);
                $('#' + data.master_product.master_sku + ' .master_product__name').html(data.master_product.name);
                $('#' + data.master_product.master_sku + ' .master_product__variant_name').html(data.master_product
                    .variant_name);
                $('#' + data.master_product.master_sku + ' .master_product__purchase_price').html(parseFloat(data
                    .master_product.purchase_price).toFixed(2));
            }
            $('#editMasterProductModal').modal('hide');
            if (formElem.hasClass('form-set-order')) {
                location.reload();
            }
        }
    </script>
@endsection
