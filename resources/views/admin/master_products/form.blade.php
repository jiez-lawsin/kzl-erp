<div class="caboodle-form-group">
  <label for="master_sku">Master Sku</label>
  {!! Form::textarea('master_sku', null, ['class'=>'form-control redactor', 'id'=>'master_sku', 'placeholder'=>'Master Sku', 'required', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>
<div class="caboodle-form-group">
  <label for="name">Name</label>
  {!! Form::textarea('name', null, ['class'=>'form-control redactor', 'id'=>'name', 'placeholder'=>'Name', 'required', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>
<div class="caboodle-form-group">
  <label for="variant_name">Variant Name</label>
  {!! Form::text('variant_name', null, ['class'=>'form-control', 'id'=>'variant_name', 'placeholder'=>'Variant Name', 'required']) !!}
</div>
