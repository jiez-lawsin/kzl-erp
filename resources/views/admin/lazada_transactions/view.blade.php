@extends('layouts.admin')

@section('breadcrumbs')
<ol class="breadcrumb">
  <li><a href="{{route('adminDashboard')}}">Dashboard</a></li>
  <li><a href="{{route('adminLazadaTransactions')}}">Lazada Transactions</a></li>
  <li class="active">View</li>
</ol>
@stop

@section('content')
<div class="col-md-8">
	<table class='table table-striped table-bordered table-view'>
		<tr>
			<th>Id</th>
			<td>{!!$data->id!!}</td>
		</tr>
		<tr>
			<th>Order number</th>
			<td>{!!$data->order_number!!}</td>
		</tr>
		<tr>
			<th>Order item number</th>
			<td>{!!$data->order_item_number!!}</td>
		</tr>
		<tr>
			<th>Transaction number</th>
			<td>{!!$data->transaction_number!!}</td>
		</tr>
		<tr>
			<th>Transaction date</th>
			<td>{!!$data->transaction_date!!}</td>
		</tr>
		<tr>
			<th>Transaction type</th>
			<td>{!!$data->transaction_type!!}</td>
		</tr>
		<tr>
			<th>Fee name</th>
			<td>{!!$data->fee_name!!}</td>
		</tr>
		<tr>
			<th>Seller sku</th>
			<td>{!!$data->seller_sku!!}</td>
		</tr>
		<tr>
			<th>Lazada sku</th>
			<td>{!!$data->lazada_sku!!}</td>
		</tr>
		<tr>
			<th>Amount</th>
			<td>{!!$data->amount!!}</td>
		</tr>
		<tr>
			<th>Vat amount</th>
			<td>{!!$data->vat_amount!!}</td>
		</tr>
		<tr>
			<th>Wht amount</th>
			<td>{!!$data->wht_amount!!}</td>
		</tr>
		<tr>
			<th>Wht included</th>
			<td>{!!$data->wht_included!!}</td>
		</tr>
		<tr>
			<th>Statement</th>
			<td>{!!$data->statement!!}</td>
		</tr>
		<tr>
			<th>Paid status</th>
			<td>{!!$data->paid_status!!}</td>
		</tr>
		<tr>
			<th>Order item status</th>
			<td>{!!$data->order_item_status!!}</td>
		</tr>
		<tr>
			<th>Shipping provider</th>
			<td>{!!$data->shipping_provider!!}</td>
		</tr>
		<tr>
			<th>Shipping speed</th>
			<td>{!!$data->shipping_speed!!}</td>
		</tr>
		<tr>
			<th>Shipment type</th>
			<td>{!!$data->shipment_type!!}</td>
		</tr>
		<tr>
			<th>Reference number</th>
			<td>{!!$data->reference_number!!}</td>
		</tr>
		<tr>
			<th>Payment reference id</th>
			<td>{!!$data->payment_reference_id!!}</td>
		</tr>
		<tr>
			<th>Comment</th>
			<td>{!!$data->comment!!}</td>
		</tr>
		<tr>
			<th>Created at</th>
			<td>
				@if ($data->created_at)
				<?php $created_at = new Carbon($data->created_at); ?>
				{{$created_at->toFormattedDateString() . ' ' . $created_at->toTimeString()}}
				@endif
			</td>
		</tr>
		<tr>
			<th>Updated at</th>
			<td>
				@if ($data->updated_at)
				<?php $created_at = new Carbon($data->updated_at); ?>
				{{$created_at->toFormattedDateString() . ' ' . $created_at->toTimeString()}}
				@endif
			</td>
		</tr>
	</table>
</div>
@stop