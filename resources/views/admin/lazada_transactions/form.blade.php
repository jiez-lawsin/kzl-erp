<div class="caboodle-form-group">
  <label for="order_number">Order Number</label>
  {!! Form::text('order_number', null, ['class'=>'form-control', 'id'=>'order_number', 'placeholder'=>'Order Number', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="order_item_number">Order Item Number</label>
  {!! Form::text('order_item_number', null, ['class'=>'form-control', 'id'=>'order_item_number', 'placeholder'=>'Order Item Number', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="transaction_number">Transaction Number</label>
  {!! Form::text('transaction_number', null, ['class'=>'form-control', 'id'=>'transaction_number', 'placeholder'=>'Transaction Number', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="transaction_date">Transaction Date</label>
  {!! Form::text('transaction_date', null, ['class'=>'form-control', 'id'=>'transaction_date', 'placeholder'=>'Transaction Date', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="transaction_type">Transaction Type</label>
  {!! Form::text('transaction_type', null, ['class'=>'form-control', 'id'=>'transaction_type', 'placeholder'=>'Transaction Type', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="fee_name">Fee Name</label>
  {!! Form::text('fee_name', null, ['class'=>'form-control', 'id'=>'fee_name', 'placeholder'=>'Fee Name', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="seller_sku">Seller Sku</label>
  {!! Form::text('seller_sku', null, ['class'=>'form-control', 'id'=>'seller_sku', 'placeholder'=>'Seller Sku', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="lazada_sku">Lazada Sku</label>
  {!! Form::text('lazada_sku', null, ['class'=>'form-control', 'id'=>'lazada_sku', 'placeholder'=>'Lazada Sku', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="amount">Amount</label>
  {!! Form::text('amount', null, ['class'=>'form-control', 'id'=>'amount', 'placeholder'=>'Amount', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="vat_amount">Vat Amount</label>
  {!! Form::text('vat_amount', null, ['class'=>'form-control', 'id'=>'vat_amount', 'placeholder'=>'Vat Amount', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="wht_amount">Wht Amount</label>
  {!! Form::text('wht_amount', null, ['class'=>'form-control', 'id'=>'wht_amount', 'placeholder'=>'Wht Amount', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="wht_included">Wht Included</label>
  {!! Form::text('wht_included', null, ['class'=>'form-control', 'id'=>'wht_included', 'placeholder'=>'Wht Included', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="statement">Statement</label>
  {!! Form::text('statement', null, ['class'=>'form-control', 'id'=>'statement', 'placeholder'=>'Statement', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="paid_status">Paid Status</label>
  {!! Form::text('paid_status', null, ['class'=>'form-control', 'id'=>'paid_status', 'placeholder'=>'Paid Status', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="order_item_status">Order Item Status</label>
  {!! Form::text('order_item_status', null, ['class'=>'form-control', 'id'=>'order_item_status', 'placeholder'=>'Order Item Status', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="shipping_provider">Shipping Provider</label>
  {!! Form::text('shipping_provider', null, ['class'=>'form-control', 'id'=>'shipping_provider', 'placeholder'=>'Shipping Provider', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="shipping_speed">Shipping Speed</label>
  {!! Form::text('shipping_speed', null, ['class'=>'form-control', 'id'=>'shipping_speed', 'placeholder'=>'Shipping Speed', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="shipment_type">Shipment Type</label>
  {!! Form::text('shipment_type', null, ['class'=>'form-control', 'id'=>'shipment_type', 'placeholder'=>'Shipment Type', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="reference_number">Reference Number</label>
  {!! Form::text('reference_number', null, ['class'=>'form-control', 'id'=>'reference_number', 'placeholder'=>'Reference Number', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="payment_reference_id">Payment Reference Id</label>
  {!! Form::text('payment_reference_id', null, ['class'=>'form-control', 'id'=>'payment_reference_id', 'placeholder'=>'Payment Reference Id', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="comment">Comment</label>
  {!! Form::textarea('comment', null, ['class'=>'form-control redactor', 'id'=>'comment', 'placeholder'=>'Comment', 'required', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>
