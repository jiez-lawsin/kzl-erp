@extends('layouts.admin')

@section('breadcrumbs')
<ol class="breadcrumb">
  <li><a href="{{route('adminDashboard')}}">Dashboard</a></li>
  <li><a href="{{route('adminOrderItems')}}">Order Items</a></li>
  <li class="active">View</li>
</ol>
@stop

@section('content')
<div class="col-md-8">
	<table class='table table-striped table-bordered table-view'>
		<tr>
			<th>Id</th>
			<td>{!!$data->id!!}</td>
		</tr>
		<tr>
			<th>Order number</th>
			<td>{!!$data->order_number!!}</td>
		</tr>
		<tr>
			<th>Order item id</th>
			<td>{!!$data->order_item_id!!}</td>
		</tr>
		<tr>
			<th>Name</th>
			<td>{!!$data->name!!}</td>
		</tr>
		<tr>
			<th>Seller sku</th>
			<td>{!!$data->seller_sku!!}</td>
		</tr>
		<tr>
			<th>Master sku</th>
			<td>{!!$data->master_sku!!}</td>
		</tr>
		<tr>
			<th>Original price</th>
			<td>{!!$data->original_price!!}</td>
		</tr>
		<tr>
			<th>Discounted price</th>
			<td>{!!$data->discounted_price!!}</td>
		</tr>
		<tr>
			<th>Capital price</th>
			<td>{!!$data->capital_price!!}</td>
		</tr>
		<tr>
			<th>Quantity</th>
			<td>{!!$data->quantity!!}</td>
		</tr>
		<tr>
			<th>Created at</th>
			<td>
				@if ($data->created_at)
				<?php $created_at = new Carbon($data->created_at); ?>
				{{$created_at->toFormattedDateString() . ' ' . $created_at->toTimeString()}}
				@endif
			</td>
		</tr>
		<tr>
			<th>Updated at</th>
			<td>
				@if ($data->updated_at)
				<?php $created_at = new Carbon($data->updated_at); ?>
				{{$created_at->toFormattedDateString() . ' ' . $created_at->toTimeString()}}
				@endif
			</td>
		</tr>
	</table>
</div>
@stop