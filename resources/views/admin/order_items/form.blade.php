<div class="caboodle-form-group">
  <label for="order_number">Order Number</label>
  {!! Form::text('order_number', null, ['class'=>'form-control', 'id'=>'order_number', 'placeholder'=>'Order Number', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="order_item_id">Order Item Id</label>
  {!! Form::text('order_item_id', null, ['class'=>'form-control', 'id'=>'order_item_id', 'placeholder'=>'Order Item Id', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="name">Name</label>
  {!! Form::textarea('name', null, ['class'=>'form-control redactor', 'id'=>'name', 'placeholder'=>'Name', 'required', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>
<div class="caboodle-form-group">
  <label for="seller_sku">Seller Sku</label>
  {!! Form::textarea('seller_sku', null, ['class'=>'form-control redactor', 'id'=>'seller_sku', 'placeholder'=>'Seller Sku', 'required', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>
<div class="caboodle-form-group">
  <label for="master_sku">Master Sku</label>
  {!! Form::textarea('master_sku', null, ['class'=>'form-control redactor', 'id'=>'master_sku', 'placeholder'=>'Master Sku', 'required', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>
<div class="caboodle-form-group">
  <label for="original_price">Original Price</label>
  {!! Form::text('original_price', null, ['class'=>'form-control', 'id'=>'original_price', 'placeholder'=>'Original Price', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="discounted_price">Discounted Price</label>
  {!! Form::text('discounted_price', null, ['class'=>'form-control', 'id'=>'discounted_price', 'placeholder'=>'Discounted Price', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="capital_price">Capital Price</label>
  {!! Form::text('capital_price', null, ['class'=>'form-control', 'id'=>'capital_price', 'placeholder'=>'Capital Price', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="quantity">Quantity</label>
  {!! Form::text('quantity', null, ['class'=>'form-control', 'id'=>'quantity', 'placeholder'=>'Quantity', 'required']) !!}
</div>
