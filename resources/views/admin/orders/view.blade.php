@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far">
            <a href="{{ route('adminDashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminOrders') }}">Orders</a>
        </li>
        <li class="breadcrumb-item far active" aria-current="page">
            <span>{{ $data->order_number }}</span>
        </li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
	<div style="width: 100%;" class="caboodle-flex-content" style="align-items: baseline;">
		<div class="flex-center">
			<h1 class="margin-bottom-5 margin-right-10" style="flex: initial">{{ $data->order_number }}</h1>
			<div class="flex-center margin-bottom-5">
				<span>
					@if(strtolower($data->channel) == 'lazada')
					<img class="channel-logo" src="{{ asset('img/admin/laz.png') }}">
					@endif
					@if(strtolower($data->channel) == 'shopee')
					<img class="channel-logo" src="{{ asset('img/admin/shopee.png') }}">
					@endif
				</span>
				<h4 class="color-grey no-margin margin-left-5 margin-right-10">{{$data->store_name}}</h4>
				<span class="caboodle-tag uppercase
					@if(strtolower($data->status) == 'cancelled')
					caboodle-tag-grey 
					@elseif(strtolower($data->status) == 'delivered')
					caboodle-tag-success 
					@elseif(strtolower($data->status) == 'shipping')
					caboodle-tag-secondary 
					@elseif(strtolower($data->status) == 'ready to ship')
					caboodle-tag-purple 
					@elseif(strtolower($data->status) == 'failed')
					caboodle-tag-danger 
					@elseif(strtolower($data->status) == 'returned')
					caboodle-tag-warning 
					@else
					caboodle-tag-light
					@endif
					small">{{ $data->status}}</span>
			</div>
		</div>
		<div class="sub-text-2">Created At: <b>{{ $data->order_creation_date }}</b></div>
		<div class="sub-text-2">Updated At: <b>{{ $data->order_update_date }}</b></div>
	</div>
	<div class="header-actions">
		{{-- <a href="{{ route('adminOrders') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a> --}}
	</div>
</header>
@stop

{{-- @section('footer')
<footer>
	<div class="text-right">
		<a href="{{ route('adminOrders') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
		<button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
	</div>
</footer> --}}

@section('content')
    <div class="row">
        <div class="col-sm-7">
			<div class="caboodle-card">
				<div class="caboodle-card-header pad-bottom-10">
					<h4 class="no-margin"><i class="fas fa-user"></i> Customer Information</h4>
				</div>
				<div class="caboodle-card-body pad-top-10">
					<table class="caboodle-table">
						<tr>
							<td class="sub-text-2">Buyer Account</td>
							<td>{{ $data->buyer_name }}</td>
						</tr>
						<tr>
							<td class="sub-text-2">Name</td>
							<td>{{ $data->recipient_name }}</td>
						</tr>
						<tr>
							<td class="sub-text-2">Address</td>
							<td>{{ $data->recipient_address }}</td>
						</tr>
						<tr>
							<td class="sub-text-2">Phone</td>
							<td>{{ $data->recipient_phone }}</td>
						</tr>
					</table>
				</div>
			</div>
            <div class="caboodle-card">
				<div class="caboodle-card-header pad-bottom-10">
					<h4 class="no-margin"><i class="far fa-box"></i> Items</h4>
				</div>
                <div class="caboodle-card-body pad-top-10">
					<table class="caboodle-table">
						<thead>
							<tr>
								<th>Product Name</th>
								<th class="text-center">Qty</th>
								<th class="text-right" style="width: 150px;">Amount</th>
							</tr>
						</thead>
						<?php 
							$subtotal = $data->shipping_fee;
						?>
						@foreach ($data->items as $item)
						<?php
							$subtotal += $item->discounted_price * $item->quantity;
						?>
						<tr>
							<td class="color-secondary sub-text-2">
								@if (@$item->master->name)
									{{ $item->master->name }} 
									@if ($item->master->variant_name != '' && $item->master->variant_name != '-')
										<br> <span class="color-grey">{{ $item->master->variant_name }}<span> 
									@endif
									<br> <span class="color-grey">MSKU: {{ $item->master->master_sku }}</span>
								@else 
								{{ $item->name }}
								@endif
							</td>
							<td class="text-center color-grey">{{ $item->quantity }}</td>
							<td class="text-right">₱ {{ number_format($item->discounted_price * $item->quantity, 2) }}</td>
						</tr>
						@endforeach
						
					</table>
					<hr>
					<table class="caboodle-table">
						<tr class="no-border">
							<td class="text-right"><h5>Shipping</h5></td>
							<td style="width: 150px;" class="color-grey text-right"><h5>₱ {{ number_format($data->shipping_fee, 2) }}</h5></td>
						</tr>
						<tr class="no-border">
							<td class="text-right"><h4>Subtotal</h4></td>
							<td style="width: 150px;" class="text-right"><h4>₱ {{ number_format($subtotal, 2) }}</h4></td>
						</tr>
					</table>
                </div>
            </div>
        </div>
		<div class="col-sm-5">
			@if ($data->channel == 'Shopee')
				@if ($data->shopee_transaction)
				<?php
					$trx = $data->shopee_transaction;
				?>
				<div class="caboodle-card">
					<div class="caboodle-card-header pad-bottom-10">
						<h4 class="no-margin"><i class="fas fa-credit-card"></i> Payout Details <span class="float-right uppercase caboodle-tag caboodle-tag-success">PAID: {{ Carbon::parse($trx->payout_completed_date)->format('M d, Y') }}</span></h4>
					</div>
					<div class="caboodle-card-body pad-top-10">
						<table class="caboodle-table">
							<tr>
								<td class="sub-text-1">Products Total</td>
								<td class="text-right sub-text-1">₱ {{ number_format($trx->original_price, 2) }}</td>
							</tr>
							@if($trx->seller_product_promotion > 0)
							<tr>
								<td class="sub-text-2 color-grey">Seller Product Promotion</td>
								<td class="text-right sub-text-2 {{ $trx->seller_product_promotion >= 0 ? '' : 'color-red' }}">₱ {{ number_format($trx->seller_product_promotion, 2) }}</td>
							</tr>
							@endif
							@if($trx->buyer_refund_amount > 0)
							<tr>
								<td class="sub-text-2 color-grey">Refund Amount to Buyer</td>
								<td class="text-right sub-text-2 {{ $trx->buyer_refund_amount >= 0 ? '' : 'color-red' }}">₱ {{ number_format($trx->buyer_refund_amount, 2) }}</td>
							</tr>
							@endif
							@if($trx->product_discount_rebate > 0)
							<tr>
								<td class="sub-text-2 color-grey">Product Discount Rebate</td>
								<td class="text-right sub-text-2 {{ $trx->product_discount_rebate >= 0 ? '' : 'color-red' }}">₱ {{ number_format($trx->product_discount_rebate, 2) }}</td>
							</tr>
							@endif
							@if($trx->seller_voucher_discount > 0)
							<tr>
								<td class="sub-text-2 color-grey">Seller Voucher Discount</td>
								<td class="text-right sub-text-2 {{ $trx->seller_voucher_discount >= 0 ? '' : 'color-red' }}">₱ {{ number_format($trx->seller_voucher_discount, 2) }}</td>
							</tr>
							@endif
							@if($trx->seller_absorbed_coin_cashback > 0)
							<tr>
								<td class="sub-text-2 color-grey">Seller Absorbed Coin Cashback</td>
								<td class="text-right sub-text-2 {{ $trx->seller_absorbed_coin_cashback >= 0 ? '' : 'color-red' }}">₱ {{ number_format($trx->seller_absorbed_coin_cashback, 2) }}</td>
							</tr>
							@endif
							<tr>
								<td class="sub-text-2 color-grey">Shipping Fee Paid by Buyer</td>
								<td class="text-right sub-text-2">₱ {{ number_format($trx->buyer_paid_shipping_fee, 2) }}</td>
							</tr>
							@if($trx->shipping_fee_rebate > 0)
							<tr>
								<td class="sub-text-2 color-grey">Shipping Fee Rebate</td>
								<td class="text-right sub-text-2">₱ {{ number_format($trx->shipping_fee_rebate, 2) }}</td>
							</tr>
							@endif
							<tr>
								<td class="sub-text-2 color-grey">3rd Party Logistics - Defined Shipping Fee</td>
								<td class="text-right sub-text-2 {{ $trx->third_party_logistics_shipping >= 0 ? '' : 'color-red' }}">₱ {{ number_format($trx->third_party_logistics_shipping, 2) }}</td>
							</tr>
							@if($trx->reverse_shipping_fee > 0)
							<tr>
								<td class="sub-text-2 color-grey">Reverse Shipping Fee</td>
								<td class="text-right sub-text-2 {{ $trx->reverse_shipping_fee >= 0 ? '' : 'color-red' }}">₱ {{ number_format($trx->reverse_shipping_fee, 2) }}</td>
							</tr>
							@endif
							@if($trx->commission_fee > 0)
							<tr>
								<td class="sub-text-2 color-grey">Commission fee</td>
								<td class="text-right sub-text-2 {{ $trx->commission_fee >= 0 ? '' : 'color-red' }}">₱ {{ number_format($trx->commission_fee, 2) }}</td>
							</tr>
							@endif
							<tr>
								<td class="sub-text-2 color-grey">Service Fee</td>
								<td class="text-right sub-text-2 {{ $trx->service_fee >= 0 ? '' : 'color-red' }}">₱ {{ number_format($trx->service_fee, 2) }}</td>
							</tr>
							<tr>
								<td class="sub-text-2 color-grey">Transaction Fee</td>
								<td class="text-right sub-text-2 {{ $trx->transaction_fee >= 0 ? '' : 'color-red' }}">₱ {{ number_format($trx->transaction_fee, 2) }}</td>
							</tr>
							<tr>
								<td><h5>Total Payout</h5></td>
								<td class="text-right"><h4>₱ {{ number_format($trx->total_released_amount, 2) }}</h4></td>
								<?php 
									$totalPayout = $trx->total_released_amount;
								?>
							</tr>
						</table>
					</div>
				</div>
				@endif
			@endif

			
			@if ($data->channel == 'Lazada')
			@if ($data->laz_transactions)
			<?php
				$paid_transactions = $data->laz_transactions()->where('paid_status', 'paid')->count();
			?>
			<div class="caboodle-card">
				<div class="caboodle-card-header pad-bottom-10">
					<h4 class="no-margin inline">
						<i class="fas fa-credit-card"></i>
						Payout Details
					</h4>
					<div class="float-right">
						@if ($paid_transactions == count($data->laz_transactions) && count($data->laz_transactions) > 0)
							<span class="caboodle-tag uppercase caboodle-tag-success">Paid</span>
						@elseif ($paid_transactions != 0 && $paid_transactions < count($data->laz_transactions))
							<span class="caboodle-tag uppercase caboodle-tag-secondary">Partially Paid</span>
						@else 
							<span class="caboodle-tag uppercase caboodle-tag-warning">Pending</span>
						@endif
					</div>
				</div>
				<div class="caboodle-card-body pad-top-10">
					<?php
						$laz_fee_name_trx = $data->laz_transactions()->groupBy('fee_name')->select('fee_name', 'id')->orderBy('id')->get();
						$totalPayout = 0;
					?>
					<table class="caboodle-table">
						@foreach($laz_fee_name_trx as $trx)
						<?php
							$totalAmount = 0;
							$laz_trx = $data->laz_transactions()->where('fee_name', $trx->fee_name)->get();
							foreach ($laz_trx as $lt) {
								$totalAmount += $lt->amount;
							}
							$totalPayout += $totalAmount;
						?>
						<tr>
							<td>{{ $trx->fee_name }}</td>
							<td class="text-right {{ $totalAmount >= 0 ? '' : 'color-red' }}">₱ {{ number_format($totalAmount, 2) }}</td>
						</tr>
						@endforeach
						<tr>
							<td><h5>Total Payout</h5></td>
							<td class="text-right"><h4>₱ {{ number_format($totalPayout, 2) }}</h4></td>
						</tr>
					</table>
				</div>
			</div>
			@endif
			@endif
			
			<div class="caboodle-card">
				<div class="caboodle-card-header pad-bottom-10">
					<h5 class="no-margin">Profit Details</h5>
				</div>
				<div class="caboodle-card-body pad-top-10">
					<table class="caboodle-table">
						<thead>
							<tr>
								<th>Product Name</th>
								<th class="text-right" style="width: 150px;">Purchase Price</th>
								<th class="text-center">Qty</th>
							</tr>
						</thead>
						<?php 
							$totalCapital = 0;
						?>
						@foreach ($data->items as $item)
						<?php
							$totalCapital += $item->capital_price;
						?>
						<tr class="profit__order-item" id="prodit__order-item-{{ $item->id }}">
							<td class="sub-text-2 color-secondary">
								@if (@$item->master->name)
								{{ $item->master->name }}
									@if ($item->master->variant_name != '' && $item->master->variant_name != '-')
										<br> <span class="color-grey">{{ $item->master->variant_name }}<span> 
									@endif
								<br><span class="color-grey sub-text-3">MSKU: {{ $item->master->master_sku }}</span>
								@else 
								{{ $item->name }}
								@endif
							</td>
							<td class="vertical-align-top text-right color-black sub-text-2">
								₱ <span class="profit__order-item__purchase_price">{{ number_format($item->capital_price, 2) }}</span> &nbsp;
								<a href="#" data-toggle="modal" data-target="#editPurchasePriceModal" data-json="{{ json_encode($item) }}">
									<i class="small color-secondary fal fa-edit"></i>
								</a>
							</td>
							<td class="vertical-align-top  text-center color-grey sub-text-2">x {{ $item->quantity }}</td>
						</tr>
						@endforeach
						
					</table>
					<hr>
					<table class="caboodle-table">
						<tr class="no-border">
							<td class="text-right"><h5>Total Paid Out</h5></td>
							<td style="width: 150px;" class="color-grey text-right"><h5>₱ {{ number_format(@$totalPayout, 2) }}</h5></td>
						</tr>
						<tr class="no-border">
							<td class="text-right"><h5>Capital</h5></td>
							<td style="width: 150px;" class="color-grey text-right"><h5>₱ <span id="profit__total_capital">{{ number_format($totalCapital, 2) }}</span></h5></td>
						</tr>
						<tr class="no-border">
							<td class="text-right"><h4>Net Profit</h4></td>
							<td style="width: 150px;" class="text-right"><h4>₱ <span id="profit__net_profit">{{ number_format(@$totalPayout - $totalCapital, 2) }}</span></h4></td>
						</tr>
					</table>
				</div>
			</div>
			
			@if ($data->channel == 'Lazada')
			@if ($data->laz_transactions)
			<div class="caboodle-card">
				<div class="caboodle-card-header pad-bottom-10">
					<h5 class="no-margin">Lazada Transaction Summary</h5>
				</div>
				<div class="caboodle-card-body pad-top-10">
					<table class="caboodle-table">
						@foreach ($data->laz_transactions as $trx)
						<tr>
							<td class="sub-text-2">{{ $trx->fee_name }}</td>
							<td class="sub-text-2">{{ $trx->seller_sku }}</td>
							<td class="text-right sub-text-2 {{ $trx->amount >= 0 ? 'color-grey' : 'color-red' }}">₱ {{ number_format($trx->amount, 2) }}</td>
							<td class="uppercase sub-text-2 {{ $trx->paid_status == 'paid' ? 'color-green' : 'color-grey' }}">{{ $trx->paid_status }}</td>
						</tr>
						@endforeach
					</table>
				</div>
			</div>
			@endif
			@endif
		</div>
    </div>

	<div class="modal fade" id="editPurchasePriceModal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="editPurchasePriceLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div>
                        <h5 class="modal-title" id="editPurchasePriceLabel">Edit Purchase Price</h5>
						<hr>
						<div class="color-secondary">
							<div class="product_name__modal"></div>
							<div class="sub-text-2 variant_name__modal"></div>
							<div class="sub-text-2">MSKU: <span class="master_sku__modal"></span></div>
						</div>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::model($data, ['route' => 'adminOrderItemUpdatePurchasePrice', 'files' => true, 'method' => 'patch', 'class' => 'form form-parsley form-edit']) !!}
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="id">
                    <div class="caboodle-form-group">
                        {!! Form::number('purchase_price', null, ['step'=>'any', 'class' => 'form-control', 'id' => 'purchase_price', 'placeholder' => 'Purchase Price', 'required']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button"
                        data-dismiss="modal">Cancel</button>
                    <button type="button"
                        class="form-parsley-submit caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
                        data-submit-specific=".form-edit" data-mdc-auto-init="MDCRipple">Save</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('added-scripts')
    <script>
		const totalPayout = {{ @$totalPayout }};
        $(function() {
            var modal = $('#editPurchasePriceModal');
            $('[data-target="#editPurchasePriceModal"]').on('click', function() {
				modal.find('.variant_name__modal').html('');
				modal.find('.master_sku__modal').html('');
                var serialized = $(this).data('json');
                var data = serialized;
                try {
                    data = JSON.parse(serialized);
                } catch (error) {}
				console.log(data);
                if (data.id) {
					var product_name = data.name;
					modal.find('.master_sku__modal').parent().addClass('hidden');
					if (data.master && data.master.name) {
						product_name = data.master.name;
						if (data.master.variant_name){
							modal.find('.variant_name__modal').html(data.master.variant_name);
						}
						modal.find('.master_sku__modal').parent().removeClass('hidden');
						modal.find('.master_sku__modal').html(data.master_sku);
					}
					console.log(data.id);
					modal.find('.product_name__modal').html(product_name);
                    modal.find('[name="id"]').val(data.id);
                    modal.find('[name="purchase_price"]').val(data.capital_price ? data.capital_price : 0);
                }
            });
        });

        function viewCallback(data, formElem) {
			if (formElem.hasClass('form-edit') && data.order_item) {
				var serialized = $('#prodit__order-item-' + data.order_item.id + ' [data-target="#editPurchasePriceModal"]').data('json', data.order_item);
				$('#prodit__order-item-' + data.order_item.id + ' .profit__order-item__purchase_price').html(parseFloat(data.order_item.capital_price).toFixed(2));
				$('#editPurchasePriceModal').modal('hide');
				recomputeProfitDetails();
			}
        }
		$('.profit__order-item').each(function (index, elem) {
			var capital = $(elem).find('.profit__order-item__purchase_price').html();
			console.log(capital);
		});
		function recomputeProfitDetails() {
			var totalCapital = 0;
			$('.profit__order-item').each(function (index, elem) {
				var capital = $(elem).find('.profit__order-item__purchase_price').html();
				totalCapital += parseFloat(capital);
			});
			$('#profit__total_capital').html(totalCapital.toFixed(2));
			$('#profit__net_profit').html(parseFloat(parseFloat(totalPayout).toFixed(2) - parseFloat(totalCapital).toFixed(2)).toFixed(2));
		}
    </script>
@endsection
