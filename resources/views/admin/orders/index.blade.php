@extends('layouts.admin')

@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item far"><a href="{{ route('adminDashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item far active"><span>Orders</span></li>
        </ol>
    </nav>
@stop

@section('header')
    <header class="flex-center">
        <h1>{{ $title }}</h1>
        <div class="header-actions">
            <a class="margin-right-10 caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
                data-mdc-auto-init="MDCRipple" href="{{ route('adminOrdersCreate') }}">
                Create
            </a>
            <a class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
                data-mdc-auto-init="MDCRipple" href="#" data-toggle="modal" data-target="#importOrdersModal">
                Import
            </a>
        </div>
    </header>
    <div class="modal fade" id="importOrdersModal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="importOrdersModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="importOrdersModalLabel">Import Orders From Ginee</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('adminOrdersImport') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="file" name="file" class="" id="chooseFile">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button"
                            data-dismiss="modal">Cancel</button>
                        <button type="submit"
                            class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated "
                            data-mdc-auto-init="MDCRipple">Import</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="caboodle-card">
                <div class="caboodle-card-header">
                    <div class="filters no-padding">
                        {!! Form::open(['route' => 'adminOrders', 'method' => 'get', 'class' => 'no-margin']) !!}
                        <div
                            class="caboodle-form-group caboodle-flex caboodle-flex-row caboodle-flex-left caboodle-form-control-connected">
                            <label class="no-padding" for="order_number">
                                {!! Form::text('order_number', @$request['order_number'], ['class' => 'form-control input-sm no-margin', 'placeholder' => 'Order Number']) !!}
                            </label>
                            &nbsp;
                            <label class="no-padding" for="product_name">
                                {!! Form::text('product_name', @$request['product_name'], ['class' => 'form-control input-sm no-margin', 'placeholder' => 'Product Name']) !!}
                            </label>
                            &nbsp;
                            <label class="no-padding" for="master_sku">
                                {!! Form::text('master_sku', @$request['master_sku'], ['class' => 'form-control input-sm no-margin', 'placeholder' => 'Master SKU']) !!}
                            </label>
                            &nbsp;
                            <label class="single-search no-padding" for="date_range">
                                {!! Form::text('date_range', @$request['date_range'], ['autocomplete' => 'off', 'class' => 'input-date-range form-control input-sm no-margin', 'placeholder' => 'Date Range']) !!}
                            </label>
                            &nbsp;
                            <label class="no-padding" for="channel">
                                <select class="form-control input-sm no-margin" name="channel">
                                    <option value="" disabled selected>Select Channel</option>
                                    <option value="">[All]</option>
                                    @foreach ($channels as $channel)
                                        <option @if (@$request['channel'] == $channel->channel) selected @endif value="{{ $channel->channel }}">
                                            {{ $channel->channel }}
                                        </option>
                                    @endforeach
                                </select>
                            </label>
                            &nbsp;
                            <label class="no-padding" for="store">
                                <select class="form-control input-sm no-margin" name="store">
                                    <option value="" disabled selected>Select Store</option>
                                    <option value="">[All]</option>
                                    @foreach ($stores as $store)
                                        <option @if (@$request['store'] == $store->store_name) selected @endif value="{{ $store->store_name }}">
                                            {{ $store->store_name }}
                                        </option>
                                    @endforeach
                                </select>
                            </label>
                            &nbsp;
                            <label class="no-padding" for="status">
                                <select class="form-control input-sm no-margin" name="status">
                                    <option value="" disabled selected>Status</option>
                                    <option value="">[All]</option>
                                    @foreach ($statuses as $status)
                                        <option @if (@$request['status'] == $status->status) selected @endif value="{{ $status->status }}">
                                            {{ $status->status }}
                                        </option>
                                    @endforeach
                                </select>
                            </label>
                            &nbsp;
                            <button type="submit"
                                class="caboodle-btn caboodle-btn-small caboodle-btn-primary mdc-button mdc-button--unelevated "
                                data-mdc-auto-init="MDCRipple">Filter</button>
                            <!-- {!! Form::hidden('sort', @$sort) !!} {!! Form::hidden('sortBy', @$sortBy) !!} -->
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="caboodle-card-body">
                    @if (count($data) > 0)
                        {!! Form::open(['route' => 'adminOrdersDestroy', 'method' => 'delete', 'class' => 'form form-parsley form-delete']) !!}
                        <table class="caboodle-table">
                            <thead>
                                <tr>
                                    <th width="50px">
                                        <div class="mdc-form-field" data-toggle="tooltip" title="Select All">
                                            <div class="mdc-checkbox caboodle-table-select-all">
                                                <input type="checkbox" class="mdc-checkbox__native-control"
                                                    name="select_all" />
                                                <div class="mdc-checkbox__background">
                                                    <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                                        <path class="mdc-checkbox__checkmark-path" fill="none"
                                                            stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                                    </svg>
                                                    <div class="mdc-checkbox__mixedmark"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="caboodle-table-col-action">
                                        <a class="caboodle-btn caboodle-btn-icon caboodle-btn-danger mdc-button mdc-ripple-upgraded mdc-button--unelevated x-small uppercase"
                                            data-mdc-auto-init="MDCRipple" href="{{ route('adminOrdersDestroy') }}"
                                            method="DELETE" data-toggle-alert="warning"
                                            data-alert-form-to-submit=".form-delete" permission-action="delete"
                                            data-notif-message="Deleting...">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </th>
                                    <th width="180px" class="caboodle-table-col-header hide">Order Number</th>
                                    <th width="30%" class="caboodle-table-col-header hide">Items</th>
                                    <th class="caboodle-table-col-header hide">Subtotal</th>
                                    <th class="caboodle-table-col-header hide">Status</th>
                                    <th class="caboodle-table-col-header hide">Store</th>
                                    <th class="caboodle-table-col-header hide">Date</th>
                                    <th colspan="100%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $d)
                                    <tr>
                                        <td class="no-top-padding vertical-align-top">
                                            <div class="mdc-form-field">
                                                <div class="mdc-checkbox">
                                                    <input type="checkbox" class="mdc-checkbox__native-control" name="ids[]"
                                                        value="{{ $d->id }}" />
                                                    <div class="mdc-checkbox__background">
                                                        <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                                            <path class="mdc-checkbox__checkmark-path" fill="none"
                                                                stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                                        </svg>
                                                        <div class="mdc-checkbox__mixedmark"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="color-secondary vertical-align-top">
                                            <a href="{{ route('adminOrdersView', $d->order_number) }}"
                                                target="_blank">{{ $d->order_number }}</a>
                                        </td>
                                        <td>
                                            @foreach ($d->items as $key => $item)
                                                <div class="margin-bottom-10">
                                                    <div class="color-secondary sub-text-2 no-margin margin-right-10">
                                                        @if (@$item->master->name)
                                                            {{ $item->master->name }}
                                                            @if ($item->master->variant_name != '' && $item->master->variant_name != '-')
                                                                <br> <span
                                                                    class="color-grey">{{ $item->master->variant_name }}<span>
                                                            @endif
                                                            <br> <span class="color-grey">MSKU:
                                                                {{ $item->master->master_sku }}</span>
                                                        @else
                                                            {{ $item->name }}
                                                        @endif
                                                    </div>
                                                    <div class="sub-text-2">
                                                        ₱ {{ number_format($item->discounted_price, 2) }}
                                                        x {{ $item->quantity }}
                                                    </div>
                                                </div>
                                            @endforeach
                                        </td>
                                        <td class="vertical-align-top">₱ {{ number_format($d->subtotal, 2) }}</td>
                                        <td class="vertical-align-top uppercase">
                                            <span
                                                class="caboodle-tag uppercase
                                                @if (strtolower($d->status) == 'cancelled')
                                                caboodle-tag-grey 
                                                @elseif(strtolower($d->status) == 'delivered')
                                                caboodle-tag-success 
                                                @elseif(strtolower($d->status) == 'shipping')
                                                caboodle-tag-secondary 
                                                @elseif(strtolower($d->status) == 'ready to ship')
                                                caboodle-tag-purple 
                                                @elseif(strtolower($d->status) == 'failed')
                                                caboodle-tag-danger 
                                                @elseif(strtolower($d->status) == 'returned')
                                                caboodle-tag-warning 
                                                @else
                                                caboodle-tag-light
                                                @endif
                                                xx-small">{{ $d->status }}</span>
                                        </td>
                                        <td class="uppercase sub-text-2 vertical-align-top">
                                            <span>
                                                @if (strtolower($d->channel) == 'lazada')
                                                    <img class="channel-logo" src="{{ asset('img/admin/laz.png') }}">
                                                @elseif (strtolower($d->channel) == 'shopee')
                                                    <img class="channel-logo"
                                                        src="{{ asset('img/admin/shopee.png') }}">
                                                @elseif (strtolower($d->channel) == 'fb marketplace')
                                                    <img class="channel-logo" src="{{ asset('img/admin/fb.png') }}">
                                                @else
                                                    <img class="channel-logo" src="{{ asset('img/admin/person.png') }}">
                                                @endif
                                            </span>
                                            {{ $d->store_name }}
                                        </td>
                                        <td class="vertical-align-top sub-text-2">
                                            @if ($d->order_creation_origin == 'manual')
                                                <div class="sub-text-3">Ordered At</div>
                                            @else
                                                <div class="sub-text-3">Created At</div>
                                            @endif
                                            {{ $d->order_creation_date }}
                                            <br>
                                            <div class="sub-text-3">Updated At</div>
                                            @if ($d->order_creation_origin == 'manual')
                                                {{ $d->updated_at }}
                                            @else
                                                {{ $d->order_update_date }}
                                            @endif
                                        </td>
                                        <td width="110px" class="text-center">
                                            @if (Auth::user()->type == 'super')
                                                <a href="{{ route('adminOrdersView', [$d->order_number]) }}"
                                                    class="mdc-icon-toggle animated-icon" data-toggle="tooltip"
                                                    title="Manage" role="button" aria-pressed="false"
                                                    permission-action="edit" target="_blank">
                                                    <i class="far fa-eye" aria-hidden="true"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! Form::close() !!}
                    @else
                        <div class="empty text-center">
                            No results found
                        </div>
                    @endif
                    @if ($pagination)
                        <div class="caboodle-pagination">
                            {{ $data->links('layouts.pagination') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop


@section('added-scripts')
    <script>
        $(function() {
            $('input.input-date-range').daterangepicker({
                opens: 'left',
                autoUpdateInput: false,
            }, function(start, end, label) {
                $('input.input-date-range').val(start.format('MM/DD/YYYY') + ' - ' + end.format(
                    'MM/DD/YYYY'));
            });
        });
    </script>
@endsection
