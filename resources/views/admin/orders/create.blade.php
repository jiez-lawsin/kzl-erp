@extends('layouts.admin')

@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item far">
                <a href="{{ route('adminDashboard') }}">Dashboard</a>
            </li>
            <li class="breadcrumb-item far">
                <a href="{{ route('adminOrders') }}">Orders</a>
            </li>
            <li class="breadcrumb-item far active" aria-current="page">
                <span>{{ $order_number }}</span>
            </li>
        </ol>
    </nav>
@stop

@section('header')
    <header class="flex-center">
        @if(@$data)
        <div style="width: 100%;" class="caboodle-flex-content" style="align-items: baseline;">
            <div class="flex-center">
                <h1 class="margin-bottom-5 margin-right-10" style="flex: initial">{{ $data->order_number }}</h1>
                <div class="flex-center margin-bottom-5">
                    <span class="caboodle-tag uppercase
                        @if(strtolower($data->status) == 'cancelled')
                        caboodle-tag-grey 
                        @elseif(strtolower($data->status) == 'delivered')
                        caboodle-tag-success 
                        @elseif(strtolower($data->status) == 'shipping')
                        caboodle-tag-secondary 
                        @elseif(strtolower($data->status) == 'ready to ship')
                        caboodle-tag-purple 
                        @elseif(strtolower($data->status) == 'failed')
                        caboodle-tag-danger 
                        @elseif(strtolower($data->status) == 'returned')
                        caboodle-tag-warning 
                        @else
                        caboodle-tag-light
                        @endif
                        small">{{ $data->status}}</span>
                </div>
            </div>
            @if (@$data->order_creation_origin == 'manual')
                <div class="sub-text-2">Ordered At: <b>{{ $data->order_creation_date }}</b></div>
                <div class="sub-text-2">Updated At: <b>{{ $data->updated_at }}</b></div>
            @else
                <div class="sub-text-2">Created At: <b>{{ $data->order_creation_date }}</b></div>
                <div class="sub-text-2">Updated At: <b>{{ $data->order_update_date }}</b></div>
            @endif
        </div>
        @else
        <h1>{{ $title }}</h1>
        @endif
        <div class="header-actions">
            <a href="{{ route('adminOrders') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button"
                data-mdc-auto-init="MDCRipple">Cancel</a>
            <button
                class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit"
                data-mdc-auto-init="MDCRipple">Save</button>
        </div>
    </header>
@stop

@section('footer')
    <footer>
        <div class="text-right">
            <a href="{{ route('adminOrders') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button"
                data-mdc-auto-init="MDCRipple">Cancel</a>
            <button
                class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit"
                data-mdc-auto-init="MDCRipple">Save</button>
        </div>
    </footer>
@stop

@section('content')
    {!! Form::model(@$data, ['route' => 'adminOrdersStore', 'files' => true, 'method' => 'post', 'class' => 'form form-parsley form-create']) !!}
    <input type="hidden" name="order_number" value="{{ $order_number }}">
    <div class="row">
        <div class="col-sm-7">
            <div class="caboodle-card">
                <div class="caboodle-card-header">
                    <h4 class="no-margin">
                        <i class="far fa-box"></i> Items
                        <button type="button" data-toggle="modal" data-target="#searchMasterProductModal"
                            class="float-right caboodle-btn caboodle-btn-xx-small caboodle-btn-clear mdc-button"><i
                                class="fal fa-plus"></i> Add</button>
                    </h4>
                </div>
                <div class="caboodle-card-body no-bottom-padding">
                    <table class="caboodle-table">
                        <thead>
                            <tr>
                                <th>Product Name</th>
                                <th class="text-right" style="width: 150px;">Price</th>
                                <th class="text-center" style="width: 100px;">Qty</th>
                                <th class="text-center" style="width: 50px;"></th>
                            </tr>
                        </thead>
                        <tbody id="orderItemsBody">
                        </tbody>
                    </table>
                </div>
                <div class="caboodle-card-footer pad-top-10">
                    <table class="caboodle-table">
                        <tr>
                            <td class="text-right color-dark-grey">
                                <h4 class="no-margin">Subtotal</h4>
                            </td>
                            <td class="text-right" style="width: 150px">
                                <h4 class="no-margin"><span id="orderSubtotal">0</span></h4>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="caboodle-card">
                <div class="caboodle-card-header">
                    <h4 class="no-margin">
                        <i class="far fa-clipboard-list"></i> Other Transactions
                        <button type="button" data-toggle="modal" data-target="#addOrderTransationModal"
                            class="float-right caboodle-btn caboodle-btn-xx-small caboodle-btn-clear mdc-button"><i
                                class="fal fa-plus"></i> Add</button>
                    </h4>
                </div>
                <div class="caboodle-card-body">
                    <table class="caboodle-table">
                        <tbody id="otherTransactionsBody"></tbody>
                    </table>
                </div>
            </div>
            <div class="caboodle-card">
                <div class="caboodle-card-header">
                    <h4 class="no-margin">
                        Total Order Amount
                        <span class="color-secondary float-right"><span id="orderTotalAmount">0</span></span>
                    </h4>
                </div>
            </div>
        </div>
        <div class="col-sm-5">
            <div class="caboodle-card">
                <div class="caboodle-card-header">
                    <h4 class="no-margin"><i class="far fa-cube"></i> <span
                            class="color-green">#{{ $order_number }}</span></h4>
                </div>
                <div class="caboodle-card-body">
                    @include('admin.orders.form')
                </div>
            </div>
            <input type="hidden" name="order_items">
        </div>
    </div>
    {!! Form::close() !!}

    <div class="modal fade" id="searchMasterProductModal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="searchMasterProductModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="searchMasterProductModalLabel">Search Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @csrf
                <div class="modal-body">
                    <div
                        class="caboodle-form-group caboodle-flex flex caboodle-flex-row caboodle-flex-left caboodle-form-control-connected">
                        <label class="flex-1 no-margin single-search no-padding">
                            {!! Form::text('product_keyword', null, ['class' => 'form-control input-sm no-margin', 'placeholder' => 'Product Name']) !!}
                        </label>
                        <button type="button" id="searchProductName"
                            class="margin-left-10 caboodle-btn caboodle-btn-small caboodle-btn-primary mdc-button mdc-button--unelevated "
                            data-mdc-auto-init="MDCRipple">Search</button>
                    </div>
                    <div class="caboodle-table-container" style="max-height: 500px; overflow: auto;">
                        <div class="elipsis-loader list hidden" id="search-product-loader">
                            <div></div>
                            <div style="animation-delay: 0.5s"></div>
                            <div style="animation-delay: 1s"></div>
                        </div>
                        <form action="#" id="searchedProductForm">
                            <table class="caboodle-table">
                                <tbody id="searched-product-results"></tbody>
                            </table>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button"
                        data-dismiss="modal">Cancel</button>
                    <button type="submit"
                        class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated "
                        data-mdc-auto-init="MDCRipple" id="addMasterProductsButton">Add</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addOrderTransationModal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="editMasterProductLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div>
                        <h5 class="modal-title">Add Transaction</h5>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="#" id="addOrderTransationForm">
                    <div class="modal-body">
                        <div class="caboodle-form-group margin-bottom-15">
                            <label for="transaction_type">Transaction Type</label>
                            {!! Form::text('transaction_type', null, ['class' => 'form-control', 'id' => 'transaction_type', 'placeholder' => 'Transaction Type', 'required']) !!}
                        </div>
                        <div class="caboodle-form-group margin-bottom-15">
                            <label for="amount">Amount</label>
                            {!! Form::number('amount', null, ['step' => 'any', 'class' => 'form-control', 'id' => 'amount', 'placeholder' => 'Amount', 'required']) !!}
                        </div>
                        <div class="caboodle-form-group margin-bottom-15">
                            <label for="comment">Comment</label>
                            {!! Form::text('comment', null, ['class' => 'form-control', 'id' => 'comment', 'placeholder' => 'Comment']) !!}
                        </div>
                    </div>
                    <div class="modal-footer no-top-padding">
                        <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button"
                            data-dismiss="modal">Cancel</button>
                        <button type="submit"
                            class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated "
                            data-mdc-auto-init="MDCRipple" id="addOrderTransationButton">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('added-scripts')
    @include('admin.orders.templates')
    <script>
        $(function() {
            var currency = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'PHP',
            });

            var searchedProductItemSource = document.getElementById("searched-product-item-template").innerHTML;
            var searchedProductItemTemplate = Handlebars.compile(searchedProductItemSource);
            var orderItemSource = document.getElementById("order-item-template").innerHTML;
            var orderItemTemplate = Handlebars.compile(orderItemSource);
            var orderTransactionSource = document.getElementById("order-transaction-template").innerHTML;
            var orderTransactionTemplate = Handlebars.compile(orderTransactionSource);
            var _selectedProducts = [];
            var _orderItems = [];
            @if (@$data && @count($data->items) > 0) 
                @foreach ($data->items as $item)
                    _orderItems.push({
                        master_sku: '{{ $item->master_sku }}',
                        name: '{{ $item->master->name }}',
                        variant_name: '{{ $item->master->variant_name }}',
                        amount: '{{ $item->discounted_price }}',
                        quantity: '{{ $item->quantity }}',
                    });
                @endforeach
            @endif
            @if (@$data && @count($data->order_transactions) > 0) 
                @foreach ($data->order_transactions as $trx)
                    var html = orderTransactionTemplate({
                        transaction_type: '{{ @$trx->transaction_type }}',
                        amount: '{{ @$trx->amount }}',
                        amountFormatted: currency.format('{{ @$trx->amount }}'),
                        isFee: {{ @$trx->amount < 0 ? true : "false" }},
                        comment: '{{ @$trx->comment }}',
                        withComment: {{ @$trx->comment != '' ? true : "false" }},
                    });
                    $('#otherTransactionsBody').append(html);
                @endforeach
            @endif
            function getProducts(keyword) {

                $('#searched-product-results').html('');
                $('#search-product-loader').removeClass('hidden');
                $.ajax({
                    type: 'get',
                    url: "{{ route('apiMasterProductsSearch') }}?keyword=" + keyword,
                    dataType: 'json',
                    success: function(response) {
                        $('#search-product-loader').addClass('hidden');
                        if (response.data && response.data.length > 0) {
                            const data = response.data;

                            data.forEach(product => {
                                if (product.variant_name != '' && product.variant_name != '-') {
                                    product.withVariantName = true;
                                } else {
                                    product.withVariantName = false;
                                }
                                var html = searchedProductItemTemplate(product);
                                $('#searched-product-results').append(html);
                            });

                            $('[name="master_skus[]"]').on('change', function() {
                                processSearchedFormData(data);
                            });
                        }
                    },
                    error: function(data, text, error) {
                        $('#search-product-loader').addClass('hidden');
                    }
                });
            }

            $('#searchProductName').on('click', function(e) {
                e.preventDefault();
                var keyword = $('[name="product_keyword"]').val();
                if (keyword.trim() != '') {
                    getProducts(keyword.trim());
                }
            });

            function processSearchedFormData(products) {
                var tmpSelectedProducts = [];
                const data = $('#searchedProductForm').serializeArray();
                data.forEach(d => {
                    tmpSelectedProducts.push(_.find(products, function(p) {
                        return p.master_sku == d.value;
                    }));
                });
                _selectedProducts = tmpSelectedProducts;
            }

            $('#addMasterProductsButton').on('click', function(e) {
                _selectedProducts.forEach(product => {
                    var orderItemExists = _.findIndex(_orderItems, function(item) {
                        return product.master_sku == item.master_sku;
                    });
                    if (orderItemExists < 0) {
                        product.amount = 0;
                        product.quantity = 1;
                        _orderItems.push(product);
                        var html = orderItemTemplate(product);
                        $('#orderItemsBody').append(html);
                    }
                });
                $('#searchMasterProductModal').modal('hide');
            });

            function calculateSubtotal() {
                var orderSubtotal = 0;
                $('#orderItemsBody .order--item').each(function() {
                    var price = $(this).find('.order--item-price').val();
                    var qty = $(this).find('.order--item-quantity').val();
                    orderSubtotal += price * qty;
                });
                $('#orderSubtotal').html(currency.format(parseFloat(orderSubtotal).toFixed(2)));

                return orderSubtotal;
            }
            
            $('#orderItemsBody').on('change', '.order--item-price', function() {
                calculateTotal();
            });
            $('#orderItemsBody').on('change', '.order--item-quantity', function() {
                calculateTotal();
            });
            $('#orderItemsBody').on('click', '.order--item-remove', function() {
                var msku = $(this).data('msku');
                _orderItems = _.without(_orderItems, _.findWhere(_orderItems, {
                    master_sku: msku
                }));
                $(this).closest('.order--item').remove();
                calculateTotal();
            });
            $('#otherTransactionsBody').on('click', '.order--transaction-remove', function() {
                $(this).closest('.order--transaction').remove();
                calculateTotal();
            });

            $('#addOrderTransationForm').on('submit', function(e) {
                e.preventDefault();
                var data = $(this).serializeArray();
                var trx = {transaction_type: '', amount: 0, amountFormatted: '', comment: '', withComment: false, isFee: false};
                if (data.length > 2) {
                    data.forEach(d => {
                        if (d.name == 'transaction_type') trx.transaction_type = d.value;
                        if (d.name == 'amount') {
                            if (d.value < 0) trx.isFee = true;
                            trx.amount = d.value;
                            trx.amountFormatted = currency.format(d.value);
                        }
                        if (d.name == 'comment' && d.value != '') {
                            trx.comment = d.value;
                            trx.withComment = true;
                        };
                    });
                    var html = orderTransactionTemplate(trx);
                    $('#otherTransactionsBody').append(html);
                    $('#addOrderTransationModal').modal('hide');
                    resetAddOrderTransationModal();
                    calculateTotal();
                }
            });

            function calculateTransactions() {
                var orderTransactions = 0;
                $('#otherTransactionsBody .order--transaction').each(function() {
                    var amount = parseFloat($(this).find('[name="transaction_amount[]"]').val());
                    orderTransactions += amount;
                });

                return orderTransactions;
            }

            function calculateTotal() {
                var subtotal = calculateSubtotal();
                var transactions = calculateTransactions();
                var total = parseFloat(subtotal + transactions);
                $('#orderTotalAmount').html(currency.format(total));
            }

            function resetAddOrderTransationModal() {
                $('#addOrderTransationModal').find('input').val('');
            }

            function initOrderitems() {
                if (_orderItems.length > 0) {
                    _orderItems.forEach(product => {
                        _orderItems.push(product);
                        var html = orderItemTemplate(product);
                        $('#orderItemsBody').append(html);
                    });
                    calculateTotal();
                }
            }
            initOrderitems();
        });
    </script>
@endsection
