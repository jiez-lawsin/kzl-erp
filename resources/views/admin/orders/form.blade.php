<div class="row">
    <div class="col-sm-6">
        <div class="caboodle-form-group margin-bottom-15">
            <label for="order_creation_date">Date Ordered</label>
            {!! Form::date('order_creation_date', Carbon::parse(@$data->order_creation_date)->format('Y-m-d'), ['class' => 'form-control', 'id' => 'order_creation_date', 'placeholder' => 'Date Ordered', 'required']) !!}
        </div>
    </div>
    <div class="col-sm-6">
      <div class="caboodle-form-group margin-bottom-15">
        <label for="sales_rep">Received By</label>
        {!! Form::text('sales_rep', null, ['class' => 'form-control', 'id' => 'sales_rep', 'placeholder' => 'Received By', 'required']) !!}
    </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="caboodle-form-group margin-bottom-15">
            <label for="channel">Channel</label>
            {!! Form::select('channel', ['Walk-In' => 'Walk-In', 'FB Marketplace' => 'FB Marketplace', 'Resell' => 'Resell', 'Wholesale' => 'Wholesale'], null, ['class' => 'form-control select2']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="caboodle-form-group margin-bottom-15">
            <label for="store_name">Store Name</label>
            {!! Form::text('store_name', 'KZL Marketplace', ['class' => 'form-control', 'id' => 'store_name', 'placeholder' => 'Store Name', 'required']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="caboodle-form-group margin-bottom-15">
            <label for="payment">Payment</label>
            {!! Form::select('payment', ['COD' => 'COD', 'GCASH' => 'GCASH', 'Bank Transfer' => 'Bank Transfer', 'Cash' => 'Cash'], null, ['class' => 'form-control select2']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="caboodle-form-group margin-bottom-15">
            <label for="status">Status</label>
            {!! Form::select('status', ['Draft' => 'Draft', 'Ready To Ship' => 'Ready To Ship', 'Shipping' => 'Shipping', 'Delivered' => 'Delivered', 'Cancelled' => 'Cancelled', 'Failed' => 'Failed', 'Returned' => 'Returned'], null, ['class' => 'form-control select2']) !!}
        </div>
    </div>
</div>
<div class="caboodle-form-group margin-bottom-15">
    <label for="buyer_name">Buyer Name</label>
    {!! Form::text('buyer_name', null, ['class' => 'form-control', 'id' => 'buyer_name', 'placeholder' => 'Buyer Name', 'required']) !!}
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="caboodle-form-group margin-bottom-15">
            <label for="recipient_name">Recipient Name</label>
            {!! Form::text('recipient_name', null, ['class' => 'form-control', 'id' => 'recipient_name', 'placeholder' => 'Recipient Name']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="caboodle-form-group margin-bottom-15">
            <label for="recipient_phone">Recipient Phone</label>
            {!! Form::text('recipient_phone', null, ['class' => 'form-control', 'id' => 'recipient_phone', 'placeholder' => 'Recipient Phone']) !!}
        </div>
    </div>
</div>
<div class="caboodle-form-group margin-bottom-15">
    <label for="recipient_address">Recipient Address</label>
    {!! Form::text('recipient_address', null, ['class' => 'form-control', 'id' => 'recipient_address', 'placeholder' => 'Recipient Address']) !!}
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="caboodle-form-group margin-bottom-15">
            <label for="courier">Courier</label>
            {!! Form::text('courier', null, ['class' => 'form-control', 'id' => 'courier', 'placeholder' => 'Courier']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="caboodle-form-group margin-bottom-15">
            <label for="tracking_number">Tracking Number</label>
            {!! Form::text('tracking_number', null, ['class' => 'form-control', 'id' => 'tracking_number', 'placeholder' => 'Tracking Number']) !!}
        </div>
    </div>
</div>
<div class="caboodle-form-group">
  <label for="remarks">Remarks/Comments</label>
  {!! Form::text('remarks', null, ['class' => 'form-control', 'id' => 'remarks', 'placeholder' => 'Remarks/Comments']) !!}
</div>

