<script id="searched-product-item-template" type="text/x-handlebars-template">
    <tr>
        <td>
            <div class="mdc-form-field">
                <div class="mdc-checkbox">
                    <input type="checkbox" class="mdc-checkbox__native-control" name="master_skus[]"
                        value="@{{ master_sku }}" />
                    <div class="mdc-checkbox__background">
                        <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                            <path class="mdc-checkbox__checkmark-path" fill="none"
                                stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                        </svg>
                        <div class="mdc-checkbox__mixedmark"></div>
                    </div>
                </div>
            </div>
        </td>
        <td>
            <div class="margin-bottom-10">
                <div class="color-secondary sub-text-2 no-margin margin-right-10">
                    @{{ name }}
                    @{{#if withVariantName }}
                    <br><span class="color-grey">@{{ variant_name }}<span>
                    @{{/if}}
                    <br><span class="color-grey">MSKU: @{{ master_sku }}</span>
                </div>
            </div>
        </td>
    </tr>
</script>

<script id="order-item-template" type="text/x-handlebars-template">
    <tr class="order--item">
        <td>
            <input type="hidden" name="master_sku[]" value="@{{ master_sku }}">
            <div class="margin-bottom-10">
                <div class="color-secondary sub-text-2 no-margin margin-right-10">
                    @{{ name }}
                    @{{#if withVariantName}}
                    <br><span class="color-grey">@{{ variant_name }}<span>
                    @{{/if}}
                    <br><span class="color-grey">MSKU: @{{ master_sku }}</span>
                </div>
            </div>
        </td>
        <td class="vertical-align-top">
            <div class="caboodle-form-group no-margin">
                <input type="decimal" class="form-control order--item-price" required value="@{{ amount }}" name="item_price[]">
            </div>
        </td>
        <td class="vertical-align-top">
            <div class="caboodle-form-group no-margin">
                <input type="number" class="form-control order--item-quantity" required value="@{{ quantity }}" name="item_quantity[]">
            </div>
        </td>
        <td class="vertical-align-top">
            <div data-msku="@{{ master_sku }}" class="order--item-remove cursor color-red" style="height: 40px; display: flex; align-items: center; font-size: 20px;"><i class="far fa-times"></i></div>
        </td>
    </tr>
</script>

<script id="order-transaction-template" type="text/x-handlebars-template">
    <tr class="order--transaction">
        <td>
            <input type="hidden" name="transaction_type[]" value="@{{ transaction_type }}">
            <input type="hidden" name="transaction_amount[]" value="@{{ amount }}">
            <input type="hidden" name="transaction_comment[]" value="@{{ comment }}">
            <div>
                <div class="color-secondary sub-text-2 no-margin margin-right-10">
                    @{{ transaction_type }}
                    @{{#if withComment}}
                    <br><span class="color-grey">@{{ comment }}<span>
                    @{{/if}}
                </div>
            </div>
        </td>
        <td style="width: 150px;" class="@{{#if isFee}} color-red @{{/if}} text-right">@{{ amountFormatted }}</td>
        <td style="width: 50px;">
            <div class="order--transaction-remove cursor color-red" style="height: 40px; display: flex; align-items: center; font-size: 20px;"><i class="far fa-times"></i></div>
        </td>
    </tr>
</script>
