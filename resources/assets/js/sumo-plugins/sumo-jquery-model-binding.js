// Biboy
// Model Binding (2 Way binding)
$(document).ready(function() {
	// init
	$(".form-trigger").trigger("change");
	$(".date-picker[bind-model], .time-picker[bind-model]").trigger("dp.change");
	initBindModel();
    
    // APPICABLE FOR CHECKBOX TRIGGERING
	$(".form-trigger").change(function() {
        formTriggerInit($(this));
    });
    
    $(document).on("input", '.form-trigger', function(e) {
        console.log('asdasd');
    });

    $('.form-trigger').each(function(e) {
        formTriggerInit($(this));
    });
    
    function formTriggerInit(_this) {
		var triggerValue = _this.val();
		var name = _this.attr("name");
        var target = $(".form-target[data-trigger=" + name + "]");
        if(_this.attr("type") == "checkbox") {
            if (_this.is(":checked")) {
                triggerValue = true;
            } else {
                triggerValue = false;
            }
        }
        console.log(triggerValue);
        
		$.each(target,function(index, val){
			if($(this).data("trigger-type") === undefined) {
				$(this).addClass("hidden");
				$("input",this).prop("disabled",true);
				$(".require-on-show",this).prop("required",false);
			} else {
				$(this).removeClass("hidden");
				$("input",this).prop("disabled",false);
				$(".require-on-show",this).prop("required",true);
			}

			if ($(this).data("trigger-value") == triggerValue) {
				if($(this).data("trigger-type") === undefined) {
					$(this).removeClass("hidden");
					$("input",this).prop("disabled",false);
					$(".require-on-show",this).prop("required",true);
				} else {
					$(this).addClass("hidden");
					$("input",this).prop("disabled",true);
					$(".require-on-show",this).prop("required",false);
				}
			}
		});
    };
	
	// FORM Trigger CHANGE
    $(document).on("change input", ".form-trigger", function() {
		var name = $(this).attr("name");
		var target = $(".form-target[data-trigger=" + name + "]");
		$(target).each(function(e) {
			$(this).find('input').trigger('change');
		});
    });

	//---- two way binding model change
	$(document).on("dp.change", ".date-picker[bind-model], .time-picker[bind-model]", function(){
		var _this = $(this);
		var type = _this.attr("type");
		var value = _this.val();
		var model = _this.attr("bind-model");
		var tagName = (_this.prop("tagName")).toLowerCase();

		eval(model + " = " + '"' + value + '"');

        $("[bind-data='" + model + "']").html(value);
        $("[bind-data='" + model + "']").val(value);
        $("[bind-data='" + model + "']").trigger("change");
		checkBindIf();
	});

	//bind-model on input/change event
	$(document).on("change input", "[bind-model]", function() {
		var _this = $(this);
		var type = _this.attr("type");
		var model = _this.attr("bind-model");
		var value = _this.val() ? _this.val() : '';
		var tagName = (_this.prop("tagName")).toLowerCase();
		var funct = _this.attr("bind-change");
		if (type == "text" ||
		    type == "number" || 
		    tagName == "textarea" ||
		    tagName == "select" ||
		    (type == "radio" && _this.is(":checked")) ||
		    type == "checkbox" && _this.is(":checked")) {

			if(value !== eval(model)) {
        		eval(model + " = " + '"' + value + '"');

		        if(type !== "radio" && type !== "checkbox")
          			$("[bind-model='" + model + "']").val(value);

        		$("[bind-data='" + model + "']").html(value);
        		$("[bind-data='" + model + "']").val(value);
        		$("[bind-data='" + model + "']").trigger("change");
	        	
	        	checkBindIf();

	        	if(funct !== undefined)
	          		eval(funct);
	      	}
	    } else if(type == "radio" && !_this.is(":checked") ||
		    type == "checkbox" && !_this.is(":checked")){
	      	eval(model + " = " + '""');

    		$("[bind-data='" + model + "']").html(value);
    		$("[bind-data='" + model + "']").val(value);
            $("[bind-data='" + model + "']").trigger("change");
            
        	checkBindIf();

        	if(funct !== undefined)
          		eval(funct);
	    }
	});
});

//--- initialize value of bind models
function initBindModel() {
	$.each($("[bind-model]"), function(i, v) {
	    var _this = $(this);
	    var type = _this.attr("type");
	    var model = _this.attr("bind-model");
	    var value = _this.val() ? _this.val() : '';
	    var tagName = (_this.prop("tagName")).toLowerCase();
	    var funct = _this.attr("bind-change");

	    if(type == "text" ||
			type == "number" || 
			tagName == "textarea" ||
			tagName == "select" ||
			(type == "radio" && _this.is(":checked")) ||
			type == "checkbox" && _this.is(":checked")) {

	      	eval(model + " = " + '"' + value + '"');
	      
	      	if(type !== "radio" && type !== "checkbox")
	        	$("[bind-model='" + model + "']").val(value);

	      	$("[bind-data='" + model + "']").html(value);
	      	$("[bind-data='" + model + "']").val(value);
	      	$("[bind-data='" + model + "']").trigger("change");
	      	checkBindIf();
	    } else if(type == "radio" && !_this.is(":checked") ||
		    type == "checkbox" && !_this.is(":checked")) {
	    }
  	});
};

function checkBindIf() {
  	$.each ($("[bind-if]"), function () {
	    if (eval($(this).attr("bind-if"))) {
	      	$(this).show();
	    } else {
	      	$(this).hide();
	    }
  });
};