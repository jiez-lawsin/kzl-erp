 /*!
  * Sumo Product Browse Generator for Ecommerce Base
  * http://sumofy.me/
  *
  * Copyright Think Sumo Creative Media Inc.
  * Developed by Jiez Lawsin
  * Released under the MIT license.
  * http://sumofy.me/
  *
  * NOTE: Not compatible with multiple product browsing
  */

(function($) {
    var modal;
    var products = [];
    var selected;
    var tmpSelected;
    var keyword = '';
    
	$.fn.sumoproductbrowse = function(options) {
		if(typeof _.each === 'undefined') {
			throw new Error('sumo-product-browse requires Underscore.js to be loaded first');
        }

        if(typeof Handlebars === 'undefined') {
			throw new Error('sumo-product-browse requires Handlebars.js to be loaded first');
        }

        modal = $('#productBrowseModal');

        if (modal.length == 0) {
            throw new Error('sumo-product-browse requires the product_browse.blade.php to be included first');
        }

		var defaults = {
			    title: 'Add products', // (STRING) override modal header title
			    filters: false, // (BOOL) Enable/Disable product filtering, will give different templates
			    searchPlaceholder: 'Search products', // (STRING) ovveride search input placeholder
                multipleSelect: true, // (BOOL) Enable/Disable multiple select of product; if false, will only show parent products
                addButtonText: 'Add', // (STRING) ovveride apply button text
            };

		//Extend those options
        var options = $.extend(defaults, options);

        if (options.multipleSelect) selected = [];

		$(this).each(function(e) {
            var elem = $(this); // element
            initializeEvents(elem, options);

            $('body').on('click', '#productBrowseModal .add-btn', function() {
                modal.modal('hide');
                selected = tmpSelected;
                if (options.onAdd !== undefined) {
                    options.onAdd(elem, selected);
                }
            });
            
            $('body').on('shown.bs.modal', '#productBrowseModal', function () {
                if (options.multipleSelect) {
                    tmpSelected = selected.slice();
                } else {
                    tmpSelected = $.extend(true, {}, selected);
                }
            });
        });

        return {
            updateSelected : function(data) {
                selected = data;
            }
        };
    }

    function initializeEvents(elem, options) {
        var formGroupTemplate = Handlebars.compile($("#productBrowseFormGroupTemplate").html());
        var modalTemplate = Handlebars.compile($("#productBrowseModalTemplate").html());
        elem.html(formGroupTemplate());
        modal.html(modalTemplate(options));

        bindEvents(elem, options);
    };

    function bindEvents(elem, options) {
        elem.find('input').on('input', function(e) {
            keyword = elem.find('input').val();
            showBrowseModal(options);
        });

        elem.find('button').on('click', function(e) {
            keyword = elem.find('input').val();
            showBrowseModal(options);
        });

        modal.find('.modal-filters').find('input').on('input', function(e) {
            keyword = $(this).val();
            elem.find('input').val(keyword);
            showBrowseModal(options);
        });
    };

    function showBrowseModal(options) {
        modal.modal('show');
        var tableLoader = Handlebars.compile($("#tableLoader").html());
        var noResultsFoundTable = Handlebars.compile($("#noResultsFoundTable").html());
        var modalBodyTemplate = Handlebars.compile($("#productBrowseListTemplate").html());

        modal.find('.modal-body').html(tableLoader());
        modal.find('.modal-filters').find('input').val(keyword);

        var productListUrl = modal.find('.modal-body').data('list');
        var URL = modal.find('.modal-body').data('default');
        var link = modal.find('.modal-body').data('link');
        var linkVariant = modal.find('.modal-body').data('variant');

        $.ajax({
            type: "GET",
            url: productListUrl,
            data: {
                title: keyword
            },
            dataType: "JSON",
            success: function(res){
                if (res.success) {
                    if (res.data.length) {
                        if (!options.multipleSelect && !tmpSelected) tmpSelected = res.data[0];
                        _.map(res.data, function(prod) {
                            prod.image = (prod.asset != null) ? 'background-image: url(' + URL + '/' + prod.asset.path + ')' : '';
                            prod.link = link.replace('0', prod.id);
                            if (prod.variants.length) {
                                prod.noVariants = false;
                                _.map(prod.variants, function(variant) {
                                    variant.price = numberWithCommas(variant.price);
                                    variant.link = linkVariant.replace('#prod', prod.id).replace('#variant', variant.id);
                                });
                            } else {
                                prod.noVariants = true;
                            }

                            if (options.multipleSelect) {
                                prod.singleSelect = false;
                            } else {
                                prod.variants = [];
                                prod.noVariants = true;
                                prod.singleSelect = true;
                                if (tmpSelected.id == prod.id) {
                                    prod.selected = true;
                                } else {
                                    prod.selected = false;
                                }
                            }
                            prod.price = numberWithCommas(prod.price);
                        });
                        products = res.data;
                        // console.log(products);
                        modal.find('.modal-body').html(modalBodyTemplate(products));
                        
                        if (options.multipleSelect) {
                            if (tmpSelected.length) {
                                _.each(tmpSelected, function(product) {
                                    generateCheckbox(0, product.id, true);
                                });
                            }
                        }
                    } else {
                        modal.find('.modal-body').html(noResultsFoundTable());
                    }
                } else {
                    modal.find('.modal-body').html(noResultsFoundTable());
                }
            },
            error: function(xhr){
                console.log(xhr);
            }
        });
    };

    $('body').on('click', '#productBrowseModal .browse-list .entry-list', function(e) {
        if (!$(this).hasClass('disabled') && !$(this).hasClass('single-select')) {
            var checkbox = $(this).find('[name="product_ids[]"]');
            var parent = $(this).data('parent');
            var id = parseInt($(this).data('id'));

            if (parent == 0) {
                var parentProduct = _.where(products, {id: id})[0];
                var tmpProd = $.extend({}, parentProduct);

                var selectedProduct = _.where(tmpSelected, {id: id});
                if (selectedProduct.length) {
                    if (tmpProd.variants.length == selectedProduct[0].variants.length || selectedProduct[0].variants.length == 0) {
                        tmpSelected = _.reject(selected, {id: id});
                    } else {
                        _.map(tmpSelected, function(product) {
                            if (product.id == id) {
                                product.variants = tmpProd.variants;
                            };
                        });
                    }
                } else {
                    tmpSelected.push(tmpProd);
                }
            } else {
                var parentProduct = _.where(products, {id: parent})[0];
                var variant = _.where(parentProduct.variants, {id: id})[0];

                if (_.where(tmpSelected, {id: parent}).length) {
                    _.map(tmpSelected, function(product) {
                        if (parent == product.id) {
                            if (_.where(product.variants, {id: id}).length) {
                                product.variants = _.reject(product.variants, {id: id});
                                if (product.variants.length == 0) {
                                    tmpSelected = _.reject(tmpSelected, {id: parent});
                                }
                            } else {
                                product.variants.push(variant);
                            }
                        }
                    });
                } else {
                    var tmpProd = $.extend({}, parentProduct);
                    tmpProd.variants = [variant];
                    tmpSelected.push(tmpProd);
                }
            }

            generateCheckbox(id, parent, false);
        } else {
            if ($(this).hasClass('single-select')) {
                $(this).find('[name="product_ids"]').prop("checked", true);
                tmpSelected = _.where(products, {id: $(this).data('id')})[0];
            }
        }
    });

    // id : variant/product id
    // parent : id of parent
    // disabled : if true, will disable the item
    function generateCheckbox(id, parent, disabled) {
        var parentID = (parent == 0) ? id : parent;
        var selectedProduct = _.where(tmpSelected, {id: parentID});
        var origProduct = _.where(products, {id : parentID});
        
        if (selectedProduct.length) {
            if (origProduct[0].variants.length == selectedProduct[0].variants.length) {
                $('#productBrowseModal .browse-list .entry-list[data-id="' + parentID + '"]')
                    .addClass('selected')
                    .find('.select-box-item')
                    .find('i')
                    .removeClass('fa-square-o')
                    .removeClass('fa-minus-square-o')
                    .addClass('fa-check-square-o');
                if (disabled) {
                    $('#productBrowseModal .browse-list .entry-list[data-id="' + parentID + '"]').addClass('disabled').removeClass('selected');
                }
            } else {
                $('#productBrowseModal .browse-list .entry-list[data-id="' + parentID + '"]')
                    .addClass('selected')
                    .find('.select-box-item')
                    .find('i')
                    .removeClass('fa-check-square-o')
                    .removeClass('fa-square-o')
                    .addClass('fa-minus-square-o');
            }

            if ($('#productBrowseModal .browse-list .entry-list[data-id="' + parentID + '"]').hasClass('disabled')) {
                $('#productBrowseModal .browse-list .entry-list[data-id="' + parentID + '"]').removeClass('selected');
            }

            _.each(origProduct[0].variants, function(variant) {
                if (_.where(selectedProduct[0].variants, {id: variant.id}).length) {
                    $('#productBrowseModal .browse-list .entry-list[data-id="' + variant.id + '"]')
                        .addClass('selected')
                        .find('.select-box-item')
                        .find('i')
                        .removeClass('fa-square-o')
                        .removeClass('fa-minus-square-o')
                        .addClass('fa-check-square-o');
                    if (disabled) {
                        $('#productBrowseModal .browse-list .entry-list[data-id="' + variant.id + '"]').addClass('disabled').removeClass('selected');
                    }

                    if ($('#productBrowseModal .browse-list .entry-list[data-id="' + variant.id + '"]').hasClass('disabled')) {
                        $('#productBrowseModal .browse-list .entry-list[data-id="' + variant.id + '"]').removeClass('selected');
                    }
                } else {
                    $('#productBrowseModal .browse-list .entry-list[data-id="' + variant.id + '"]')
                        .removeClass('selected')
                        .find('.select-box-item')
                        .find('i')
                        .removeClass('fa-check-square-o')
                        .removeClass('fa-minus-square-o')
                        .addClass('fa-square-o');
                }
            });
        } else {
            $('#productBrowseModal .browse-list .entry-list[data-id="' + parentID + '"]')
                .removeClass('selected')
                .find('.select-box-item')
                .find('i')
                .removeClass('fa-check-square-o')
                .removeClass('fa-minus-square-o')
                .addClass('fa-square-o');
            $('#productBrowseModal .browse-list .entry-list[data-parent="' + parentID + '"]')
                .removeClass('selected')
                .find('.select-box-item')
                .find('i')
                .removeClass('fa-check-square-o')
                .removeClass('fa-minus-square-o')
                .addClass('fa-square-o');
        }
    };
})(jQuery);

// (Reusable global function) Suggested result template for the selected products
// Call function with params:
//      target : the element targeted by the plugin
//      products : list of products with variants to display
function generateProductBrowseResultItemsTemplate(target, products) {
    var browseResultTemplate = Handlebars.compile($("#productBrowseResultItems").html());
    target.find('.browse-selected').html(browseResultTemplate(products));
};

// (Reusable global function) Suggested function for the removing selected variant/product from the selected items
// Call function with params:
//      id : id of the variant/product
//      parent : parent id of the variant; 0 if product
//      products : list of products with variants
function removeItemFromProductBrowseResult(id, parent, products) {
    if (parent == 0) {
        products =  _.reject(products, {id: id});
    } else {
        var tmpProds = products.slice();
        _.map(products, function(product) {
            if (product.id == parent) {
                product.variants = _.reject(product.variants, {id: id});
                if (product.variants.length == 0) {
                    tmpProds = _.reject(tmpProds, {id: product.id});
                } else {
                    var index = _.findIndex(tmpProds, {id : product.id});
                    tmpProds[index].variants = product.variants;
                }
            }
        });
        products = tmpProds;
    }
    return products;
};
