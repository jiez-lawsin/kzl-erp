<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('api/samples/find', array('as'=>'apiSamplesFind','uses'=>'SampleController@find', 'middleware'=>['api','cors']));
// Route::get('api/samples/get', array('as'=>'apiSamplesGet','uses'=>'SampleController@get', 'middleware'=>['api','cors']));
// Route::post('api/samples/store', array('as'=>'apiSamplesStore','uses'=>'SampleController@store', 'middleware'=>['api','cors']));
// Route::post('api/samples/update', array('as'=>'apiSamplesUpdate','uses'=>'SampleController@update', 'middleware'=>['api','cors']));
// Route::post('api/samples/destroy', array('as'=>'apiSamplesDestroy','uses'=>'SampleController@destroy', 'middleware'=>['api','cors']));


Route::get('api/master_products/search', array('as'=>'apiMasterProductsSearch','uses'=>'MasterProductController@search', 'middleware'=>'cors'));
