<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/test', array('as'=>'test','uses'=>'HomeController@amp'));
// Route::get('/', array('as'=>'homePage','uses'=>'HomeController@index'));
// Route::get('page/{slug}/preview', array('as'=>'pagePreview','uses'=>'HomeController@preview'));
// Route::get('/seo', 'HomeController@seo');