<?php

/*
|--------------------------------------------------------------------------
| BASE Routes
|--------------------------------------------------------------------------
|
| (do not modify here. if you have updates to base, update laravel-base repo instead)
|
*/

Route::auth();

Route::get('autogen', array('as'=>'autogen','uses'=>'AutogenController@index'));

Route::get('/', array('as'=>'adminLogin','uses'=>'Admin\AuthController@login'));
Route::post('/', array('as'=>'adminAuthenticate','uses'=>'Admin\AuthController@authenticate'));
Route::get('logout', array('as'=>'adminLogout','uses'=>'Admin\AuthController@logout'));

Route::get('dashboard', array('as'=>'adminDashboard','uses'=>'Admin\DashboardController@index'));
Route::get('dashboard/products', array('as'=>'adminDashboardProducts','uses'=>'Admin\DashboardController@productSales'));

Route::get('samples', array('as'=>'adminSamples','uses'=>'Admin\SampleController@index'));
Route::get('samples/datatable', array('as'=>'adminSamplesDatatable','uses'=>'Admin\SampleController@datatable'));
Route::get('samples/create', array('as'=>'adminSamplesCreate','uses'=>'Admin\SampleController@create'));
Route::post('samples/', array('as'=>'adminSamplesStore','uses'=>'Admin\SampleController@store'));
Route::get('samples/{id}/show', array('as'=>'adminSamplesShow','uses'=>'Admin\SampleController@show'));
Route::get('samples/{id}/view', array('as'=>'adminSamplesView','uses'=>'Admin\SampleController@view'));
Route::get('samples/{id}/edit', array('as'=>'adminSamplesEdit','uses'=>'Admin\SampleController@edit'));
Route::patch('samples/{id}', array('as'=>'adminSamplesUpdate','uses'=>'Admin\SampleController@update'));
Route::post('samples/seo', array('as'=>'adminSamplesSeo','uses'=>'Admin\SampleController@seo'));
Route::delete('samples/destroy', array('as'=>'adminSamplesDestroy','uses'=>'Admin\SampleController@destroy'));
Route::get('samples/crop/url', array('as'=>'adminSamplesCropUrl','uses'=>'Admin\SampleController@crop_url'));
Route::get('samples/{id}/crop/{column}/{asset_id}', array('as'=>'adminSamplesCropForm','uses'=>'Admin\SampleController@crop_form'));
Route::patch('samples/{id}/crop', array('as'=>'adminSamplesCrop','uses'=>'Admin\SampleController@crop'));

Route::get('users', array('as'=>'adminUsers','uses'=>'Admin\UserController@index'));
Route::get('users/create', array('as'=>'adminUsersCreate','uses'=>'Admin\UserController@create'));
Route::post('users/', array('as'=>'adminUsersStore','uses'=>'Admin\UserController@store'));
Route::get('users/{id}/change_password', array('as'=>'adminUsersChangePassword','uses'=>'Admin\UserController@change_password'));
Route::patch('users/{id}/change_password', array('as'=>'adminUsersUpdatePassword','uses'=>'Admin\UserController@user_password_update'));
Route::get('users/{id}/show', array('as'=>'adminUsersShow','uses'=>'Admin\UserController@show'));
Route::get('users/{id}/edit', array('as'=>'adminUsersEdit','uses'=>'Admin\UserController@edit'));
Route::patch('users/{id}', array('as'=>'adminUsersUpdate','uses'=>'Admin\UserController@update'));
Route::delete('users/destroy', array('as'=>'adminUsersDestroy','uses'=>'Admin\UserController@destroy'));

Route::get('profile', array('as'=>'adminProfile','uses'=>'Admin\ProfileController@index'));
Route::get('profile/edit', array('as'=>'adminProfileEdit','uses'=>'Admin\ProfileController@edit'));
Route::patch('profile/edit', array('as'=>'adminProfileUpdate','uses'=>'Admin\ProfileController@update'));
Route::get('profile/change_password', array('as'=>'adminProfilePasswordEdit','uses'=>'Admin\ProfileController@password_edit'));
Route::patch('profile/change_password', array('as'=>'adminProfilePasswordUpdate','uses'=>'Admin\ProfileController@password_update'));

Route::get('options', array('as'=>'adminOptions','uses'=>'Admin\OptionController@index'));
Route::get('options/create', array('as'=>'adminOptionsCreate','uses'=>'Admin\OptionController@create'));
Route::post('options/', array('as'=>'adminOptionsStore','uses'=>'Admin\OptionController@store'));
Route::get('options/{id}/show', array('as'=>'adminOptionsShow','uses'=>'Admin\OptionController@show'));
Route::get('options/{id}/view', array('as'=>'adminOptionsView','uses'=>'Admin\OptionController@view'));
Route::get('options/{id}/edit', array('as'=>'adminOptionsEdit','uses'=>'Admin\OptionController@edit'));
Route::patch('options/{id}', array('as'=>'adminOptionsUpdate','uses'=>'Admin\OptionController@update'));
Route::delete('options/destroy', array('as'=>'adminOptionsDestroy','uses'=>'Admin\OptionController@destroy'));

Route::post('assets/upload', array('as'=>'adminAssetsUpload','uses'=>'Admin\AssetController@upload'));
Route::post('assets/redactor', array('as'=>'adminAssetsRedactor','uses'=>'Admin\AssetController@redactor'));
Route::get('assets/all', array('as'=>'adminAssetsAll','uses'=>'Admin\AssetController@all'));
Route::get('assets/get', array('as'=>'adminAssetsGet','uses'=>'Admin\AssetController@get'));
Route::get('assets/download', array('as'=>'adminAssetsDownload','uses'=>'Admin\AssetController@download'));
Route::post('assets/update', array('as'=>'adminAssetsUpdate','uses'=>'Admin\AssetController@update'));
Route::post('assets/destroy', array('as'=>'adminAssetsDestroy','uses'=>'Admin\AssetController@destroy'));
Route::get('assets/get_asset_tags', array('as'=>'adminGetAssetTags','uses'=>'Admin\AssetController@getAssetTags'));

Route::get('tests', array('as'=>'adminTests','uses'=>'Admin\TestController@index'));
Route::get('tests/create', array('as'=>'adminTestsCreate','uses'=>'Admin\TestController@create'));
Route::post('tests/', array('as'=>'adminTestsStore','uses'=>'Admin\TestController@store'));
Route::get('tests/{id}/show', array('as'=>'adminTestsShow','uses'=>'Admin\TestController@show'));
Route::get('tests/{id}/view', array('as'=>'adminTestsView','uses'=>'Admin\TestController@view'));
Route::get('tests/{id}/edit', array('as'=>'adminTestsEdit','uses'=>'Admin\TestController@edit'));
Route::patch('tests/{id}', array('as'=>'adminTestsUpdate','uses'=>'Admin\TestController@update'));
Route::post('tests/seo', array('as'=>'adminTestsSeo','uses'=>'Admin\TestController@seo'));
Route::delete('tests/destroy', array('as'=>'adminTestsDestroy','uses'=>'Admin\TestController@destroy'));
Route::get('tests/crop/url', array('as'=>'adminTestsCropUrl','uses'=>'Admin\TestController@crop_url'));
Route::get('tests/{id}/crop/{column}/{asset_id}', array('as'=>'adminTestsCropForm','uses'=>'Admin\TestController@crop_form'));
Route::patch('tests/{id}/crop', array('as'=>'adminTestsCrop','uses'=>'Admin\TestController@crop'));
Route::get('tests/order', array('as'=>'adminTestsOrder','uses'=>'Admin\TestController@order'));

Route::get('long_names', array('as'=>'adminLongNames','uses'=>'Admin\LongNameController@index'));
Route::get('long_names/create', array('as'=>'adminLongNamesCreate','uses'=>'Admin\LongNameController@create'));
Route::post('long_names/', array('as'=>'adminLongNamesStore','uses'=>'Admin\LongNameController@store'));
Route::get('long_names/{id}/show', array('as'=>'adminLongNamesShow','uses'=>'Admin\LongNameController@show'));
Route::get('long_names/{id}/edit', array('as'=>'adminLongNamesEdit','uses'=>'Admin\LongNameController@edit'));
Route::patch('long_names/{id}', array('as'=>'adminLongNamesUpdate','uses'=>'Admin\LongNameController@update'));
Route::delete('long_names/destroy', array('as'=>'adminLongNamesDestroy','uses'=>'Admin\LongNameController@destroy'));

Route::get('page_categories', array('as'=>'adminPageCategories','uses'=>'Admin\PageCategoryController@index'));
Route::get('page_categories/create', array('as'=>'adminPageCategoriesCreate','uses'=>'Admin\PageCategoryController@create'));
Route::post('page_categories/', array('as'=>'adminPageCategoriesStore','uses'=>'Admin\PageCategoryController@store'));
Route::get('page_categories/{id}/show', array('as'=>'adminPageCategoriesShow','uses'=>'Admin\PageCategoryController@show'));
Route::get('page_categories/{id}/view', array('as'=>'adminPageCategoriesView','uses'=>'Admin\PageCategoryController@view'));
Route::get('page_categories/{id}/edit', array('as'=>'adminPageCategoriesEdit','uses'=>'Admin\PageCategoryController@edit'));
Route::patch('page_categories/{id}', array('as'=>'adminPageCategoriesUpdate','uses'=>'Admin\PageCategoryController@update'));
Route::post('page_categories/seo', array('as'=>'adminPageCategoriesSeo','uses'=>'Admin\PageCategoryController@seo'));
Route::delete('page_categories/destroy', array('as'=>'adminPageCategoriesDestroy','uses'=>'Admin\PageCategoryController@destroy'));
Route::get('page_categories/pages/', array('as' => 'adminPageByCategory', 'uses' => 'Admin\PageCategoryController@getPageByCategory'));

Route::get('pages', array('as'=>'adminPages','uses'=>'Admin\PageController@index'));
Route::get('pages/order', array('as'=>'adminPagesOrder','uses'=>'Admin\PageController@order'));
Route::get('pages/create', array('as'=>'adminPagesCreate','uses'=>'Admin\PageController@create'));
Route::post('pages/', array('as'=>'adminPagesStore','uses'=>'Admin\PageController@store'));
Route::get('pages/{id}/show', array('as'=>'adminPagesShow','uses'=>'Admin\PageController@show'));
Route::get('pages/{id}/view', array('as'=>'adminPagesView','uses'=>'Admin\PageController@view'));
Route::get('dev/pages/{id}/edit', array('as'=>'adminPagesEdit','uses'=>'Admin\PageController@edit'));
Route::patch('pages/{id}', array('as'=>'adminPagesUpdate','uses'=>'Admin\PageController@update'));
Route::post('pages/seo', array('as'=>'adminPagesSeo','uses'=>'Admin\PageController@seo'));
Route::delete('pages/destroy', array('as'=>'adminPagesDestroy','uses'=>'Admin\PageController@destroy'));

Route::get('banners', array('as'=>'adminBanners','uses'=>'Admin\BannerController@index'));
Route::get('banners/create', array('as'=>'adminBannersCreate','uses'=>'Admin\BannerController@create'));
Route::post('banners/', array('as'=>'adminBannersStore','uses'=>'Admin\BannerController@store'));
Route::get('banners/{id}/show', array('as'=>'adminBannersShow','uses'=>'Admin\BannerController@show'));
Route::get('banners/{id}/view', array('as'=>'adminBannersView','uses'=>'Admin\BannerController@view'));
Route::get('banners/{id}/edit', array('as'=>'adminBannersEdit','uses'=>'Admin\BannerController@edit'));
Route::patch('banners/{id}', array('as'=>'adminBannersUpdate','uses'=>'Admin\BannerController@update'));
Route::get('banners/order', array('as'=>'adminBannersOrder','uses'=>'Admin\BannerController@order'));
Route::post('banners/seo', array('as'=>'adminBannersSeo','uses'=>'Admin\BannerController@seo'));
Route::delete('banners/destroy', array('as'=>'adminBannersDestroy','uses'=>'Admin\BannerController@destroy'));
Route::get('banners/crop/url', array('as'=>'adminBannersCropUrl','uses'=>'Admin\BannerController@crop_url'));
Route::get('banners/{id}/crop/{column}/{asset_id}', array('as'=>'adminBannersCropForm','uses'=>'Admin\BannerController@crop_form'));
Route::patch('banners/{id}/crop', array('as'=>'adminBannersCrop','uses'=>'Admin\BannerController@crop'));

Route::post('user_permissions/', array('as'=>'adminUserPermissionsStore','uses'=>'Admin\UserPermissionController@store'));
Route::get('user_permissions/{id}/show', array('as'=>'adminUserPermissionsShow','uses'=>'Admin\UserPermissionController@show'));
Route::get('user_permissions/{id}/view', array('as'=>'adminUserPermissionsView','uses'=>'Admin\UserPermissionController@view'));
Route::get('user_permissions/{id}/edit', array('as'=>'adminUserPermissionsEdit','uses'=>'Admin\UserPermissionController@edit'));
Route::patch('user_permissions/{id}', array('as'=>'adminUserPermissionsUpdate','uses'=>'Admin\UserPermissionController@update'));
Route::post('user_permissions/seo', array('as'=>'adminUserPermissionsSeo','uses'=>'Admin\UserPermissionController@seo'));
Route::delete('user_permissions/destroy', array('as'=>'adminUserPermissionsDestroy','uses'=>'Admin\UserPermissionController@destroy'));
Route::get('user_permissions/{id}/permissions', array('as'=>'adminUserPermissions','uses'=>'Admin\UserPermissionController@index'));
Route::get('user_permissions/{id}/create', array('as'=>'adminUserPermissionsCreate','uses'=>'Admin\UserPermissionController@create'));
Route::get('user_permissions/order', array('as'=>'adminUserPermissionsOrder','uses'=>'Admin\UserPermissionController@order'));

Route::get('user_roles', array('as'=>'adminUserRoles','uses'=>'Admin\UserRoleController@index'));
Route::get('user_roles/create', array('as'=>'adminUserRolesCreate','uses'=>'Admin\UserRoleController@create'));
Route::post('user_roles/', array('as'=>'adminUserRolesStore','uses'=>'Admin\UserRoleController@store'));
Route::get('user_roles/{id}/show', array('as'=>'adminUserRolesShow','uses'=>'Admin\UserRoleController@show'));
Route::get('user_roles/{id}/view', array('as'=>'adminUserRolesView','uses'=>'Admin\UserRoleController@view'));
Route::get('user_roles/{id}/edit', array('as'=>'adminUserRolesEdit','uses'=>'Admin\UserRoleController@edit'));
Route::patch('user_roles/{id}', array('as'=>'adminUserRolesUpdate','uses'=>'Admin\UserRoleController@update'));
Route::post('user_roles/seo', array('as'=>'adminUserRolesSeo','uses'=>'Admin\UserRoleController@seo'));
Route::delete('user_roles/destroy', array('as'=>'adminUserRolesDestroy','uses'=>'Admin\UserRoleController@destroy'));

Route::get('articles', array('as'=>'adminArticles','uses'=>'Admin\ArticleController@index'));
Route::get('articles/create', array('as'=>'adminArticlesCreate','uses'=>'Admin\ArticleController@create'));
Route::post('articles/', array('as'=>'adminArticlesStore','uses'=>'Admin\ArticleController@store'));
Route::get('articles/{id}/show', array('as'=>'adminArticlesShow','uses'=>'Admin\ArticleController@show'));
Route::get('articles/{id}/view', array('as'=>'adminArticlesView','uses'=>'Admin\ArticleController@view'));
Route::get('articles/{id}/edit', array('as'=>'adminArticlesEdit','uses'=>'Admin\ArticleController@edit'));
Route::patch('articles/{id}', array('as'=>'adminArticlesUpdate','uses'=>'Admin\ArticleController@update'));
Route::post('articles/seo', array('as'=>'adminArticlesSeo','uses'=>'Admin\ArticleController@seo'));
Route::delete('articles/destroy', array('as'=>'adminArticlesDestroy','uses'=>'Admin\ArticleController@destroy'));
Route::get('articles/crop/url', array('as'=>'adminArticlesCropUrl','uses'=>'Admin\ArticleController@crop_url'));
Route::get('articles/{id}/crop/{column}/{asset_id}', array('as'=>'adminArticlesCropForm','uses'=>'Admin\ArticleController@crop_form'));
Route::patch('articles/{id}/crop', array('as'=>'adminArticlesCrop','uses'=>'Admin\ArticleController@crop'));

Route::get('emails', array('as'=>'adminEmails','uses'=>'Admin\EmailController@index'));
Route::get('emails/create', array('as'=>'adminEmailsCreate','uses'=>'Admin\EmailController@create'));
Route::post('emails/', array('as'=>'adminEmailsStore','uses'=>'Admin\EmailController@store'));
Route::get('emails/{id}/show', array('as'=>'adminEmailsShow','uses'=>'Admin\EmailController@show'));
Route::get('emails/{id}/view', array('as'=>'adminEmailsView','uses'=>'Admin\EmailController@view'));
Route::get('emails/{id}/edit', array('as'=>'adminEmailsEdit','uses'=>'Admin\EmailController@edit'));
Route::patch('emails/{id}', array('as'=>'adminEmailsUpdate','uses'=>'Admin\EmailController@update'));
Route::post('emails/seo', array('as'=>'adminEmailsSeo','uses'=>'Admin\EmailController@seo'));
Route::delete('emails/destroy', array('as'=>'adminEmailsDestroy','uses'=>'Admin\EmailController@destroy'));

Route::get('activities', array('as'=>'adminActivities','uses'=>'Admin\ActivityController@index'));
Route::get('activities/{id}/show', array('as'=>'adminActivitiesShow','uses'=>'Admin\ActivityController@show'));
Route::get('activities/{id}/view', array('as'=>'adminActivitiesView','uses'=>'Admin\ActivityController@view'));

Route::get('video_options', array('as'=>'adminVideoOptions','uses'=>'Admin\VideoOptionController@index'));
Route::post('video_options/create', array('as'=>'adminVideoOptionsCreate','uses'=>'Admin\VideoOptionController@create'));
Route::post('video_options/delete', array('as'=>'adminVideoOptionsDeleteVideo','uses'=>'Admin\VideoOptionController@deleteVideoBanner'));
Route::post('video_options/', array('as'=>'adminVideoOptionsStore','uses'=>'Admin\VideoOptionController@store'));
Route::get('getUploadedVideos/', array('as'=>'getUploadedVideos','uses'=>'Admin\VideoOptionController@getUploadedVideos'));
Route::get('video_options/{id}/show', array('as'=>'adminVideoOptionsShow','uses'=>'Admin\VideoOptionController@show'));
Route::get('video_options/{id}/view', array('as'=>'adminVideoOptionsView','uses'=>'Admin\VideoOptionController@view'));
Route::get('video_options/{id}/edit', array('as'=>'adminVideoOptionsEdit','uses'=>'Admin\VideoOptionController@edit'));
Route::patch('video_options/{id}', array('as'=>'adminVideoOptionsUpdate','uses'=>'Admin\VideoOptionController@update'));
Route::post('video_options/seo', array('as'=>'adminVideoOptionsSeo','uses'=>'Admin\VideoOptionController@seo'));
Route::delete('video_options/destroy', array('as'=>'adminVideoOptionsDestroy','uses'=>'Admin\VideoOptionController@destroy'));

Route::get('video-options/youtube', array('as'=>'adminVideosYoutube','uses'=>'Admin\VideoOptionController@youtube'));
Route::get('video-options/vimeo', array('as'=>'adminVideosVimeo','uses'=>'Admin\VideoOptionController@vimeo'));
Route::get('video-options/library', array('as'=>'adminVideosLibrary','uses'=>'Admin\VideoOptionController@library'));

Route::get('navbar_links', array('as'=>'adminNavbarLinks','uses'=>'Admin\NavbarLinkController@index'));
Route::post('navbar_links/', array('as'=>'adminNavbarLinksStore','uses'=>'Admin\NavbarLinkController@store'));
Route::get('navbar_links/{id}/show', array('as'=>'adminNavbarLinksShow','uses'=>'Admin\NavbarLinkController@show'));
Route::get('navbar_links/{id}/view', array('as'=>'adminNavbarLinksView','uses'=>'Admin\NavbarLinkController@view'));
Route::patch('navbar_links/{id}', array('as'=>'adminNavbarLinksUpdate','uses'=>'Admin\NavbarLinkController@update'));
Route::post('navbar_links/seo', array('as'=>'adminNavbarLinksSeo','uses'=>'Admin\NavbarLinkController@seo'));
Route::delete('navbar_links/destroy', array('as'=>'adminNavbarLinksDestroy','uses'=>'Admin\NavbarLinkController@destroy'));
Route::get('navbar_links/order', array('as'=>'adminNavbarLinksOrder','uses'=>'Admin\NavbarLinkController@order'));


Route::get('page_contents', array('as'=>'adminPageContents','uses'=>'Admin\PageContentController@index'));
Route::get('page_contents/{id}/create', array('as'=>'adminPageContentsCreate','uses'=>'Admin\PageContentController@create'));
Route::post('page_contents/', array('as'=>'adminPageContentsStore','uses'=>'Admin\PageContentController@store'));
Route::get('page_contents/{id}/show', array('as'=>'adminPageContentsShow','uses'=>'Admin\PageContentController@show'));
Route::get('page_contents/{id}/view', array('as'=>'adminPageContentsView','uses'=>'Admin\PageContentController@view'));
Route::get('page_contents/{id}/edit', array('as'=>'adminPageContentsEdit','uses'=>'Admin\PageContentController@edit'));
Route::patch('page_contents/{id}', array('as'=>'adminPageContentsUpdate','uses'=>'Admin\PageContentController@update'));
Route::post('page_contents/seo', array('as'=>'adminPageContentsSeo','uses'=>'Admin\PageContentController@seo'));
Route::delete('page_contents/destroy', array('as'=>'adminPageContentsDestroy','uses'=>'Admin\PageContentController@destroy'));
Route::get('page_contents/order', array('as'=>'adminPageContentsOrder','uses'=>'Admin\PageContentController@order'));
Route::post('page_content/toggle', array('as'=>'adminClientPageContentToggle','uses'=>'Admin\PageContentController@pageContentToggle'));

Route::get('page_content_items', array('as'=>'adminPageContentItems','uses'=>'Admin\PageContentItemController@index'));
Route::get('page_content_items/{id}/create', array('as'=>'adminPageContentItemsCreate','uses'=>'Admin\PageContentItemController@create'));
Route::post('page_content_items/', array('as'=>'adminPageContentItemsStore','uses'=>'Admin\PageContentItemController@store'));
Route::get('page_content_items/{id}/show', array('as'=>'adminPageContentItemsShow','uses'=>'Admin\PageContentItemController@show'));
Route::get('page_content_items/{id}/view', array('as'=>'adminPageContentItemsView','uses'=>'Admin\PageContentItemController@view'));
Route::get('page_content_items/{id}/edit', array('as'=>'adminPageContentItemsEdit','uses'=>'Admin\PageContentItemController@edit'));
Route::patch('page_content_items/{id}', array('as'=>'adminPageContentItemsUpdate','uses'=>'Admin\PageContentItemController@update'));
Route::post('page_content_items/seo', array('as'=>'adminPageContentItemsSeo','uses'=>'Admin\PageContentItemController@seo'));
Route::delete('page_content_items/destroy', array('as'=>'adminPageContentItemsDestroy','uses'=>'Admin\PageContentItemController@destroy'));
Route::get('page_content_items/order', array('as'=>'adminPageContentItemsOrder','uses'=>'Admin\PageContentItemController@order'));


// Pages
Route::get('page/{slug?}', array('as'=>'adminClientPage','uses'=>'Admin\PageController@view'));
Route::post('page/toggle', array('as'=>'adminClientPageToggle','uses'=>'Admin\PageController@pageToggle'));
Route::get('page/{slug}/preview', array('as'=>'adminPagesPreview','uses'=>'Admin\PageController@preview'));

// Route::get('page/{slug}/content/{content}/item/create', array('as'=>'adminClientCaboodlePageContentItemCreate','uses'=>'Caboodle\PageContentItemController@clientCreate'));
Route::get('page/{slug}/content/{content}/item/edit/{id}', array('as'=>'adminClientPageContentItemEdit','uses'=>'Admin\PageContentItemController@clientEdit'));
// Route::delete('page/content/item/{id}/delete', array('as'=>'adminClientCaboodlePageContentItemDestroy','uses'=>'Caboodle\PageContentItemController@clientDestroy'));
Route::post('page/{slug}/content/{content}/item/sort', array('as'=>'adminClientCaboodlePageContentItemSort','uses'=>'Caboodle\PageContentItemController@clientSort'));

/* PAGE CONTENT */
Route::get('page/{slug}/content/edit/{id}',array('as'=>'adminClientPageContentEdit','uses'=>'Admin\PageContentController@clientEdit'));
Route::get('page/{slug}/content/edit/{id}/preview/{preview_id}',array('as'=>'adminClientPageContentPreviewEdit','uses'=>'Admin\PageContentController@clientEdit'));
Route::get('page/{slug}/content/create/{content_id?}', array('as'=>'adminClientPageContentCreate','uses'=>'Admin\PageContentController@clientCreate'));
Route::post('page/content/store/{content_id?}', array('as'=>'adminPageClientStore','uses'=>'Admin\PageContentController@clientStore'));
Route::patch('page/{slug}/content/{id}/{preview_id?}', array('as'=>'adminClientPageContentUpdate','uses'=>'Admin\PageContentController@clientUpdate'));
Route::delete('page/content/{id}/delete', array('as'=>'adminClientPageContentDestroy', 'uses'=>'Admin\PageContentController@clientDestroy'));
Route::delete('page/{slug}/content/{id}/preview/delete', array('as'=>'adminPreviewDestroy', 'uses'=>'Admin\PageContentController@preview_destroy'));

// /* PAGE CONTENT ITEMS */
Route::get('page/page_content_item/{slug}/{id}/create', array('as'=>'adminClientPageContentItemCreate','uses'=>'Admin\PageContentItemController@clientCreate'));
Route::patch('page/{slug}/content/{content}/item/{id}', array('as'=>'adminClientPageContentItemUpdate','uses'=>'Admin\PageContentItemController@clientUpdate'));
Route::post('page/{slug}/content/{content}/item', array('as'=>'adminClientPageContentItemStore','uses'=>'Admin\PageContentItemController@clientStore'));
Route::delete('page/content/item/delete/{id}',array('as'=>'adminClientPageContentItemDestroy', 'uses'=>'Admin\PageContentItemController@clientDestroy' ));


// Route::get('test',array('as'=>'pagetemplate', 'uses'=>'Admin\PageTemplateController@index'));
Route::post('test',array('as'=>'pageTemplateEdit', 'uses'=>'Admin\PageTemplateController@edit'));

///contact controller
Route::get('contacts', array('as'=>'adminContacts','uses'=>'Admin\ContactController@index'));
Route::get('contacts/create', array('as'=>'adminContactsCreate','uses'=>'Admin\ContactController@create'));
Route::post('contacts/', array('as'=>'adminContactsStore','uses'=>'Admin\ContactController@store'));
Route::get('contacts/{id}/show', array('as'=>'adminContactsShow','uses'=>'Admin\ContactController@show'));
Route::get('contacts/{id}/view', array('as'=>'adminContactsView','uses'=>'Admin\ContactController@view'));
Route::get('contacts/{id}/edit', array('as'=>'adminContactsEdit','uses'=>'Admin\ContactController@edit'));
Route::patch('contacts/{id}', array('as'=>'adminContactsUpdate','uses'=>'Admin\ContactController@update'));
Route::post('contacts/seo', array('as'=>'adminContactsSeo','uses'=>'Admin\ContactController@seo'));
Route::delete('contacts/destroy', array('as'=>'adminContactsDestroy','uses'=>'Admin\ContactController@destroy'));
Route::get('contacts/order', array('as'=>'adminContactsOrder','uses'=>'Admin\ContactController@order'));

Route::post('contacts', array('as'=>'adminFilterResults','uses'=>'Admin\ContactController@filterResults'));
Route::get('contacts/filterDate', array('as'=>'adminFilterDate','uses'=>'Admin\ContactController@filterByDate'));
Route::get('contacts/export', array('as'=>'adminExport','uses'=>'Admin\ContactController@export'));

Route::get('pages/get_pages', array('as'=>'adminPagesGet','uses'=>'Admin\PageController@get_pages'));
Route::get('articles/get_articles', array('as'=>'adminArticlesGet','uses'=>'Admin\ArticleController@get_articles'));


Route::get('master_products', array('as'=>'adminMasterProducts','uses'=>'Admin\MasterProductController@index'));
Route::get('master_products/create', array('as'=>'adminMasterProductsCreate','uses'=>'Admin\MasterProductController@create'));
Route::post('master_products/', array('as'=>'adminMasterProductsStore','uses'=>'Admin\MasterProductController@store'));
Route::post('master_products/import', array('as'=>'adminMasterProductsImport','uses'=>'Admin\MasterProductController@import'));
Route::get('master_products/export', array('as'=>'adminMasterProductsExport','uses'=>'Admin\MasterProductController@export'));
Route::get('master_products/{id}/show', array('as'=>'adminMasterProductsShow','uses'=>'Admin\MasterProductController@show'));
Route::get('master_products/{id}/view', array('as'=>'adminMasterProductsView','uses'=>'Admin\MasterProductController@view'));
Route::get('master_products/{id}/edit', array('as'=>'adminMasterProductsEdit','uses'=>'Admin\MasterProductController@edit'));
Route::patch('master_products/{id}', array('as'=>'adminMasterProductsUpdate','uses'=>'Admin\MasterProductController@update'));
Route::patch('master_products/', array('as'=>'adminMasterProductsUpdateFromModal','uses'=>'Admin\MasterProductController@updateFromModal'));
Route::post('master_products/seo', array('as'=>'adminMasterProductsSeo','uses'=>'Admin\MasterProductController@seo'));
Route::delete('master_products/destroy', array('as'=>'adminMasterProductsDestroy','uses'=>'Admin\MasterProductController@destroy'));

Route::get('orders', array('as'=>'adminOrders','uses'=>'Admin\OrderController@index'));
Route::get('orders/create', array('as'=>'adminOrdersCreate','uses'=>'Admin\OrderController@create'));
Route::post('orders/import', array('as'=>'adminOrdersImport','uses'=>'Admin\OrderController@import'));
Route::post('orders/', array('as'=>'adminOrdersStore','uses'=>'Admin\OrderController@save'));
Route::get('orders/{id}/show', array('as'=>'adminOrdersShow','uses'=>'Admin\OrderController@show'));
Route::get('orders/{id}/view', array('as'=>'adminOrdersView','uses'=>'Admin\OrderController@view'));
Route::get('orders/{id}/edit', array('as'=>'adminOrdersEdit','uses'=>'Admin\OrderController@edit'));
Route::patch('orders/{id}', array('as'=>'adminOrdersUpdate','uses'=>'Admin\OrderController@save'));
Route::post('orders/seo', array('as'=>'adminOrdersSeo','uses'=>'Admin\OrderController@seo'));
Route::delete('orders/destroy', array('as'=>'adminOrdersDestroy','uses'=>'Admin\OrderController@destroy'));

Route::get('lazada_transactions', array('as'=>'adminLazadaTransactions','uses'=>'Admin\LazadaTransactionController@index'));
Route::post('lazada_transactions/', array('as'=>'adminLazadaTransactionsStore','uses'=>'Admin\LazadaTransactionController@store'));
Route::post('lazada_transactions/import', array('as'=>'adminLazadaTransactionsImport','uses'=>'Admin\LazadaTransactionController@import'));
Route::delete('lazada_transactions/destroy', array('as'=>'adminLazadaTransactionsDestroy','uses'=>'Admin\LazadaTransactionController@destroy'));

Route::get('shopee_transactions', array('as'=>'adminShopeeTransactions','uses'=>'Admin\ShopeeTransactionController@index'));
Route::post('shopee_transactions/', array('as'=>'adminShopeeTransactionsStore','uses'=>'Admin\ShopeeTransactionController@store'));
Route::post('shopee_transactions/import', array('as'=>'adminShopeeTransactionsImport','uses'=>'Admin\ShopeeTransactionController@import'));
Route::delete('shopee_transactions/destroy', array('as'=>'adminShopeeTransactionsDestroy','uses'=>'Admin\ShopeeTransactionController@destroy'));

Route::patch('orders_item/purchase_price', array('as'=>'adminOrderItemUpdatePurchasePrice','uses'=>'Admin\OrderController@updatePurchasePriceFromModal'));
Route::patch('orders_item/purchase_price/multiple', array('as'=>'adminOrderItemUpdatePurchasePriceMultiple','uses'=>'Admin\OrderController@updatePurchasePriceFromModalMultiple'));